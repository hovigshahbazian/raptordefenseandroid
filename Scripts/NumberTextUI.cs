﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (Text))]
public class NumberTextUI : MonoBehaviour {
	private Text text;

	public string eventName;
	public bool Decrementing;
	public int number;
	public int GoalNumber;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();

	

		if (Decrementing) {
			if (eventName != "") {
				EventManager.StartListening (eventName, DecrementNumber);
			}
		} else{

			if (eventName != "") {
				EventManager.StartListening (eventName, IncrementNumber);
				//Debug.Log ("Listening for " + eventName);
			}
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (GoalNumber == 0) {
			text.text = number.ToString ();;
		} else {
			text.text = number.ToString () + " / " + GoalNumber.ToString ();
		}



	}


	public void IncrementNumber(){
		//Debug.Log ("Incrementing Number for " + eventName);
		number = Mathf.Clamp(++number, 0, GoalNumber);
		//number++;
	}


	public void DecrementNumber(){
		number = Mathf.Clamp(number--, 0, GoalNumber);
	}


}
