﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelGainAnimation : MonoBehaviour {

	public Animator fuelGainAnim;
	//private PlayerGameData playerData;
	public Sprite fullCell;
	public Sprite emptyCell;

	public Image[] fuelCells;
	[SerializeField]
	private int previousFuelCell;




	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		previousFuelCell = GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells;
		//fuelGainAnim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(fuelGainAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle")){
			//Debug.Log("Not playing");
		}
	}

	public void SetPreviousFuelCell(){
		previousFuelCell = GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells;
	}

	public void SetPreviousFuelCell(int f){
		previousFuelCell = f;
	}


	public void TriggerAnimation(){
		
		fuelGainAnim.SetInteger("Fuel",2);

		switch(previousFuelCell){

		case 7:
			fuelGainAnim.SetTrigger("FuelGain8");
			break;
		case 6:
			fuelGainAnim.SetTrigger("FuelGain7");
			break;
		case 5:
			fuelGainAnim.SetTrigger("FuelGain6");
			break;
		case 4:
			fuelGainAnim.SetTrigger("FuelGain5");
			break;
		case 3:
			fuelGainAnim.SetTrigger("FuelGain4");
			break;
		case 2:
			fuelGainAnim.SetTrigger("FuelGain3");
			break;
		case 1:
			fuelGainAnim.SetTrigger("FuelGain2");
			break;
		case 0:
			fuelGainAnim.SetTrigger("FuelGain1");
			break;
		}

		//UpdateFuelCellsImg();
	}


	public void UpdateFuelCellsImg(){
		for(int i = 0; i < fuelCells.Length; i++){

			fuelCells[i].overrideSprite = emptyCell;

			if(i < previousFuelCell){
				fuelCells[i].overrideSprite = fullCell;
			}
		}
	}




}
