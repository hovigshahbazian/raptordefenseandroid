﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNodeSpawner : MonoBehaviour {

	private PathNode currentNode;
	public Vector3 NewNodePos;
	public ObjectPool pool;
	public bool useRandomPos = false;
	public float maximunHeight  = 18;
	public float minimunHeight = 0;
	public float VerticalMoveRange = 10f;
	public float VerticalZag = 5f;
	public float HorzontalMoveRange = 50f;
	public float HorizontalZag = 50f;
	// Use this for initialization



	void Start () {

		pool = GameObject.Find ("PathNodePool").GetComponent<ObjectPool> ();

		float randx = Random.Range (HorzontalMoveRange, HorzontalMoveRange+HorizontalZag);
		float randy =  Random.Range (-(VerticalMoveRange),  VerticalMoveRange);

		if (randy < 0) {
			randy -= VerticalZag;
		} else {
			randy += VerticalZag;
		}

		if (useRandomPos) {
			NewNodePos = new Vector3 (randx,randy, 0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public GameObject SpawnNewNode(){
		GameObject obj = pool.GetPooledObject();


		obj.transform.localPosition =  new Vector3(this.transform.localPosition.x + NewNodePos.x, Mathf.Clamp(this.transform.localPosition.y + NewNodePos.y, minimunHeight,maximunHeight), 0); 
		obj.transform.rotation = Quaternion.identity;
		obj.transform.localScale = Vector3.one;
		obj.SetActive (true);

		return obj ;
	}

}
