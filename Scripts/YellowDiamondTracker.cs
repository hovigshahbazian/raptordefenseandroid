﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YellowDiamondTracker : MonoBehaviour {



	//public LevelGameData levelData;

	public Toggle EmblemOneTgl;
	public Toggle EmblemTwoTgl;
	public Toggle EmblemThreeTgl;


	// Use this for initialization
	void Start () {
		EventManager.StartListening ("EmblemOne",ActiveEmblemOne);
		EventManager.StartListening ("EmblemTwo",ActiveEmblemTwo);
		EventManager.StartListening ("EmblemThree",ActiveEmblemThree);

		/*
		if (levelData.FirstEmblemGot) {
			EmblemOneTgl.isOn = true;
		}

		if (levelData.SecondEmblemGot) {
			EmblemTwoTgl.isOn = true;
		}

		if (levelData.ThirdEmblemGot) {
			EmblemThreeTgl.isOn = true;
		}
       */

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void ActiveEmblemOne(){
		EmblemOneTgl.isOn = true;
		//levelData.FirstEmblemGot = true;
	}

	public void ActiveEmblemTwo(){
		EmblemTwoTgl.isOn = true;
		//levelData.SecondEmblemGot = true;
	}

	public void ActiveEmblemThree(){
		EmblemThreeTgl.isOn = true;
		//levelData.ThirdEmblemGot = true;
	}


}
