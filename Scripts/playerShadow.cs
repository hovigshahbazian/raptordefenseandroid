﻿using UnityEngine;
using System.Collections;

public class playerShadow : MonoBehaviour {

	private SpriteRenderer sprRender;

	public Transform playerTrans;

	private float currentPlaneZ;
	private float curZ;
	private Vector3 curUp;
	// Use this for initialization
	void Start () {
	
		sprRender = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {



		if (playerTrans != null) {

			playerTrans.rotation.ToAngleAxis ( out curZ, out curUp);

			transform.rotation = Quaternion.AngleAxis (curZ, Vector3.up);

	


			if (playerTrans.position.y <= transform.position.y) {
				sprRender.enabled = false;
			} else {
				sprRender.enabled = true;
			}

		
		}



	}
}
