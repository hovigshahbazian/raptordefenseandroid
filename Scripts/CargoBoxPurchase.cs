﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoBoxPurchase : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GainCommonCargoBoxes(int i){
		GameDataManager.instance.LoadedTempPlayerData.GainCommonCargoBox(i);
		GameDataManager.instance.SaveTempPlayer();
	}

	public void GainRareCargoBoxes(int i){
		GameDataManager.instance.LoadedTempPlayerData.GainRareCargoBox(i);
		GameDataManager.instance.SaveTempPlayer();
	}

	public void GainEpicCargoBoxes(int i){
		GameDataManager.instance.LoadedTempPlayerData.GainEpicCargoBox(i);
		GameDataManager.instance.SaveTempPlayer();
	}

}
