﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Upgrades { Armor,Wings, Ammo, Heat };
public enum CargoTypes { Common, Rare, Epic };
public static class PlayerUpgradesData {

	public static int[] SecondaryWeaponSupply = { 0, 1, 5, 10, 20, 50 };
	public static float[] SecondaryFireSpeed = { 0,0,0.1f,0.2f,0.3f,0.4f};
	public static int[] ArmorStrength = { 0,1,7,10,12,15};
	public static float[] RotationSpeed = {0f,10f,20f,60f,80f,120f };
	public static float[] HeatUpRate = {0f,0f,0.1f, 0.2f,0.3f,0.4f};
	public static float[] CoolDownTime = {0f,0f,0.5f,1f,2f,3f};
}
