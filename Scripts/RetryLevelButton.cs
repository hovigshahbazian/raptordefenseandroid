﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetryLevelButton : MonoBehaviour {
	//private PlayerGameData playerData;
	public GM game;
	public RectTransform displayPrompt;
	public Button playbutton;
	public bool DoCheck = true;
    public bool DecrementingFuel = false;
	[SerializeField]
	AudioSource confirm;

	[SerializeField]
	AudioSource decline;

	// Use this for initialization
	void Start () {
       DecrementingFuel = false;
		//playerData = GameDataManager.instance.loadedPlayerData;
	}
	
	// Update is called once per frame
	void Update () {

		if (DoCheck) {
			DisableButtonIfNoFuelCells ();
        }else
        {
            displayPrompt.gameObject.SetActive(false);
        }


	}
		
	public void CheckFuelCells(){
        Debug.Log("Checking fuel");

        if (GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells > 0 && DoCheck)
        {


            if (!DecrementingFuel)
            {
                GameDataManager.instance.LoadedTempPlayerData.DecrementFuelCell();
                DecrementingFuel = true;
            }

            Debug.Log("Decrmeenting fuel");
            GameDataManager.instance.SaveTempPlayer();
            displayPrompt.gameObject.SetActive(false);

            confirm.Play();

            game.RestartLevel();


        }
        else if (!DoCheck)
        {

            GameDataManager.instance.SaveTempPlayer();
            displayPrompt.gameObject.SetActive(false);

            confirm.Play();

            game.RestartLevel();

        }
        else
        {

            displayPrompt.gameObject.SetActive(true);
            decline.Play();
            GameDataManager.instance.GetComponent<FuelCellTimer>().PlayEmptyFuelCell();
        }
			
	}


	public void DisableButtonIfNoFuelCells(){
		if (GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells > 0) {

			displayPrompt.gameObject.SetActive (false);
			if(playbutton !=null)
				playbutton.interactable = true;
			
		}
		else{
			displayPrompt.gameObject.SetActive (true);
			if(playbutton !=null)
				playbutton.interactable = false;
		}
	}


	public void StartChecking(){
		DoCheck = true;
	}

	public void StopChecking(){
		DoCheck = false;
	}
}
