﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTextUI : MonoBehaviour {

	//public LevelGameData leveldata;
	public LevelName levelName;
	public Achievement achievement;
	public AcheivementCounter achcounter;
	public Text text;
	public bool onlyNum;
	public bool MissionScore;
    // Use this for initialization
    private GM gameManager;


	void Start () {

       // gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GM>();

		switch (achievement) {
		case Achievement.Ace:
			text.text = "Air Enemies Destroyed: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].AirEnemiesKilled;
			if (onlyNum)
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].AirEnemiesKilled.ToString ();

			if (MissionScore) 
				text.text = achcounter.enemyAir.ToString ();
			

			break;
		case Achievement.DeathFromAbove:
			text.text = "Kills with secondary Weapons: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryKills;
			if (onlyNum) 
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryKills.ToString();

			if (MissionScore) 
				text.text =	achcounter.secondaryKill.ToString ();
			

			break;
		case Achievement.AlwaysLoaded:
			text.text = "Secondary Ammo collected: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryAmmoCollected;
			if (onlyNum)
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryAmmoCollected.ToString ();

			if (MissionScore)
				text.text =	achcounter.secondaryAmmoCollected.ToString ();;

			break;
		case Achievement.OverPowered:
			text.text = "PowerUps Collected: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].PowerUpsCollected;
			if (onlyNum)
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].PowerUpsCollected.ToString ();

			if (MissionScore)
				text.text =	achcounter.powerUpCollected.ToString ();
			

			break;
		case Achievement.EarthMover:
			text.text = "Ground Enemies Destroyed: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].GroundEnemiesKilled;
			if (onlyNum)
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].GroundEnemiesKilled.ToString ();

			if (MissionScore)
				text.text =	achcounter.enemyground.ToString ();
			

			break;
		case Achievement.GuardianAngel:
			text.text = "Allies Saved: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].AlliesSaved;
			if (onlyNum)
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].AlliesSaved.ToString ();


			if (MissionScore)
				text.text =	achcounter.allysaved.ToString ();
			

			break;
		}

	}
	
	// Update is called once per frame
	void Update () {
		switch (achievement) {
		case Achievement.Ace:
			text.text = "Air Enemies Destroyed: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].AirEnemiesKilled;
			if (onlyNum) 
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].AirEnemiesKilled.ToString();

			if (MissionScore) 
				text.text =	achcounter.enemyAir.ToString ();

			break;
		case Achievement.DeathFromAbove:
			text.text = "Kills with secondary Weapons: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryKills;
			if (onlyNum) 
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryKills.ToString();

			if (MissionScore) 
				text.text =	achcounter.secondaryKill.ToString ();
			

			break;
		case Achievement.AlwaysLoaded:
			text.text = "Secondary Ammo collected: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryAmmoCollected;
			if (onlyNum) 
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].SecondaryAmmoCollected.ToString();

			if (MissionScore) 
				text.text =	achcounter.secondaryAmmoCollected.ToString ();
	
			break;
		case Achievement.OverPowered:
			text.text = "PowerUps Collected: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].PowerUpsCollected;
			if (onlyNum) 
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].PowerUpsCollected.ToString();
			
			if (MissionScore) 
				text.text =	achcounter.powerUpCollected.ToString ();
		
			break;
		case Achievement.EarthMover:
			text.text = "Ground Enemies Destroyed: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].GroundEnemiesKilled;
			if (onlyNum) 
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].GroundEnemiesKilled.ToString();


			if (MissionScore) 
				text.text =	achcounter.enemyground.ToString ();
			

			break;
		case Achievement.GuardianAngel:
			text.text = "Allies Saved: " + GameDataManager.instance.LoadedTempLevelData[(int)levelName].AlliesSaved;
			if (onlyNum) 
				text.text = GameDataManager.instance.LoadedTempLevelData[(int)levelName].AlliesSaved.ToString();


			if (MissionScore) 
				text.text =	achcounter.allysaved.ToString ();
			

			break;
		}
	}
}
