﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;

public class LeaderBoardTest : MonoBehaviour {

	public string leaderboardID;

	void Start () 
	{
		AuthenticateToGameCenter();
	}


	private bool isAuthenticatedToGameCenter;

	public void  AuthenticateToGameCenter()
	{

		Social.localUser.Authenticate(success =>
		{
		if (success)
		{
		Debug.Log("Authentication successful");
		}
		else
		{
		Debug.Log("Authentication failed");
		}
		});
	
	}

	//call to update the leaderboard with your score
	public  void ReportScore(long score, string leaderboardID)
	{

		Debug.Log("Reporting score " + score + " on leaderboard " + leaderboardID);
		Social.ReportScore(score, leaderboardID, success =>
		{
		if (success)
		{
		Debug.Log("Reported score successfully");
		}
		else
		{
		Debug.Log("Failed to report score");
		}

		});
	
	}

	//call to show leaderboard
	public void ShowLeaderboard()
	{
		
		Social.ShowLeaderboardUI();

	}


	//call to show leaderboard
	public void ReportLeaderboard()
	{

		ReportScore (100, leaderboardID);

	}







}
