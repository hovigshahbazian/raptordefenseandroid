﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
#if UNITY_ANDROID
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
#endif
using System.Text;
public class GooglePlayGameScript : MonoBehaviour {



	//public string leaderboard;
	const string SAVE_NAME = "playerInfo.dat";
	bool isSaving;
	bool isCloudDataLoaded = false;

	void Start () { // recommended for debugging: 
		#if UNITY_ANDROID
		PlayGamesPlatform.DebugLogEnabled = true; 

		//PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
		//PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.Activate (); 
		#endif  

		LogIn ();
	} 

	public  void LogIn () { 
		Social.localUser.Authenticate ((bool success) => { 
			if (success) {
				Debug.Log ("Login Sucess");
			} else {
				Debug.Log ("Login failed"); 
			} 
		}); 
	} 

	/// <summary> /// Shows All Available Leaderborad /// </summary>
	public static void OnShowLeaderBoard () { 
		//	
		//Social.ShowLeaderboardUI (); 


		#if UNITY_ANDROID
		// Show all leaderboard 
		((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (GPGSIds.leaderboard_survival_leaderboard); // Show current (Active) leaderboard 
		#endif

		#if UNITY_IPHONE
		Social.Active.ShowLeaderboardUI (); // Show current (Active) leaderboard 
		#endif

	} 

	public static void OnShowAcheivements(){
		#if UNITY_ANDROID
		// Show all leaderboard 
		((PlayGamesPlatform)Social.Active).ShowAchievementsUI(); // Show current (Active) leaderboard 
		#endif

		#if UNITY_IPHONE
		Social.Active.ShowAchievementsUI(); // Show current (Active) leaderboard 
		#endif
	}

	public static int GetPlayerScore(){
		int score = 0;
		#if UNITY_ANDROID

		PlayGamesPlatform.Instance.LoadScores (
			GPGSIds.leaderboard_survival_leaderboard,
			LeaderboardStart.PlayerCentered,
			1,
			LeaderboardCollection.Public,
			LeaderboardTimeSpan.AllTime,
			(LeaderboardScoreData data) => {
				//Debug.Log (data.Valid);
				//Debug.Log (data.Id);
				//Debug.Log (data.PlayerScore);

				if(data.PlayerScore != null){
					score = (int)data.PlayerScore.value;
				}
			//	Debug.Log (data.PlayerScore.userID);
				//Debug.Log (data.PlayerScore.formattedValue);
			});
	
		#endif

		#if UNITY_IPHONE

		#endif


		return score;
	}


	/// <summary> /// Adds Score To leader board /// </summary> /// 
	 public  void OnAddScoreToLeaderBoard () {

		if (Social.localUser.authenticated) {
			Social.ReportScore (GameDataManager.instance.HighScore, GPGSIds.leaderboard_survival_leaderboard, (bool success) => {
				if (success) {
				Debug.Log ("Update Score Success"); 
				} else {
					Debug.Log ("Update Score Fail"); 
				} 
			}); 
		} 


	}


	public static void OnOnAddScoreToLeaderBoard(int score){
		
		if (Social.localUser.authenticated) {
			Social.ReportScore (score, GPGSIds.leaderboard_survival_leaderboard, (bool success) => {
				if (success) {
					Debug.Log ("Update Score Success"); 
				} else {
					Debug.Log ("Update Score Fail"); 
				} 
			}); 
		} 

	}
	/*
	#if UNITY_ANDROID
	public void LoadCloudData(){

		if (Social.localUser.authenticated) {
			isSaving = false;
			((PlayGamesPlatform)Social.Active).SavedGame.OpenWithManualConflictResolution (SAVE_NAME, DataSource.ReadCacheOrNetwork, true, ResolveConflict, OnSavedGameOpened);
		} else {

		}

	}
	#endif

*/
	/// <summary> /// On Logout of your Google+ Account /// </summary> 
	public  void OnLogOut () {
		#if UNITY_IPHONE
		//Social.Active.SignOut (); 
		#endif

		#if UNITY_ANDROID
		((PlayGamesPlatform)Social.Active).SignOut (); 
		#endif
	} 

	/*
	public void LoadLocal(){
		GameDataManager.instance.Load ();
		GameDataManager.instance.Load();
		GameDataManager.instance.LoadLevel (1);
		GameDataManager.instance.LoadLevel (2);
		GameDataManager.instance.LoadLevel (3);
		GameDataManager.instance.LoadLevel (4);
		GameDataManager.instance.LoadLevel (5);
		GameDataManager.instance.LoadLevel (6);
		GameDataManager.instance.LoadLevel (7);
		GameDataManager.instance.LoadLevel (8);
		GameDataManager.instance.LoadLevel (9);
		GameDataManager.instance.LoadLevel (10);
		GameDataManager.instance.LoadLevel (11);
		GameDataManager.instance.LoadLevel (12);
		GameDataManager.instance.LoadLevel (13);
		GameDataManager.instance.LoadLevel (14);

	}

	public void SaveLocal(){
		GameDataManager.instance.Save ();
		GameDataManager.instance.SaveLevel (1);
		GameDataManager.instance.SaveLevel (2);
		GameDataManager.instance.SaveLevel (3);
		GameDataManager.instance.SaveLevel (4);
		GameDataManager.instance.SaveLevel (5);
		GameDataManager.instance.SaveLevel (6);
		GameDataManager.instance.SaveLevel (7);
		GameDataManager.instance.SaveLevel (8);
		GameDataManager.instance.SaveLevel (9);
		GameDataManager.instance.SaveLevel (10);
		GameDataManager.instance.SaveLevel (11);
		GameDataManager.instance.SaveLevel (12);
		GameDataManager.instance.SaveLevel (13);
		GameDataManager.instance.SaveLevel (14);
	}
*/
	/*
	public void SaveData(){
		
		if (!isCloudDataLoaded) {
			SaveLocal ();
			return;
		} 
		#if UNITY_ANDROID	
		if (Social.localUser.authenticated) {
			isSaving = true;

			((PlayGamesPlatform)Social.Active).SavedGame.OpenWithManualConflictResolution (SAVE_NAME, DataSource.ReadCacheOrNetwork, true, ResolveConflict, OnSavedGameOpened);
		} else {
			SaveLocal ();
		}
		#endif
	}


	#if UNITY_ANDROID
	private void ResolveConflict(IConflictResolver resolver, ISavedGameMetadata original, byte[]originalData, ISavedGameMetadata unmerged, byte[] unmergedData)
	{
		if (originalData == null) {
			resolver.ChooseMetadata (unmerged);
		} else if (unmergedData == null) {
			resolver.ChooseMetadata (original);
		} else {
			resolver.ChooseMetadata (original);
		}
	}
	#endif

	#if UNITY_ANDROID
	public void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
	{
		if (status == SavedGameRequestStatus.Success) {
			if (!isSaving) {
				LoadGame (game);
			} else {
				SaveGame (game);
			}
		} else {
			if (!isSaving) {
				LoadLocal ();
			} else {
				SaveLocal ();
			}
		}
	}
	#endif

	#if UNITY_ANDROID
	private void LoadGame(ISavedGameMetadata game){
		((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData (game, OnSavedGameDataRead);
	}
	#endif

	#if UNITY_ANDROID
	private void SaveGame(ISavedGameMetadata game){
		
		//string stringToSave;

		//byte[] dataToSave = 0;

		//SavedGameMetadataUpdate update = new SavedGameMetadataUpdate.Builder ().Build ();
		//((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate (game,update,dataToSave,OnSavedGameDataWritten);
	}
	#endif

	#if UNITY_ANDROID
	private void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] savedData)
	{
		if (status == SavedGameRequestStatus.Success) {



		}
	}
	#endif 
	#if UNITY_ANDROID
	private void OnSavedGameDataWritten(SavedGameRequestStatus status,ISavedGameMetadata game){
		if (status == SavedGameRequestStatus.Success) {



		}
	}
	#endif
	*/
} 