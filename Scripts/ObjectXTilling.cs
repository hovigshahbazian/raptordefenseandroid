﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectXTilling : MonoBehaviour {

	[SerializeField]
	private Transform player;

	[SerializeField]
	private Transform parent;

	private float currentTilePos;
	//private float CurrentAirbaseTile;
	private ObjectPool pool;

	[SerializeField]
	float SpawnLocationOffset;

	[SerializeField]
	float xTranslate;


	// Use this for initialization
	void Start () {
		

		currentTilePos = transform.localPosition.x;
		pool = GetComponent<ObjectPool> ();
	}
	
	// Update is called once per frame
	void Update () {

		if(player.localPosition.x > currentTilePos+SpawnLocationOffset){
			//currentTilePos += 500f;




			GameObject obj = pool.GetPooledObject ();



			if (parent != null) {
				obj.transform.parent = parent;
			}

			obj.transform.localPosition = Vector3.zero;

			obj.SetActive (true);

			//if (parent != null) {
				obj.transform.Translate(new Vector3 (currentTilePos + xTranslate, 0f, 0f));
			//} else {
				//obj.transform.Translate (new Vector3 (currentTilePos + xTranslate, 0f, 0f));
			//}

			currentTilePos = obj.transform.localPosition.x;


		}


	}
}
