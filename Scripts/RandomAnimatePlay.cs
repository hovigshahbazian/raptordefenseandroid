﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimatePlay : MonoBehaviour {
	public string triggerName;
	public Animator anim;
	public float RandomTimeMax;
	private float timer;

	public bool waiting;

	// Use this for initialization
	void Start () {
		timer = Random.Range (1,RandomTimeMax);
		waiting = true;
	}
	
	// Update is called once per frame
	void Update () {


		if (waiting) {

			timer -= Time.deltaTime;


			if (timer <= 0) {
				anim.SetTrigger (triggerName);
				timer = Random.Range (1,RandomTimeMax); 

			}
		}

	}
}
