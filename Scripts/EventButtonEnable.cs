﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventButtonEnable : MonoBehaviour {

	public bool enable;
	public string EventName;
	[SerializeField]
	private Button button;

	// Use this for initialization
	void Start () {
	//	button = GetComponent<Button> ();
		EventManager.StartListening (EventName, SetButtonEnable);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetButtonEnable(){
		button.interactable = enable;
	}
}
