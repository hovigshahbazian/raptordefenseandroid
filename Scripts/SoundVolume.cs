﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SoundVolume : MonoBehaviour {

	Slider soundSlider;
	public Image Background;

	public Image Fill;
	public Image Handle;

	// Use this for initialization
	void Start () {
		soundSlider = GetComponent<Slider> ();
		soundSlider.value = PlayerPrefs.GetFloat ("soundvolumechangevalue")*10;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Settings.active==true) {
			Background.enabled = true;
			Handle.enabled = true;
			Fill.enabled = true;
		}
		else {
			Background.enabled = false;
			Handle.enabled = false;
			Fill.enabled = false;
		}

	}

	public void ChangeSoundVolume(float vol){
		MM.soundvolumechangevalue=vol/10;
		MM.soundvolumechange = true;
	}



}
