﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectorButton : MonoBehaviour {

	[SerializeField]
	public static int currentLevel = 0;
	public Button Tutorial;
	public Button level1;
	public Button level2;
	public Button level3;
	public Button level4;
	public Button level5;
	public Button level6;
	public Button level7;
	public Button level8;
	public Button level9;
	public Button level10;
	public Button levelA;
	public Button levelB;
	public Button Endless;
	public MissionCursorUI selector;
	//private LevelName BonusA;
	//private LevelData BonusB;
	public int CurrentLevel{ 
		set{currentLevel = Mathf.Clamp(value,0,13);} 
		get{return currentLevel;}
	}



	// Use this for initialization
	void Start () {

		//GameDataManager.instance.GetTempLevelData(LevelName.LevelA, ref BonusA);
		//GameDataManager.instance.GetTempLevelData(LevelName.LevelB,ref BonusB);
		//currentLevel = 1;
		SetSelectorToButton();
		SelectLevel(currentLevel);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetSelectorToButton(){

      //  throw new System.Exception();

		switch (currentLevel) {
           case 13:
            selector.SetPositionToButton(Endless.GetComponent<RectTransform>());
            break;
            case 12:
			selector.SetPositionToButton(levelB.GetComponent<RectTransform>());
			break;
		case 11:
			selector.SetPositionToButton(levelA.GetComponent<RectTransform>());
			break;
		case 10:
			selector.SetPositionToButton(level10.GetComponent<RectTransform>());
			break;
		case 9:
			selector.SetPositionToButton(level8.GetComponent<RectTransform>());
			break;
		case 8:
			selector.SetPositionToButton(level8.GetComponent<RectTransform>());
			break;
		case 7:
			selector.SetPositionToButton(level7.GetComponent<RectTransform>());
			break;
		case 6:
			selector.SetPositionToButton(level6.GetComponent<RectTransform>());
			break;
		case 5:
			selector.SetPositionToButton(level5.GetComponent<RectTransform>());
			break;
		case 4:
			selector.SetPositionToButton(level4.GetComponent<RectTransform>());
			break;
		case 3:
			selector.SetPositionToButton(level3.GetComponent<RectTransform>());
			break;
		case 2:
			selector.SetPositionToButton(level2.GetComponent<RectTransform>());
			break;
		case 1:
			selector.SetPositionToButton(level1.GetComponent<RectTransform>());
			break;
		case 0:
			selector.SetPositionToButton(Tutorial.GetComponent<RectTransform>());
			break;
		default:
            selector.SetPositionToButton(Tutorial.GetComponent<RectTransform>());
            break;

		}

	}

	public void SelectNextLevel(){
		switch (currentLevel) {
		case 12:
			
			break;
		case 11:
			if(level6.interactable)
				level6.onClick.Invoke ();
			break;
		case 10:
			if(levelB.interactable)
				levelB.onClick.Invoke ();
			break;
		case 9:
			if(level10.interactable)
				level10.onClick.Invoke ();
			break;
		case 8:
			if(level9.interactable)
				level9.onClick.Invoke ();
			break;
		case 7:
			if(level8.interactable)
				level8.onClick.Invoke ();
			break;
		case 6:
			if(level7.interactable)
				level7.onClick.Invoke ();
			break;
		case 5:
			

			if(levelA.interactable){
				levelA.onClick.Invoke ();
			}else{
				if(level6.interactable)
					level6.onClick.Invoke ();
			}

			break;
		case 4:
			if(level5.interactable)
				level5.onClick.Invoke ();
			break;
		case 3:
			if(level4.interactable)
				level4.onClick.Invoke ();
			break;
		case 2:
			if(level3.interactable)
				level3.onClick.Invoke ();
			break;
		case 1:
			if(level2.interactable)
				level2.onClick.Invoke ();
			break;
		case 0:
			if (level1.interactable)
				level1.onClick.Invoke ();
			break;
		default:
			break;

		}

		SetSelectorToButton();
	}

	public void SelectPreviousLevel(){
		switch (currentLevel) {
		case 12:
			level10.onClick.Invoke ();
			break;
		case 11:
			level5.onClick.Invoke ();
			break;
		case 10:
			level9.onClick.Invoke ();
		
			break;
		case 9:
			level8.onClick.Invoke ();
		
			break;
		case 8:
			level7.onClick.Invoke ();

			break;
		case 7:
			level6.onClick.Invoke ();

			break;
		case 6:

			if(levelA.interactable){
				levelA.onClick.Invoke ();
			}else{
				level5.onClick.Invoke ();
			}

			break;
		case 5:
			level4.onClick.Invoke ();
		
			break;
		case 4:
			level3.onClick.Invoke ();

			break;
		case 3:
			level2.onClick.Invoke ();
		
			break;
		case 2:
			level1.onClick.Invoke ();

			break;
		case 1:
			Tutorial.onClick.Invoke ();
			break;

		default:
			break;

		}
		SetSelectorToButton();
	}

	public void SelectLevel(int i){

		currentLevel = i;

	switch (currentLevel) {
		case 13:
			Endless.onClick.Invoke ();
			break;
		case 12:
			levelB.onClick.Invoke ();
			break;
		case 11:
			levelA.onClick.Invoke ();
			break;
		case 10:
			level10.onClick.Invoke ();
			break;
		case 9:
			level9.onClick.Invoke ();
			break;
		case 8:
			level8.onClick.Invoke ();		
			break;
		case 7:
			level7.onClick.Invoke ();
			break;
		case 6:
			level6.onClick.Invoke ();
			break;
		case 5:
			level5.onClick.Invoke ();
			break;
		case 4:
			level4.onClick.Invoke ();
			break;
		case 3:
			level3.onClick.Invoke ();
			break;
		case 2:
			level2.onClick.Invoke ();
			break;
		case 1:
			level1.onClick.Invoke ();
			break;
		case 0:
			Tutorial.onClick.Invoke ();
			break;
		default:
			break;
		}
		SetSelectorToButton();
	}


	public void SetCurrentLevel(int i){
		currentLevel = i;
	}


}
