﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {

	private AudioSource collectsound;
	private Collider2D col2D;
	private SpriteRenderer sprite;

	// Use this for initialization
	void Start () {
		collectsound = GetComponent<AudioSource> ();
		col2D = GetComponent<Collider2D> ();
		sprite = GetComponent<SpriteRenderer> ();
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D col){
		//EventManager.TriggerEvent ("AcquireSecondaryAmmo");

		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();

		if (cd != null) {
			//cd.SetShipUpgradeText ();
			cd.PlayCargoAnim (CollectDisplay.DisplayEvent.HealthPack);
		}


		if (col.gameObject.tag == "Player" || col.gameObject.tag == "PlayerInvul" || col.gameObject.tag == "PlayerStealthed") {

			Health playerheath = col.gameObject.GetComponent<Health> ();
				
			if (playerheath != null) {
				playerheath.RecoverHealth (5);
			}
		}


		col2D.enabled = false;
		sprite.enabled = false;
		collectsound.Play ();
	}
}
