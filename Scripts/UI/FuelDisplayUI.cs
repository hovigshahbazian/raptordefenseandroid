﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FuelDisplayUI : MonoBehaviour {

	public NumberUI fuelNumberUI;
	//private PlayerGameData gameData;

	public Sprite fuel0;
	public Sprite fuel1;
	public Sprite fuel2;
	public Sprite fuel3;
	public Sprite fuel4;
	public Sprite fuel5;
	public Sprite fuel6;
	public Sprite fuel7;
	public Sprite fuel8;


	public Image fuelMeter;
	public Image fuelBar;
	public GameObject AddFuelButton;
	public GameObject Timer;
	// Use this for initialization
	void Start () {
		//gameData = GameDataManager.instance.loadedPlayerData;
		//fuelNumberUI.number = gameData.NumOfFuelCells;


		switch (GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells) {

		case 8:
			fuelMeter.overrideSprite = fuel8;
			break;
		case 7:
			fuelMeter.overrideSprite = fuel7;
			break;
		case 6:
			fuelMeter.overrideSprite = fuel6;
			break;
		case 5:
			fuelMeter.overrideSprite = fuel5;
			break;
		case 4:
			fuelMeter.overrideSprite = fuel4;
			break;
		case 3:
			fuelMeter.overrideSprite = fuel3;
			break;
		case 2:
			fuelMeter.overrideSprite = fuel2;
			break;
		case 1:
			fuelMeter.overrideSprite = fuel1;
			break;
		case 0:
			fuelMeter.overrideSprite = fuel0;
			break;
		default:
			break;
		}




	}
	
	// Update is called once per frame
	void Update () {
		//fuelNumberUI.number = gameData.NumOfFuelCells;

			if (SceneManager.GetActiveScene ().name != "CampaignMap") {

			if(AddFuelButton != null)
				AddFuelButton.gameObject.SetActive (false);

			if(Timer != null)
				Timer.SetActive (false);

			} else {
					
			if(AddFuelButton != null)
				AddFuelButton.gameObject.SetActive (true);

			if (Timer != null) 
				Timer.SetActive (true);
	


			}

		if (SceneManager.GetActiveScene ().name == "TitleScreen") {

			fuelBar.gameObject.SetActive (false);

			if (AddFuelButton != null)
				AddFuelButton.gameObject.SetActive (false);

			if (Timer != null)
				Timer.SetActive (false);
		}

		if (SceneManager.GetActiveScene ().name == "CampaignMap") {

			fuelBar.gameObject.SetActive (true);

			if (AddFuelButton != null)
				AddFuelButton.gameObject.SetActive (true);

			if (Timer != null)
				Timer.SetActive (true);
		}

			

		switch (GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells) {

			case 8:
				fuelMeter.overrideSprite = fuel8;
				break;
			case 7:
				fuelMeter.overrideSprite = fuel7;
				break;
			case 6:
				fuelMeter.overrideSprite = fuel6;
				break;
			case 5:
				fuelMeter.overrideSprite = fuel5;
				break;
			case 4:
				fuelMeter.overrideSprite = fuel4;
				break;
			case 3:
				fuelMeter.overrideSprite = fuel3;
				break;
			case 2:
				fuelMeter.overrideSprite = fuel2;
				break;
			case 1:
				fuelMeter.overrideSprite = fuel1;
				break;
			case 0:
				fuelMeter.overrideSprite = fuel0;
				break;
			default:
				break;
			}

		}

	public void OpenFuelBar(){
		fuelBar.gameObject.SetActive (true);

	}

	public void CloseFuelBar(){
		fuelBar.gameObject.SetActive (false);
	}
		
}
