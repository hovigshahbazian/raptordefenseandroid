using UnityEngine;
using System.Collections;

public class ButtonA : MonoBehaviour {
	public Animation animA;
	public Animation animB;
	public GameObject[] objects;
	public int i;
	public int window;
	public int touches;
	public float touchespos1x;
	public float touchespos1y;
	public float touchespos2x;
	public float touchespos2y;

	public bool touchscreenbottom;

	public GUIStyle customButton;
		
	private Vector2 coords;


	public int heightoffset;
	public int widthoffset;

	//public GUIStyle debugGUI;

	void Start(){

		coords = Camera.main.WorldToScreenPoint (transform.position);
	}



	void OnMouseDrag () {

		if(!GM.Paused){
		animA.Play();
            #if UNITY_EDITOR
				GM.button_pressed =2;
			#endif
            #if UNITY_STANDALONE_WIN
		    	GM.button_pressed =2;
			#endif 		
		}

	}

	/*
	void OnGUI(){


		GUI.Label(new Rect (200, 200, 500, 500), "Touch 1: x:(" + touchespos1x.ToString () +")" + " y:(" +  touchespos1y.ToString ()+ ")" ,debugGUI);
		GUI.Label(new Rect (200, 250, 500, 500), "Touch 2: x:(" + touchespos2x.ToString () +")" + " y:(" +  touchespos2y.ToString ()+ ")" ,debugGUI);
		GUI.Label(new Rect (200, 300, 500, 500), "Buttton A Location: x:(" + coords.x.ToString()  +")" + " y:(" +  coords.ToString()+ ")" ,debugGUI);
		GUI.Label(new Rect (200, 300, 500, 500), "Buttton B Location: x:(" + coords.x.ToString()  +")" + " y:(" +  coords.ToString()+ ")" ,debugGUI);

	}
	*/

	// Update is called once per frame
	void Update () {
		
		int nbTouches = Input.touchCount;

		if(nbTouches > 0 && !GM.Paused)
		{
	    	//print(nbTouches + " touch(es) detected");

			touches=nbTouches;

			touchespos1x=0;
			touchespos1y=0;
			touchespos2x=0;
			touchespos2y=0;

			touchscreenbottom=false;

			GM.button_pressed_type=0;



			for (int i = 0; i < nbTouches; i++)
			{
				Touch touch = Input.GetTouch(i);

			//	if (touch.phase == TouchPhase.Began) {
					if (i == 0) {
						touchespos1x = touch.position.x;
						touchespos1y = touch.position.y;
					}

					if (i == 1) {
						touchespos2x = touch.position.x;
						touchespos2y = touch.position.y;
					}
	                
					if (touchespos1x > (coords.x - window) && touchespos1x < (coords.x + window)) {
						if (touchespos1y > (coords.y - window) && touchespos1y < (coords.y + window)) {


							GM.button_pressed = 2;
							animA.Play();
						}

						
					}
					
					if (touchespos2x > (coords.x - window) && touchespos2x < (coords.x + window)) {
						if (touchespos2y > (coords.y - window) && touchespos2y < (coords.y + window)) {
							GM.button_pressed = 2;		
							//animA.Play();
						}
					}
		        




					if (touchespos1x > (ButtonB.x - window) && touchespos1x < (ButtonB.x + window)) {
						if (touchespos1y > (ButtonB.y - window) && touchespos1y < (ButtonB.y + window)) {
							GM.button_pressed_type = 3;
							GM.button_pressed = 1;
							//animB.Play();
						}
					}



					if (touchespos2x > (ButtonB.x - window) && touchespos2x < (ButtonB.x + window)) {
						if (touchespos2y > (ButtonB.y - window) && touchespos2y < (ButtonB.y + window)) {

                        if (!GM.Paused)
                        {
                            GM.button_pressed_type = 3;
                            GM.button_pressed = 1;
                            animB.Play();
                        }

						}
					}



				/*
					if (touchespos1x > (ButtonDown.x - window) && touchespos1x < (ButtonDown.x + window)) {
						if (touchespos1y > (ButtonDown.y - window) && touchespos1y < (ButtonDown.y + window)) {
							GM.button_pressed_type = 2;
							//animA.Play();
						}
					}

				*/




				/*

					if (touchespos2x > (ButtonDown.x - window) && touchespos2x < (ButtonDown.x + window)) {
						if (touchespos2y > (ButtonDown.y - window) && touchespos2y < (ButtonDown.y + window)) {
							GM.button_pressed_type = 2;
							//animA.Play();
						}
					}

				*/
				/*

					if (touchespos1x > (ButtonUp.x - window) && touchespos1x < (ButtonUp.x + window)) {
						if (touchespos1y > (ButtonUp.y - window) && touchespos1y < (ButtonUp.y + window)) {
							GM.button_pressed_type = 1;
							//animB.Play();
						}
					}

				*/
				/*

					if (touchespos2x > (ButtonUp.x - window) && touchespos2x < (ButtonUp.x + window)) {
						if (touchespos2y > (ButtonUp.y - window) && touchespos2y < (ButtonUp.y + window)) {
							GM.button_pressed_type = 1;
							//animB.Play();

						}
					}
				*/

				//}

			}
		}
		else {
			touches=0;
		}
		/*

        if (PlayerPowerState.curWeaponPower==PlayerPowerState.WeaponPower.Normal)
        	GM.primary_weapon_current=0;
        if (PlayerPowerState.curWeaponPower==PlayerPowerState.WeaponPower.Multishot)
        	GM.primary_weapon_current=1;
  //  	print("curWeaponPower "+GM.primary_weapon_current);

		//for (int i=0;i!=2;i++) {  
		//	objects[i].SetActive(false);
		//}

		//objects[GM.primary_weapon_current].SetActive(true);*/
	}
}
