﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CampaignMapAnimate : MonoBehaviour {

	public bool Active;

	public GameObject[] buttons;

	public GameObject[] objectivePanels;

	public float sequenceTime;
	public float intervalTime;


	float sequenceTimer;
	int i = 0;

	// Use this for initialization
	void Start () {
		sequenceTimer = sequenceTime;
		Active = true;
	}
	
	// Update is called once per frame
	void Update () {
	

		if (Active) {

			sequenceTimer -= Time.deltaTime;

			if (sequenceTimer <= 0) {
				buttons [i].SetActive (true);
				i++;

				if (buttons.Length <= i) {

					Active = false;

				} else {
					sequenceTimer = sequenceTime;
				}
		
			}




		}

	}


	public void UpdateObjectives(int i){


		foreach (GameObject g in objectivePanels) {
			g.SetActive (false);
		}


		objectivePanels [i].SetActive (true);

	}


	public void UpdateButtons(int i){


		foreach (GameObject g in buttons) {

			if(g.GetComponent<CampaignLevelButtonUI>() != null)
				g.GetComponent<CampaignLevelButtonUI>().Selected = false;


			if(g.GetComponent<BonusLevel>() != null)
				g.GetComponent<BonusLevel>().Selected = false;
		}



		if(buttons [i].GetComponent<CampaignLevelButtonUI>() != null){
			buttons [i].GetComponent<CampaignLevelButtonUI>().Selected = true;
			//Debug.Log("Updateing button");
		}

		if(buttons [i].GetComponent<BonusLevel>() != null){
			buttons [i].GetComponent<BonusLevel>().Selected = true;
			//Debug.Log("Updateing button");
		}

	}
}
