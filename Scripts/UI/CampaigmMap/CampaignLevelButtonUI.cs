﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CampaignLevelButtonUI : MonoBehaviour {

	public Sprite levelUnfinished;
	public Sprite LevelFinished;
	public Sprite LevelSelected;
	public Sprite noDiamonComplete;
	public Sprite oneDiamondComplete;
	public Sprite twoDiamondComplete;
	public Sprite threeDiamondComplete;
	public bool Selected;
    public bool CostFuel = true;
	public RectTransform DisplayPrompt;

	//public LevelData levelData;
	public LevelName levelName;
	//public int levelId;
	public StartLevel startButton;


	private Button button;
	private Image levelIcon;
	public Image StarsIcon;
	// Use this for initialization
	void Start () {
		button = GetComponent<Button> ();
		levelIcon = GetComponent<Image> ();
		//ref LevelData levelData;

		//levelData= GameDataManager.instance.LoadedTempLevelData[levelId];

		//Debug.Log (gameObject.name + " Level Unlocked:" + levelData.Unlocked);
		//Debug.Log (gameObject.name + " Level Completed:" + levelData.Completed);

		if (!GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked) {
			levelIcon.sprite = levelUnfinished;
			StarsIcon.sprite = noDiamonComplete;
			button.interactable = false;
		} else {
			levelIcon.sprite = levelUnfinished;
			StarsIcon.sprite = noDiamonComplete;
			button.interactable = true;
		}





		if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].Completed) {
			levelIcon.sprite = LevelFinished;
		
			if (StarsIcon != null) {
				if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 0) {
					StarsIcon.sprite = noDiamonComplete;
				} else if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 1) {
					StarsIcon.sprite = oneDiamondComplete;
				} else if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 2) {
					StarsIcon.sprite = twoDiamondComplete;
				} else if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 3) {
					StarsIcon.sprite = threeDiamondComplete;
				}
			}

		} 
		else {
			levelIcon.sprite = levelUnfinished;
		
			if(StarsIcon != null)
				StarsIcon.sprite = noDiamonComplete;
		}

	
	}
	
	// Update is called once per frame
	void Update () {

		if(levelName == LevelName.Tutorial || levelName == LevelName.Endless || levelName == LevelName.Level1){
			GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked = true;
		}


		if (!GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked) {
			levelIcon.sprite = levelUnfinished;
			StarsIcon.sprite = noDiamonComplete;
			button.interactable = false;


		} else {
			levelIcon.sprite = levelUnfinished;
			StarsIcon.sprite = noDiamonComplete;
			button.interactable = true;
		}





		if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].Completed) {
			levelIcon.sprite = LevelFinished;

			if (StarsIcon != null) {
				if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 0) {
					StarsIcon.sprite = noDiamonComplete;
				} else if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 1) {
					StarsIcon.sprite = oneDiamondComplete;
				} else if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 2) {
					StarsIcon.sprite = twoDiamondComplete;
				} else if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].GoldEmblemsColleted == 3) {
					StarsIcon.sprite = threeDiamondComplete;
				}
			}

		} 
		else {
				levelIcon.sprite = levelUnfinished;

			if(Selected){
				levelIcon.sprite = LevelSelected;
			}

			if(StarsIcon != null)
				StarsIcon.sprite = noDiamonComplete;
		}






	}


	public void SelectLevel(){

        if (CostFuel)
        {

            if (startButton.CheckIfEnoughFuelCells())
            {

                startButton.GetComponent<Button>().interactable = true;
                DisplayPrompt.gameObject.SetActive(false);
            }
            else
            {
                startButton.GetComponent<Button>().interactable = false;
                DisplayPrompt.gameObject.SetActive(true);
            }
        }else
        {
            startButton.GetComponent<Button>().interactable = true;
            DisplayPrompt.gameObject.SetActive(false);
        }

		
	}


	public void InvokeClickFunction(){

		if(button.interactable)
			button.onClick.Invoke();
	}



}
