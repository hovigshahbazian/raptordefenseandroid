﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SecondaryAmmoDisplay : MonoBehaviour {

	public Text numberui;


	public bool Unlimited;

	// Use this for initialization
	void Start () {

		if (!Unlimited) {
			numberui.text = GM.secondary_weapon_uses.ToString ();
		} else {
			numberui.text = "\u221E";
		}




	}
	
	// Update is called once per frame
	void Update () {

		if (!Unlimited) {
			numberui.text = GM.secondary_weapon_uses.ToString ();
		}


	}
}
