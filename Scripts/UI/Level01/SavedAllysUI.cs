﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SavedAllysUI : MonoBehaviour {

	public Text numbers;
	public int GoalNumber;

	// Use this for initialization
	void Start () {
		numbers.text = GM.alliesSaved.ToString () + " / " + GoalNumber.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		numbers.text = GM.alliesSaved.ToString () + " / " + GoalNumber.ToString ();
	}
}
