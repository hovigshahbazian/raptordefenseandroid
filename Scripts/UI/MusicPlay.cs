using UnityEngine;
using System.Collections;

public class MusicPlay : MonoBehaviour {

    public Renderer rend;


	void OnMouseUpAsButton () {
		Settings.musicstart=true;
	}

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Settings.active==true)
			rend.enabled = true;
		else
			rend.enabled = false;

	
	}
}
