﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextTyper : MonoBehaviour {


	public float letterPause = 0.2f;
	public AudioSource typeSound1;
	public AudioSource typeSound2;

	string message;
    Text textComp;

	string savedMessage;

	void OnEnable(){
		textComp = GetComponent<Text>();
		message = textComp.text;
		savedMessage = message;
		textComp.text = "";
		StartCoroutine(TypeText ());
	}



	// Use this for initialization
	void Start () {
		textComp = GetComponent<Text>();
		message = textComp.text;
		textComp.text = "";
		StartCoroutine(TypeText ());
	}





	IEnumerator TypeText () {
		foreach (char letter in message.ToCharArray()) {
			
			textComp.text += letter;

			if(typeSound1){
				typeSound1.Play();
			}

			yield return 0;
			yield return new WaitForSeconds (letterPause);
		}
	}


	void OnDisable(){
		textComp.text = savedMessage;
	}
}
