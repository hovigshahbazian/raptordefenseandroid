﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StructuresDestroyedUI : MonoBehaviour {
	public Text numbers;
	public int GoalNumber;
	// Use this for initialization
	void Start () {
		numbers.text = GM.strucutures_destroyed + " / " + GoalNumber;
	}
	
	// Update is called once per frame
	void Update () {
		numbers.text = GM.strucutures_destroyed + " / " + GoalNumber;
	}
}
