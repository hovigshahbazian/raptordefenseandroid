﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TanksDestroyedUI : MonoBehaviour {
	public Text numbers;
	public int goalNumber;
    // Use this for initialization

    //private GM gameManager;

	void Start () {
        //gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GM>();

		if (goalNumber == 0) {
			numbers.text = GM.enemyunits_Terrain.ToString ();
		} else {
			numbers.text = GM.enemyunits_Terrain + " / " + goalNumber.ToString();
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (goalNumber == 0) {
			numbers.text = GM.enemyunits_Terrain.ToString ();
		} else {
			numbers.text = GM.enemyunits_Terrain + " / " + goalNumber.ToString ();
		}
	}
}
