﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FollowArrowUI : MonoBehaviour {
	
	public Transform otherTransform;
	public Transform playerTransform;
	public FaceTowardObject faceToObject;
	public GameObject Pivot;

	public GoalFlag goalFlag;

	//private Vector3 targetPosition;
	//private Camera cam;
	private SpriteRenderer otherRender;
	public Image image;

	public TimedEvent timeEvent;

	// Use this for initialization
	void Start () {
		//cam = Camera.main;

		if(otherTransform != null)
			otherRender = otherTransform.GetComponent<SpriteRenderer> ();
		
		image = GetComponent<Image> ();
	//	timeEvent.StopTimer ();
	}
	
	// Update is called once per frame
	void Update () {
			
		if (otherTransform != null) {
			if (!otherTransform.gameObject.activeInHierarchy) {
				image.enabled = false;
			} else {
				image.enabled = true;
			}
		}


		if (goalFlag != null) {

			if (goalFlag.deathEventAchieved) {
				//image.enabled = false;

				Pivot.SetActive (false);
				goalFlag = null;
				faceToObject.target = null;

			}

		}

		if (otherRender != null && timeEvent != null) {
			if (otherRender.isVisible) {
				image.enabled = false;
				timeEvent.StopTimer ();
				timeEvent.ResetTimer ();
				timeEvent.gameObject.SetActive (false);
			} else {
				image.enabled = true;
				timeEvent.gameObject.SetActive (true);
				timeEvent.StartTimer ();

			}
		}
	}





}
