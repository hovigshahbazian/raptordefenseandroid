using UnityEngine;
using System.Collections;

public class SoundOff : MonoBehaviour {

    public Renderer rend;


	void OnMouseUpAsButton () {
		MM.soundvolumechange=true;
		MM.soundvolumechangevalue=0;
   // 	Settings.hSliderValue2=0;
	}

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();

	}
	
	// Update is called once per frame
	void Update () {
		if (Settings.active==true)
			rend.enabled = true;
		else
			rend.enabled = false;

	
	}
}
