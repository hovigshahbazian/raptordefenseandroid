﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemiesDestroyedUI : MonoBehaviour {
	
	public Text numbers;
	public int GoalNumber;

	// Use this for initialization
	void Start () {

		if (GoalNumber == 0) {
			numbers.text = GM.enemyunits_Air.ToString ();
		} else {
			numbers.text = GM.enemyunits_Air + " / " + GoalNumber.ToString ();
		}
	}

	// Update is called once per frame
	void Update () {

		if (GoalNumber == 0) {
			numbers.text = GM.enemyunits_Air.ToString ();
		} else {
			numbers.text = GM.enemyunits_Air + " / " + GoalNumber.ToString();
		}

	}
}
