﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DiamondHighlight : MonoBehaviour {

	public Image[]  images;

	public string EventName;
	public Animator yellowDiamonListAnim;

	int diamondscounter = 0;

	// Use this for initialization
	void Start () {
		EventManager.StartListening (EventName, HighlightDiamond);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void HighlightDiamond(){

		Color color = new Color (255f, 255f, 255f, 255f);

		images [diamondscounter].color = color;


		if (diamondscounter < images.Length - 1) {
			diamondscounter++;
			yellowDiamonListAnim.SetInteger ("YellowDiamondCount",diamondscounter);
		}


	}
}
