﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventRandomSound : MonoBehaviour {

	public AudioSource[] sounds;
	public GameObject[] Dialoagues;

	public string eventName;
	bool AbletoPlay;
	public bool eventSoundIsPlaying = false;
	public int selectedsound = 0;

	private int previousSound = 0;

	void OnEnable(){
		if (sounds != null && !sounds [selectedsound].isPlaying) {
			EventManager.StartListening (eventName, PlaySound);
		}
		//Debug.Log (sounds.Length);
	}
	// Use this for initialization
	void Start () {
		


		if (sounds != null && !sounds [selectedsound].isPlaying) {
			EventManager.StartListening (eventName, PlaySound);
		}
		
	}



	void PlaySound(){

		selectedsound = Random.Range (0, sounds.Length);

		//if previosu sound similar to previous sound
		if (selectedsound == previousSound) {
			selectedsound = (selectedsound + 1) % sounds.Length;
		} 
			

		previousSound = selectedsound;

		eventSoundIsPlaying = false;

		foreach (AudioSource a in sounds) {
			
			if(a.isPlaying){
				eventSoundIsPlaying = true;
			}

		}

		if(!sounds[selectedsound].isPlaying && !eventSoundIsPlaying){
			sounds[selectedsound].Play ();
		}




		if (Dialoagues.Length > 0) {
			//Dialoagues [selectedsound].SetActive (true);
			/*
			if (Dialoagues [selectedsound].GetComponent<Animator> ()) {
			//	Dialoagues [selectedsound].GetComponent<Animator> ().SetTrigger ("Open");
			}
*/
		}

	}

	public void Pause(){

		if(sounds[selectedsound].isPlaying){
			AbletoPlay = true;
		}

		sounds[selectedsound].Pause();

	}

	public void Play(){
		//Debug.Log ("playing sound");

		if(!sounds[selectedsound].isPlaying && AbletoPlay){
			//selectedsound = Random.Range (0, sounds.Length);

			/*if (selectedsound == previousSound) {
				selectedsound = (selectedsound + 1) % sounds.Length;
			} 

			previousSound = selectedsound;*/


			sounds[selectedsound].Play ();
			AbletoPlay = false;
		
		}
	}

	public void Disable(){

		EventManager.StopListening (eventName,PlaySound);
	}


}
