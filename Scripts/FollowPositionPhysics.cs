﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPositionPhysics : MonoBehaviour {

	//public Transform followTransform;
	public Vector3 offset;
	// Use this for initialization


	public bool ignoreX;
	public bool ignoreY;
	public bool ignoreZ;

	Vector3 followPos;
	Vector3 myPos;

	Transform myTransform;
	public Rigidbody2D followRigidBody;


	// Use this for initialization
	void Start () {
		myTransform = transform;

	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (ignoreX && ignoreY && ignoreZ) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, myTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreX && ignoreY) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, myTransform.position.y + offset.y, 0 + offset.z);
		} else if (ignoreX && ignoreZ) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, followRigidBody.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreY && ignoreZ) {
			myTransform.position = new Vector3 (followRigidBody.position.x + offset.x, myTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreX) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, followRigidBody.position.y + offset.y, 0 + offset.z);
		} else if (ignoreY) {
			myTransform.position = new Vector3 (followRigidBody.position.x + offset.x, myTransform.position.y + offset.y, 0 + offset.z);
		} else if (ignoreZ) {
			myTransform.position = new Vector3 (followRigidBody.position.x + offset.x, followRigidBody.position.y + offset.y, myTransform.position.z + offset.z);
		} else {
			myTransform.position = new Vector3 (followRigidBody.position.x + offset.x, followRigidBody.position.y + offset.y, 0 + offset.z);
		}


	}
}
