﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipUpgradeCollectable : MonoBehaviour {
	
	//private PlayerGameData playerData;
	public SpriteRenderer upgradeSprite;
	public AudioSource CollectSound;

	private Collider2D collider2d;
	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		CollectSound = GetComponent<AudioSource> ();
		upgradeSprite = GetComponent<SpriteRenderer> ();
		collider2d = GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	void OnTriggerEnter2D(Collider2D col){

//		PlayerPowerState pps = col.gameObject.GetComponent<PlayerPowerState> ();

		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();
		collider2d.enabled = false;


		if (cd != null) {
			//cd.SetShipUpgradeText ();
			cd.PlayCargoAnim (CollectDisplay.DisplayEvent.ShipUpgrade);

		}

		GameDataManager.instance.LoadedTempPlayerData.GainShipUpgrade ();

		//GameDataManager.instance.Save ();

		EventManager.TriggerEvent ("UpgradeCollected");
		upgradeSprite.enabled = false;
		CollectSound.Play ();
	}



}
