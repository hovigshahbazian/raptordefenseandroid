﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CargoTimer : MonoBehaviour {

	//private PlayerGameData playerData;
	public bool CargoReady;

	public NumberUI secondsUI;
	public NumberUI minutesUI;


	public float TimeLimit;
	public bool timesUp;
	public string eventName;
	public string message;

	public float timer;
	public int seconds;
	public int  minutes;

	public int cargoLimit;

	// Use this for initialization

	void OnEnable(){


	}

	void Start () {

		//playerData = GameDataManager.instance.loadedPlayerData;
		timer = TimeLimit;

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;


		if (timesUp) {
			CargoReady = true;

		} else {
			CargoReady = false;

		}
	
	}

	// Update is called once per frame
	void Update () {



		if(!timesUp){
			timer -= Time.deltaTime;

			if(timer <=0.0f){
				timesUp = true;
				CargoReady = true;
				//cargoButton.interactable = true;


				if(eventName != "")
					EventManager.TriggerEvent(eventName);

				if (message != "")
					SendMessage (message);

			}
		}

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;


		if (timesUp) {
			CargoReady = true;
			secondsUI.gameObject.SetActive (false);
			//Debug.Log("Timer off");
		} else {
			CargoReady = false;
			secondsUI.gameObject.SetActive (true);
			//Debug.Log("Timer on");
		}


		GameDataManager.instance.LoadedTempPlayerData.TimeUntilCargoReplenish = timer;

	}

	public float GetTimeLeft(){
		return timer;
	}

	void DisableTimer(){
		timesUp = true;
	}




	void OnDisable(){
		timesUp = true;
		timer = TimeLimit;
	}


	public void RestartTimer(){
		timer = TimeLimit;

	}

	public void ReplenishCargoUses(){
		GameDataManager.instance.LoadedTempPlayerData.CargoBoxUses = cargoLimit;
	}


	public void RestartTimer(float time){
		TimeLimit = time;
		timer = TimeLimit;


		timesUp = false;
	}


	public void StartCargoTimer(float time){
	     
			RestartTimer (time);

	}



	public void ReduceTime(float seconds){
		TimeLimit -= seconds;
		//Debug.Log("Time reduced by " + seconds.ToString());

		if (TimeLimit <= 0) {
			TimeLimit = 0;
			CargoReady = true;
		}
	}

	public void UseCargoBox(CargoTypes cargoType){

		if (timesUp) {

			switch (cargoType) {
			case CargoTypes.Common:
				GameDataManager.instance.LoadedTempPlayerData.RemoveCommonCargoBox();
				break;
			case CargoTypes.Rare:
				GameDataManager.instance.LoadedTempPlayerData.RemoveRareCargoBox();
				break;
			case CargoTypes.Epic:
				GameDataManager.instance.LoadedTempPlayerData.RemoveEpicCargoBox();
				break;
			}


		


			GameDataManager.instance.LoadedTempPlayerData.CargoBoxUses--;
			GameDataManager.instance.LoadedTempPlayerData.CargoBoxUses = Mathf.Clamp(GameDataManager.instance.LoadedTempPlayerData.CargoBoxUses, 0,999) ;


			if (GameDataManager.instance.LoadedTempPlayerData.CargoBoxUses <= 0) {
				CargoReady = false;
				StartCargoTimer (1f);

			}

			GameDataManager.instance.SaveTempPlayer();
		}
	}
		

}
