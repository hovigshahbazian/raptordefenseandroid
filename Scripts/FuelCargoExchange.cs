﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FuelCargoExchange : MonoBehaviour {

	//private PlayerGameData data;
	public GameObject FullFuelUI;
	//public GameObject NeedCargoUi;
	public EventTrigger trigger;
	private Button button;
	public Animator TradeMenuAnim;
	private bool AnimIsPlaying;
	public bool isExchangeButton;


	// Use this for initialization
	void Start () {
		//data = GameDataManager.instance.loadedPlayerData;
		button = GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
		button.onClick.RemoveAllListeners();
	
		if(GameDataManager.instance.LoadedTempPlayerData.CommonCargoBoxes > 0){
			button.interactable = true;

			if(isExchangeButton){
				button.onClick.AddListener(() => TradeFuelCells());
			}
			else{
				button.onClick.AddListener(() => RevealTradeUI());
			}

			trigger.enabled = false;
		}
		else{
			button.interactable = false;
			trigger.enabled = true;
		}

		if (GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells == 8) {
			button.interactable = false;
			if(FullFuelUI != null)
				FullFuelUI.SetActive (true);
		} else {
			if(FullFuelUI != null)
				FullFuelUI.SetActive (false);
		}


		if(AnimIsPlaying){
			button.interactable = false;
		}

	}

	public void SetAnimIsPlaying(bool b){
		AnimIsPlaying = b;
	}


	public void CheckIfFullOnFuel(){

	}


	public void TradeFuelCells(){

		//TradeMenuAnim.SetTrigger("Appear");
		GameDataManager.instance.LoadedTempPlayerData.IncrementFuelCell(2);
		GameDataManager.instance.LoadedTempPlayerData.RemoveCommonCargoBox();
		GameDataManager.instance.SaveTempPlayer();
	}

	public void RevealTradeUI(){
		TradeMenuAnim.SetTrigger("Appear");
	}
}
