﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TotalEnemiesDestroyedUI : MonoBehaviour {
	public Text numbers;
	public int goalnumber;

    //private GM gameMananger;
	void Start () {
     //   gameMananger = GameObject.FindGameObjectWithTag("GameController").GetComponent<GM>();

		numbers.text = GM.enemyunits_Terrain + GM.enemyunits_Air + "/" + goalnumber.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		numbers.text = GM.enemyunits_Terrain + GM.enemyunits_Air + "/" + goalnumber.ToString () ;
	}
}
