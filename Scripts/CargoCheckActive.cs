﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoCheckActive : MonoBehaviour {

	//private PlayerGameData playerdata;

	// Use this for initialization
	void Start () {
		//playerdata = GameDataManager.instance.loadedPlayerData;
		if (GameDataManager.instance.LoadedTempPlayerData.CommonCargoBoxes > 0) {
			gameObject.SetActive (true);
		} else {
			gameObject.SetActive (false);
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (GameDataManager.instance.LoadedTempPlayerData.CommonCargoBoxes > 0) {
			gameObject.SetActive (true);
		} else {
			gameObject.SetActive (false);
		}
	}
}
