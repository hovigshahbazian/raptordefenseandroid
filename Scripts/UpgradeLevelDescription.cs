﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeLevelDescription : MonoBehaviour {
	
	public Upgrades UpgradeType;
	//private PlayerGameData playerdata;

	public Text CurrentUpgradeLevelText;
	public Text NextUpgradeLevelText;


	// Use this for initialization
	void Start () {
		//playerdata = GameDataManager.instance.loadedPlayerData;
		UpdateUpgradeDescriptions ();

	}
	
	// Update is called once per frame
	void Update () {
		UpdateUpgradeDescriptions ();
	}


	private void UpdateUpgradeDescriptions(){
		switch (UpgradeType) {
		case Upgrades.Armor:
			int armorlevel = GameDataManager.instance.LoadedTempPlayerData.ArmorLevel;
			CurrentUpgradeLevelText.text = "Armor (Lv" + armorlevel + ") : +" + PlayerUpgradesData.ArmorStrength [armorlevel] + " Health Points";


			if (GameDataManager.instance.LoadedTempPlayerData.ArmorLevel < 5) {
				NextUpgradeLevelText.text = "Next Level(Lv" + (armorlevel+1) + "): +" + PlayerUpgradesData.ArmorStrength [armorlevel+1]  + " Health Points";
			} else {
				NextUpgradeLevelText.text = "Maxed";
			}

			break;
		case Upgrades.Ammo:
			int ammolevel = GameDataManager.instance.LoadedTempPlayerData.AmmoLevel;

			CurrentUpgradeLevelText.text = "Secondary Ammo (Lv" + ammolevel + ") : +" + PlayerUpgradesData.SecondaryWeaponSupply[ammolevel] + " Ammo";


			if (ammolevel < 5) {
				NextUpgradeLevelText.text = "Next Level(Lv" + (ammolevel+1) + "): +" + PlayerUpgradesData.SecondaryWeaponSupply[ammolevel+1]  + " Ammo";
			} else {
				NextUpgradeLevelText.text = "Maxed";
			}

			break;
		case Upgrades.Heat:

			int heatLevel = GameDataManager.instance.LoadedTempPlayerData.HeatLevel;
			CurrentUpgradeLevelText.text = "Heat (Lv" + heatLevel + ") : " + (PlayerUpgradesData.HeatUpRate[heatLevel]*200) + "% Reduction in Heat Build Up when Firing Primary Weapon";


			if (heatLevel < 5) {
				NextUpgradeLevelText.text = "Heat (Lv" + (heatLevel + 1) + ") : " + (PlayerUpgradesData.HeatUpRate[heatLevel+1]*200) + "% Reduction in Heat Build Up when Firing Primary Weapon";
			} else {
				NextUpgradeLevelText.text = "Maxed";
			}

			break;
		case Upgrades.Wings:
			int wingLevel = GameDataManager.instance.LoadedTempPlayerData.WingLevel;
			CurrentUpgradeLevelText.text = "Wing (Lv" + wingLevel + ") : +" + (PlayerUpgradesData.RotationSpeed[wingLevel]) + "% Rotation Speed";


			if (wingLevel < 5) {
				NextUpgradeLevelText.text = "Wing (Lv" + (wingLevel + 1) + ") : +" + (PlayerUpgradesData.RotationSpeed[wingLevel+1]) + "% Rotation Speed";
			} else {
				NextUpgradeLevelText.text = "Maxed";
			}

			break;
		}

	}
}
