﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum Skins { FighterBlue= 0, Camogreen = 1, ArticLeopard = 2, BlackTiger = 3, DesertEagle = 4, FlyingBeagle = 5}

public class CargoMenu : MonoBehaviour {

	//private PlayerGameData playerData;
	//public LevelGameData level01Data;
	//public LevelGameData level02Data;
	//public LevelGameData level03Data;
	//public LevelGameData level04Data;
	//public LevelGameData level05Data;
	//public LevelGameData level06Data;
	//public LevelGameData level07Data;
	//public LevelGameData level08Data;
	//public LevelGameData level09Data;
	//public LevelGameData level10Data;

	public NumberTextUI cargoBoxesUI;


	public Text aircraftText;
	public Text weaponText;

	//private int PowerUpLimit = 99;

	public NumberUI FullAutoUI;
	public NumberUI MultishotUI;
	public NumberUI LaserbeamUI;
	public NumberUI ExplodingBulletsUI;


	public NumberUI InvulnerabilityUI;
	public NumberUI SpeedUPUI; 
	public NumberUI ShieldUI;
	public NumberUI InvisibilityUI;


	public Text weaponTxt;
	public Text aircraftTxt;

	public NumberUI cargoLimitUI;


	[SerializeField]
	private AudioSource usePowerSound;



	[SerializeField]
	private AnimatorSetter animSetter;


	// Use this for initialization
	void Start () {
	
		//playerData = GameDataManager.instance.loadedPlayerData;
		cargoLimitUI.number = GameDataManager.instance.LoadedTempPlayerData.CargoBoxUses;


		weaponTxt.text = GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower.ToString ();

		aircraftTxt.text = GameDataManager.instance.LoadedTempPlayerData.StartingAirPower.ToString ();

		EventManager.StartListening ("RandomCommonPowerUp",GainRandomCommonPowerUP);
		EventManager.StartListening ("RandomRarePowerUp",GainRandomRarePowerUP);
		EventManager.StartListening ("RandomEpicPowerUp",GainRandomEpicPowerUP);
	}
	
	// Update is called once per frame
	void Update () {

		cargoBoxesUI.number = GameDataManager.instance.LoadedTempPlayerData.CommonCargoBoxes;

		cargoLimitUI.number = GameDataManager.instance.LoadedTempPlayerData.CargoBoxUses;

		FullAutoUI.number = GameDataManager.instance.LoadedTempPlayerData.FullAutoPowerUps;
		MultishotUI.number = GameDataManager.instance.LoadedTempPlayerData.MultishotPowerUps;
		LaserbeamUI.number = GameDataManager.instance.LoadedTempPlayerData.LaserbeamPowerUps;
		ExplodingBulletsUI.number = GameDataManager.instance.LoadedTempPlayerData.ExplodingBulletsPowerUps;

		InvulnerabilityUI.number = GameDataManager.instance.LoadedTempPlayerData.InvulnerabilityPowerUps;
		SpeedUPUI.number =GameDataManager.instance.LoadedTempPlayerData.SpeedUpPowerUps;
		InvisibilityUI.number =GameDataManager.instance.LoadedTempPlayerData.InvisibilityPowerUps;
		ShieldUI.number =GameDataManager.instance.LoadedTempPlayerData.ShieldPowerUps;


	}
		

	public void AddWeaponPowerUp(PlayerPowerState.WeaponPower power){

		GameDataManager.instance.LoadedTempPlayerData.GainWeaponPowerUp (power);
		GameDataManager.instance.SaveTempPlayer();
	}

	public void AddWeaponPowerUp(PlayerPowerState.WeaponPower power,int n){


		GameDataManager.instance.LoadedTempPlayerData.GainWeaponPowerUps (power, n);
		GameDataManager.instance.SaveTempPlayer();
	}

	public void AddAircraftPowerUp(PlayerPowerState.AircraftPower power){

		GameDataManager.instance.LoadedTempPlayerData.GainAircraftPowerUp (power);
		GameDataManager.instance.SaveTempPlayer();
	}
		
	public void AddAircraftPowerUp(PlayerPowerState.AircraftPower power, int n){

		GameDataManager.instance.LoadedTempPlayerData.GainAircraftPowerUps (power, n);
		GameDataManager.instance.SaveTempPlayer();


	}

	public void AddUpgrades(int num){

		GameDataManager.instance.LoadedTempPlayerData.GainShipUpgrades(num);
		GameDataManager.instance.SaveTempPlayer();
	}

	public void GainSkin(){

		//Create a list of Skins that have not already been unlocked
		//List<Skins> skins = new List<Skins>();
		/*
		if (!playerData.FighterBlueSkin) {
			skins.Add (Skins.FighterBlue);
		}

		if (!playerData.CamoGreenSkin) {
			skins.Add (Skins.Camogreen);
		}

		if (!playerData.ArticLeopardSkin) {
			skins.Add (Skins.ArticLeopard);
		}
			
		if (!playerData.BlackTigerSkin) {
			skins.Add (Skins.BlackTiger);
		}

		if (!playerData.DesertEagleSkin) {
			skins.Add (Skins.DesertEagle);
		}
			
		if (!playerData.FlyingBeagleSkin) {
			skins.Add (Skins.FlyingBeagle);
		}*/

		int randomNum = Random.Range (0, 6);

		//randomNum = 5;

		switch (randomNum) {
		case (int)Skins.FighterBlue:

			if (!GameDataManager.instance.LoadedTempPlayerData.FighterBlueSkin) {
				GameDataManager.instance.LoadedTempPlayerData.FighterBlueSkin = true;
				animSetter.triggerName = "OpenChestSkinB";
				//Debug.Log ("Got FighterBlue");
			} else {;
				animSetter.triggerName = "OpenChestUpgrade";
				//Debug.Log ("You Already Have Fighter Blue");
				AddUpgrades (20);
			}
				break;
		case (int)Skins.Camogreen:
			if (!GameDataManager.instance.LoadedTempPlayerData.CamoGreenSkin) {
				GameDataManager.instance.LoadedTempPlayerData.CamoGreenSkin = true;
				animSetter.triggerName = "OpenChestSkinC";
				//Debug.Log ("Got Camogreen");

			} else {
				animSetter.triggerName = "OpenChestUpgrade";
				//Debug.Log ("You Already Have Camo Green");
				AddUpgrades (20);
			}
				break;
		case (int)Skins.ArticLeopard:
			if (!GameDataManager.instance.LoadedTempPlayerData.ArticLeopardSkin) {
				GameDataManager.instance.LoadedTempPlayerData.ArticLeopardSkin = true;
				animSetter.triggerName = "OpenChestSkinD";
				//Debug.Log ("Got Artic Leopard");
			} else {
				animSetter.triggerName = "OpenChestUpgrade";
				//Debug.Log ("You Already Have Artic Leopard");
				AddUpgrades (20);
			}
				break;
		case (int)Skins.BlackTiger:
			if (!GameDataManager.instance.LoadedTempPlayerData.BlackTigerSkin) {
				GameDataManager.instance.LoadedTempPlayerData.BlackTigerSkin = true;
				animSetter.triggerName = "OpenChestSkinE";
				//Debug.Log ("Got Black Tiger");
			} else {
				animSetter.triggerName = "OpenChestUpgrade";
				//Debug.Log ("You Already Have Black Tiger");
				AddUpgrades (20);
			}
				break;
		case (int)Skins.DesertEagle:
			if (!GameDataManager.instance.LoadedTempPlayerData.DesertEagleSkin) {
				GameDataManager.instance.LoadedTempPlayerData.DesertEagleSkin = true;
				animSetter.triggerName = "OpenChestSkinF";
				//Debug.Log ("Got Desert Eagle");
			} else {
				animSetter.triggerName = "OpenChestUpgrade";
				//Debug.Log ("You Already Have Desert Eagle");
				AddUpgrades (20);
			}
				break;
		case (int)Skins.FlyingBeagle:
			if (!GameDataManager.instance.LoadedTempPlayerData.FlyingBeagleSkin) {
				GameDataManager.instance.LoadedTempPlayerData.FlyingBeagleSkin = true;
				animSetter.triggerName = "OpenChestSkinG";
				//Debug.Log ("Got Flying Beagle");
			} else {
				animSetter.triggerName = "OpenChestUpgrade";
				//Debug.Log ("You Already Have Flying Beagle");
				AddUpgrades (20);
			}
				break;
		}






	}

	 public void ApplyWeaponPowerUp(PlayerPowerState.WeaponPower power){
		GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower = power;
		RemoveWeaponPowerUp (power);
		GameDataManager.instance.SaveTempPlayer();
	}

	 public void ApplyAircraftPowerUp(PlayerPowerState.AircraftPower power){
		GameDataManager.instance.LoadedTempPlayerData.StartingAirPower = power;
		RemoveAircraftPowerUp (power);
		GameDataManager.instance.SaveTempPlayer();
	}
		
	public void RemoveWeaponPowerUp(PlayerPowerState.WeaponPower power){
		GameDataManager.instance.LoadedTempPlayerData.RemoveWeaponPowerUp (power);
		GameDataManager.instance.SaveTempPlayer();
	}

	public void RemoveAircraftPowerUp(PlayerPowerState.AircraftPower power){
		GameDataManager.instance.LoadedTempPlayerData.RemoveAircraftPowerUp (power);
		GameDataManager.instance.SaveTempPlayer();
	}


	
	public void SetWeaponPower(PlayerPowerState.WeaponPower power){


		usePowerSound.Play ();
		switch (power) {

		case PlayerPowerState.WeaponPower.FullAuto:

			if (GameDataManager.instance.LoadedTempPlayerData.FullAutoPowerUps > 0) {
				GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower = power;
				weaponTxt.text = power.ToString ();
			}


			break;
		case PlayerPowerState.WeaponPower.Multishot:
			
			if (GameDataManager.instance.LoadedTempPlayerData.FullAutoPowerUps > 0) {
				GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower = power;
				weaponTxt.text = power.ToString ();
			}

			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			
			GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower = power;
			weaponTxt.text = power.ToString ();

			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			
			GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower = power;
			weaponTxt.text = power.ToString ();

			break;
		default:
			break;

		}

	}

	public void SetAircraftPower(PlayerPowerState.AircraftPower power){
		
		usePowerSound.Play ();

		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			GameDataManager.instance.LoadedTempPlayerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			GameDataManager.instance.LoadedTempPlayerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;
		case PlayerPowerState.AircraftPower.Shield:
			GameDataManager.instance.LoadedTempPlayerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			GameDataManager.instance.LoadedTempPlayerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;

		default:
			break;

		}

	}

	public void AssignAnimatorCargo( AnimatorSetter CargoboxAnim){
		animSetter = CargoboxAnim;
	}


	public void GainRandomCommonPowerUP(){

		int powerupNum = Random.Range (0, 9);


		switch (powerupNum) {
			case 8:
				AddUpgrades (10);
			    animSetter.triggerName = "OpenChestUpgrade";
				break;
			case 7:
				animSetter.triggerName = "OpenChestFullAuto";
				AddWeaponPowerUp (PlayerPowerState.WeaponPower.FullAuto);

				break;
			case 6:
				animSetter.triggerName = "OpenChestMultiShot";
				AddWeaponPowerUp (PlayerPowerState.WeaponPower.Multishot);
			  
				break;
			case 5:
				animSetter.triggerName = "OpenChestLazerBeam";
				AddWeaponPowerUp (PlayerPowerState.WeaponPower.Laserbeam);
			 
				break;
			case 4:
				animSetter.triggerName = "OpenChestExplodeShot";
				AddWeaponPowerUp (PlayerPowerState.WeaponPower.ExplodingBullets);
			 
				break;
			case 3:
				animSetter.triggerName = "OpenChestInvulnerability";
				AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invulnerability);
			    
				break;
			case 2:
				animSetter.triggerName = "OpenChestSpeedUp";
				AddAircraftPowerUp (PlayerPowerState.AircraftPower.SpeedUp);
	
				break;
			case 1:
				animSetter.triggerName = "OpenChestSheild";
				AddAircraftPowerUp (PlayerPowerState.AircraftPower.Shield);

				break;
			case 0:
				animSetter.triggerName = "OpenChestInvisibility";
				AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invisibility);

				break;
			default:
				break;

		}

		animSetter.SetTrigger ();

	}
		


	public void GainRandomRarePowerUP(){

		int powerupNum = Random.Range (0, 20);

		switch (powerupNum) {
		case 19:
			GainSkin ();
			break;
		case 18:
		case 8:
			animSetter.triggerName = "OpenChestUpgrade";
			AddUpgrades (50);
			break;
		case 17:
		case  7:
			animSetter.triggerName = "OpenChestFullAuto";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.FullAuto,5);
			break;
		case 16:
		case  6:
			animSetter.triggerName = "OpenChestMultiShot";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.Multishot,5);
			break;
		case 15:
		case  5:
			animSetter.triggerName = "OpenChestLazerBeam";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.Laserbeam,5);
			break;
		case 14:
		case  4:
			animSetter.triggerName = "OpenChestExplodeShot";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.ExplodingBullets,5);
			break;
		case 13:
		case  3:
			animSetter.triggerName = "OpenChestInvulnerability";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invulnerability,5);
			break;
		case 12:
		case  2:
			animSetter.triggerName = "OpenChestSpeedUp";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.SpeedUp,5);
			break;
		case 11:
		case  1:
			animSetter.triggerName = "OpenChestSheild";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.Shield,5);
			break;
		case 10:
		case  0:
			animSetter.triggerName = "OpenChestInvisibility";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invisibility,5);
			break;
		default:
			break;

		}

		animSetter.SetTrigger ();

	}



	public void GainRandomEpicPowerUP(){
		int powerupNum = Random.Range (0, 10);
	
		switch (powerupNum) {
		case 9:


			GainSkin ();


			break;
		case 8:
			animSetter.triggerName = "OpenChestUpgrade";
			AddUpgrades (200);
			break;
		case 7:
			animSetter.triggerName = "OpenChestFullAuto";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.FullAuto,20);
			break;
		case 6:
			animSetter.triggerName = "OpenChestMultiShot";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.Multishot,20);
			break;
		case 5:
			animSetter.triggerName = "OpenChestLazerBeam";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.Laserbeam,20);
			break;
		case 4:
			animSetter.triggerName = "OpenChestExplodeShot";
			AddWeaponPowerUp (PlayerPowerState.WeaponPower.ExplodingBullets,20);
			break;
		case 3:
			animSetter.triggerName = "OpenChestInvulnerability";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invulnerability,20);
			break;
		case 2:
			animSetter.triggerName = "OpenChestSpeedUp";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.SpeedUp,20);
			break;
		case 1:
			animSetter.triggerName = "OpenChestSheild";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.Shield,20);
			break;
		case 0:
			animSetter.triggerName = "OpenChestInvisibility";
			AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invisibility,20);
			break;
		default:
			break;

		}

		animSetter.SetTrigger ();

	}



}
