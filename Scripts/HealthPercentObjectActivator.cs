﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class HealthPercentObjectActivator : MonoBehaviour {


	public GameObject[] objects;
	private Health health;
	[SerializeField]
	private float curHeathPer;
	[SerializeField]
	private float percentDivider;
	[SerializeField]
	private int counter = 0 ;
	// Use this for initialization
	void Start () {
		health = GetComponent<Health>();

		percentDivider = 1f/(float)objects.Length;
	}
	
	// Update is called once per frame
	void Update () {
		


	}


	void UpdateObjects(){
		curHeathPer = ( 1f - ((float)health.HealthPoints/(float)health.MaxHealth));

		if(curHeathPer >= ((counter+1)*percentDivider) && ((counter+1)*percentDivider) < 1){
			objects[counter].SetActive(true);
			counter++;
		}
			
	}
}
