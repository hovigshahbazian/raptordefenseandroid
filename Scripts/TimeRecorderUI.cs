﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeRecorderUI : MonoBehaviour {


	public Text timeText;
	public bool TimeRecordbeat;
	private float curTime = 0;
	private int minutes = 0;
	private int secs = 0;
	private int millisecs = 0;

	[SerializeField]
	private bool Playing;

	public bool IsPlaying{
		get{ return Playing; }
		set{ Playing = value; }
	}



	// Use this for initialization
	void Start () {
		EventManager.StartListening ("PlayerDeath", Pause);
	}
	
	// Update is called once per frame
	void Update () {

		if (Playing) {

			curTime += Time.deltaTime;

			secs = (int)curTime % 60;
			minutes = (int)curTime / 60;


			millisecs = (int)(curTime * 100) % 100;

			timeText.text = string.Format("{0:00}:{1:00}:{2:00}",minutes,secs,millisecs);
		}
	}

	public void Pause(){
		Playing = false;
	}
	public void Resume(){
		Playing = true;
	}


	public void Play(){
		Reset ();
		Playing = true;
	}

	public void Reset(){
		curTime = 0;
	}

	public int GetTimeSecs(){
		return (int)curTime;
	}

	public float GetTimeSecsF(){
		return curTime;
	}
}
