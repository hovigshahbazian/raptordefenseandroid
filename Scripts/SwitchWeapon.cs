﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchWeapon : MonoBehaviour {

	public SecondaryWeapon secWeapon1;
	public SecondaryWeapon secWeapon2;

	public Sprite weapon1Spr;
	public Sprite weapon2Spr;

	public Image secWeaponImage;

	// Use this for initialization
	void Start () {
		SetWeaponTwo ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void SetWeaponOne(){
		
		secWeapon1.Controlled = true;
		secWeaponImage.overrideSprite = weapon1Spr;
		secWeapon2.Controlled = false;

	}

	public void SetWeaponTwo(){
		
		secWeapon1.Controlled = false;
		secWeaponImage.overrideSprite = weapon2Spr;
		secWeapon2.Controlled = true;
	}






}
