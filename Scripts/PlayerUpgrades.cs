﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets._2D;

public class PlayerUpgrades : MonoBehaviour {


	//public PlayerGameData playerData;
	public HeatGuage heat;
	public Health health;
	public PlatformerCharacter2D player;
	public SecondaryWeapon secWeapon;

	// Use this for initialization
	void Start () {


		health.MaxHealth += PlayerUpgradesData.ArmorStrength[GameDataManager.instance.LoadedTempPlayerData.ArmorLevel];

		health.HealthPoints = health.MaxHealth;

		GM.secondary_weapon_uses += ( PlayerUpgradesData.SecondaryWeaponSupply[GameDataManager.instance.LoadedTempPlayerData.AmmoLevel]);

		if(secWeapon != null){
			secWeapon.effectSpawnRate += PlayerUpgradesData.SecondaryFireSpeed[GameDataManager.instance.LoadedTempPlayerData.AmmoLevel];
		}

		player.rotationSpeed += (PlayerUpgradesData.RotationSpeed[GameDataManager.instance.LoadedTempPlayerData.WingLevel]);


		heat.OverHeatTime -= (PlayerUpgradesData.CoolDownTime[GameDataManager.instance.LoadedTempPlayerData.HeatLevel]);

		heat.maxHeat -= (PlayerUpgradesData.HeatUpRate[GameDataManager.instance.LoadedTempPlayerData.HeatLevel]);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
