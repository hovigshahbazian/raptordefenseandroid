﻿using UnityEngine;
using System.Collections;

public class CargoBoxCollectable : MonoBehaviour {

	//private PlayerGameData playerData;
	public SpriteRenderer cargoSprite;
	public AudioSource CollectSound;
	private Collider2D collider2d;
	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		CollectSound = GetComponent<AudioSource> ();
		cargoSprite = GetComponent<SpriteRenderer> ();
		collider2d = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){

	//	PlayerPowerState pps = col.gameObject.GetComponent<PlayerPowerState> ();
		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();

		collider2d.enabled = false;
		//cd.SetCargoBoxText ();
		cd.PlayCargoAnim (CollectDisplay.DisplayEvent.CargoBox);
	//	Debug.Log ("Cargo box");

		GameDataManager.instance.LoadedTempPlayerData.GainCommonCargoBox();
		EventManager.TriggerEvent ("CargoBoxCollected");

		//GameDataManager.instance.Save ();

		cargoSprite.enabled = false;
		CollectSound.Play ();
	}
}
