﻿using UnityEngine;
using System.Collections;


public class EventMessage : MonoBehaviour {

	public bool onEnabled = true;
	public string Message;
	public string EventToListen;
	public string EventToTrigger;

	void OnEnable(){
		if (onEnabled) {
			if (EventToListen != "")
				EventManager.StartListening (EventToListen, TriggerMessage);
		}
	}


	// Use this for initialization
	void Start () {
	
		if(EventToListen != "")
			EventManager.StartListening (EventToListen,TriggerMessage);

	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void TriggerMessage(){

		if(Message != "")
			this.SendMessage (Message);

		if (EventToTrigger != "") 
			EventManager.TriggerEvent (EventToTrigger);
		

	}

	void OnDisable(){
		EventManager.StopListening (EventToListen,TriggerMessage);
	}
}
