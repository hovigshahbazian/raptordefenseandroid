﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDataEraseBTN : MonoBehaviour {


	private Button playerDataEraseBtn;

	// Use this for initialization
	void Start () {
		playerDataEraseBtn = GetComponent<Button> ();

		playerDataEraseBtn.onClick.AddListener (() => ErasePlayerDataClick());
	}
	



	public void ErasePlayerDataClick(){
	//	Debug.Log ("Erased playerData");
		//GameDataManager.instance.ErasePlayerData ();
	}


}
