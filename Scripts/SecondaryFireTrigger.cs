﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryFireTrigger : MonoBehaviour {

	public SecondaryWeapon secWeapon;
	[SerializeField]
	private bool activated;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnTriggerEnter2D(Collider2D col){

		if (!activated) {
			secWeapon = col.transform.Find ("SecondaryWeaponAlt").GetComponent<SecondaryWeapon> ();
			secWeapon.FireSecondaryWeaponAlt ();
			activated = true;
		}


	}


	void OnCollisionEnter2D(Collision2D other){




	}



}
