﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShowLeaderBoardBTN : MonoBehaviour {

	private Button btn;
	// Use this for initialization
	void Start () {
		btn = GetComponent<Button> ();


		btn.onClick.AddListener (() => GooglePlayGameScript.OnShowLeaderBoard());
	
	}
	
	// Update is called once per frame





}
