﻿using UnityEngine;
using System.Collections;

public class MotherShipAI : MonoBehaviour {
	public Animator BossHealthAnim;
	ForwardMovement movescript;
	Health health;
	public float reverseTime;
	public bool reversing;
	float reverseTimer;
	public int damageTakenbyEmp;
	public AudioSource EmpDamagesound;

	Collider2D shipCollider;

	void Start () {
		movescript = GetComponent<ForwardMovement> ();
		reverseTimer = reverseTime;
		health = GetComponent<Health> ();
		shipCollider = GetComponent<Collider2D> ();
	}
	

	void Update () {
		if (reversing) {
			reverseTimer -= Time.deltaTime;

			if (reverseTimer <= 0f) {
				reversing = false;
				reverseTimer = reverseTime - 2f;
				movescript.speed *= -1;
				shipCollider.enabled = true;
			}
		}





	}
		
	void OnTriggerEnter2D(Collider2D col){
		
		if (col.gameObject.tag == "Emp") {

			if (health.isAlive) {

				if(EmpDamagesound){
					EmpDamagesound.Play();
				}

				if (health.HealthPoints > damageTakenbyEmp) {
					reversing = true;
					movescript.speed *= -1;
				}
			} 


			health.Damage (damageTakenbyEmp);
			BossHealthAnim.SetTrigger ("Damage");
			shipCollider.enabled = false;

			if (health.HealthPoints > 0) {
				EventManager.TriggerEvent ("DamageMotherShip");
			}

			//Debug.Log ("ShipDamaged");
		}


	}


}
