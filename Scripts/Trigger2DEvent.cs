﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger2DEvent : MonoBehaviour {


	public bool EnterEventActive;
	public bool ExitEventActive;
	public string enterEvent;
	public string exitEvent;
	public string EnterMessage;
	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public void OnTriggerEnter2D(Collider2D other){
		if (EnterEventActive) {
			EventManager.TriggerEvent (enterEvent);
		}
		//Debug.Log ("EnterTriggered");
		if (EnterMessage != "") {
			SendMessage(EnterMessage);
		}

	}

	public void OnTriggerExit2D(Collider2D other){
		if (ExitEventActive) {
			EventManager.TriggerEvent (exitEvent);
		}
	}

}
