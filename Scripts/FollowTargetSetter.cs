﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class FollowTargetSetter : MonoBehaviour {
	
	public FollowTarget followscript;
	public string targetTag;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter2D(Collider2D col){

		if (col.gameObject.tag == targetTag) {
			followscript.target = col.gameObject.transform;
		}
	}
}
