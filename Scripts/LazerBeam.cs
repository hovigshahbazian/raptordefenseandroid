﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerBeam : MonoBehaviour {

	public GameObject ChargeUpAnim;
	public GameObject LazerbeamAnim;
	public AudioSource chargesound;
	public AudioSource lazerSound;
	public AudioSource ContLazerSound;

	public float ChargeTime;
	private float Chargetimer;
	public bool Charging;

	// Use this for initialization
	void Start () {
		Chargetimer = ChargeTime;
	}
	
	// Update is called once per frame
	void Update () {

		if(Charging){
			Chargetimer -= Time.deltaTime;

			if(Chargetimer <= 0){
				Charging = false;
				FireLazer();
				Chargetimer = ChargeTime;
			}

		}
	}


	public void ChargeUp(){
		ChargeUpAnim.SetActive(true);
		Charging = true;
		chargesound.Play();
	}


	public void FireLazer(){
		ChargeUpAnim.SetActive(false);
		LazerbeamAnim.SetActive(true);
		lazerSound.Play();
		ContLazerSound.Play();
	}


	public void StopLazer(){
		ContLazerSound.Stop();
		ChargeUpAnim.SetActive(false);
		LazerbeamAnim.SetActive(false);
		Charging = false;
		Chargetimer = ChargeTime;
	}


}
