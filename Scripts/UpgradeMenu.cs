﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class UpgradeMenu : MonoBehaviour {
	//private PlayerGameData playerData;


	//public Text armorLevelTxt;
	//public Text wingLevelTxt;
	//public Text ammoLevelTxt;
	//public Text heatLevelTxt;
	public Text upgradesAmt;

	public LevelMeter levelMeterArmor;
	public LevelMeter levelMeterAmmo;
	public LevelMeter levelMeterHeat;
	public LevelMeter levelMeterWing;


	public Text AmountRequiredArmor;
	public Text AmountRequiredAmmo;
	public Text AmountRequiredHeat;
	public Text AmountRequiredWing;

	public Button armorUpgradeBtn;
	public Button ammoUpgradeBtn;
	public Button heatUpgradeBtn;
	public Button wingUpgradeBtn;


	public bool heatUpgradeAble;
	public bool ammoUpgradeAble;
	public bool armorUpgradeAble;
	public bool wingUpgradeAble;

	public int[] levelRequirments;
	[SerializeField]
	private AudioSource upgradeSound;
	[SerializeField]
	private AudioSource declineSound;

	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		armorUpgradeBtn.onClick.AddListener (() => Upgrade(Upgrades.Armor));
		ammoUpgradeBtn.onClick.AddListener (() => Upgrade (Upgrades.Ammo));
		heatUpgradeBtn.onClick.AddListener (() => Upgrade (Upgrades.Heat));
		wingUpgradeBtn.onClick.AddListener (() => Upgrade (Upgrades.Wings));

		int shipUp = GameDataManager.instance.LoadedTempPlayerData.shipUpgrades;
		int armorLevel = GameDataManager.instance.LoadedTempPlayerData.ArmorLevel;
		int ammoLevel = GameDataManager.instance.LoadedTempPlayerData.AmmoLevel;
		int heatLevel = GameDataManager.instance.LoadedTempPlayerData.HeatLevel;
		int wingLevel = GameDataManager.instance.LoadedTempPlayerData.WingLevel;



		AmountRequiredArmor.text = shipUp + "/" + levelRequirments [armorLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredAmmo.text = shipUp + "/" + levelRequirments [ammoLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredHeat.text = shipUp + "/" + levelRequirments [heatLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredWing.text = shipUp + "/" + levelRequirments [wingLevel - 1].ToString () + " needed for next upgrade";

		//armorLevelTxt.text = armorLevel.ToString ();
		//ammoLevelTxt.text = ammoLevel.ToString ();
		//heatLevelTxt.text = heatLevel.ToString ();
		//wingLevelTxt.text = wingLevel.ToString ();




		//levelMeterArmor.SetUpLevelMeter(playerData.ArmorLevel);
		//levelMeterAmmo.SetUpLevelMeter(playerData.AmmoLevel);
		//levelMeterHeat.SetUpLevelMeter (playerData.HeatLevel);
		//levelMeterWing.SetUpLevelMeter (playerData.WingLevel);


		upgradesAmt.text = shipUp.ToString ();


		if (armorLevel >= 5) {
			armorUpgradeBtn.onClick.RemoveAllListeners ();
			armorUpgradeBtn.interactable = false;
			AmountRequiredArmor.text = "MAX";
		}

		if (wingLevel >= 5) {
			wingUpgradeBtn.onClick.RemoveAllListeners ();
			wingUpgradeBtn.interactable = false;
			AmountRequiredWing.text = "MAX";
		}
			
		if (ammoLevel >= 5) {
			ammoUpgradeBtn.onClick.RemoveAllListeners ();
			ammoUpgradeBtn.interactable = false;
			AmountRequiredAmmo.text = "MAX";
		}

		if (heatLevel >= 5) {
			heatUpgradeBtn.onClick.RemoveAllListeners ();
			heatUpgradeBtn.interactable = false;
			AmountRequiredHeat.text = "MAX";
		}
	}
	
	// Update is called once per frame
	void Update () {
		//armorLevel.text = playerData.ArmorLevel.ToString ();
		//ammoLevel.text = playerData.AmmoLevel.ToString ();
		//heatLevel.text = playerData.HeatLevel.ToString ();
		//wingLevel.text = playerData.WingLevel.ToString ();
		int shipup = GameDataManager.instance.LoadedTempPlayerData.shipUpgrades;
		int armorLevel = GameDataManager.instance.LoadedTempPlayerData.ArmorLevel;
		int ammoLevel = GameDataManager.instance.LoadedTempPlayerData.AmmoLevel;
		int heatLevel = GameDataManager.instance.LoadedTempPlayerData.HeatLevel;
		int wingLevel = GameDataManager.instance.LoadedTempPlayerData.WingLevel;


		levelMeterArmor.SetUpLevelMeter(GameDataManager.instance.LoadedTempPlayerData.ArmorLevel);
		levelMeterAmmo.SetUpLevelMeter(GameDataManager.instance.LoadedTempPlayerData.AmmoLevel);
		levelMeterHeat.SetUpLevelMeter (GameDataManager.instance.LoadedTempPlayerData.HeatLevel);
		levelMeterWing.SetUpLevelMeter (GameDataManager.instance.LoadedTempPlayerData.WingLevel);

		AmountRequiredArmor.text = shipup + "/" + levelRequirments [armorLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredAmmo.text = shipup + "/" + levelRequirments [ammoLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredHeat.text = shipup + "/" + levelRequirments [heatLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredWing.text = shipup + "/" + levelRequirments [wingLevel - 1].ToString () + " needed for next upgrade";

		upgradesAmt.text = shipup.ToString ();



		if (shipup>= levelRequirments [heatLevel - 1]) {
			heatUpgradeAble = true;
		} else {
			heatUpgradeAble = false;
		}


		if (shipup >= levelRequirments [ammoLevel- 1]) {
			ammoUpgradeAble = true;
		} else {
			ammoUpgradeAble = false;
		}

		if (shipup >= levelRequirments [armorLevel - 1]) {
			armorUpgradeAble = true;
		} else {
			armorUpgradeAble = false;
		}
			
		if (shipup >= levelRequirments [wingLevel - 1]) {
			wingUpgradeAble = true;
		} else {
			wingUpgradeAble = false;
		}

	}


	public void Upgrade( Upgrades upgrade){
		int shipup = GameDataManager.instance.LoadedTempPlayerData.shipUpgrades;
		int armorLevel = GameDataManager.instance.LoadedTempPlayerData.ArmorLevel;
		int ammoLevel = GameDataManager.instance.LoadedTempPlayerData.AmmoLevel;
		int heatLevel = GameDataManager.instance.LoadedTempPlayerData.HeatLevel;
		int wingLevel = GameDataManager.instance.LoadedTempPlayerData.WingLevel;

		switch (upgrade) {
		case Upgrades.Armor:

			if (shipup >= levelRequirments [armorLevel - 1]) {
				GameDataManager.instance.LoadedTempPlayerData.RemoveShipUpgrades (levelRequirments [armorLevel - 1]);
				GameDataManager.instance.LoadedTempPlayerData.ArmorLevel++;
				upgradeSound.Play ();
			} else {
				declineSound.Play ();
			}
			break;
		case Upgrades.Wings:
			if (shipup >= levelRequirments [wingLevel - 1]) {
				GameDataManager.instance.LoadedTempPlayerData.RemoveShipUpgrades(levelRequirments [wingLevel - 1]);
				GameDataManager.instance.LoadedTempPlayerData.WingLevel++;
				upgradeSound.Play ();
			}else
			{
				declineSound.Play ();
			}
			break;
		case Upgrades.Ammo:

			if (shipup >= levelRequirments [ammoLevel - 1]) {
				GameDataManager.instance.LoadedTempPlayerData.RemoveShipUpgrades(levelRequirments [ammoLevel - 1]);
				GameDataManager.instance.LoadedTempPlayerData.AmmoLevel++;
				upgradeSound.Play ();
			} else {
				declineSound.Play ();
			}
			break;
		case Upgrades.Heat:

			if (shipup >= levelRequirments [heatLevel - 1]) {
				GameDataManager.instance.LoadedTempPlayerData.RemoveShipUpgrades(levelRequirments [heatLevel - 1]);
				GameDataManager.instance.LoadedTempPlayerData.HeatLevel++;
			
				upgradeSound.Play ();
			} else {
				declineSound.Play ();

			}
			break;
		default:
			break;
		}

		GameDataManager.instance.SaveTempPlayer();


		//armorLevelTxt.text = armorLevel.ToString ();
		//ammoLevelTxt.text = ammoLevel.ToString ();
		//heatLevelTxt.text = heatLevel.ToString ();
		//wingLevelTxt.text = wingLevel.ToString ();


		upgradesAmt.text = shipup.ToString ();

		levelMeterArmor.SetUpLevelMeter(armorLevel);
		levelMeterAmmo.SetUpLevelMeter(armorLevel);
		levelMeterHeat.SetUpLevelMeter (heatLevel);
		levelMeterWing.SetUpLevelMeter (wingLevel);



		AmountRequiredArmor.text = shipup + "/" + levelRequirments [armorLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredAmmo.text = shipup + "/" + levelRequirments [ammoLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredHeat.text = shipup + "/" + levelRequirments [heatLevel - 1].ToString () + " needed for next upgrade";
		AmountRequiredWing.text = shipup + "/" + levelRequirments [wingLevel - 1].ToString () + " needed for next upgrade";




		if (armorLevel >= 5) {
			armorUpgradeBtn.onClick.RemoveAllListeners ();
			AmountRequiredArmor.text = "MAX";
			armorUpgradeBtn.interactable = false;
		}


		if (heatLevel >= 5) {
			heatUpgradeBtn.interactable = false;
			AmountRequiredHeat.text = "MAX";
			heatUpgradeBtn.onClick.RemoveAllListeners ();
		}

		if (ammoLevel >= 5) {
			ammoUpgradeBtn.onClick.RemoveAllListeners ();
			AmountRequiredAmmo.text = "MAX";
			ammoUpgradeBtn.interactable = false;
		}

		if (wingLevel >= 5) {
			wingUpgradeBtn.onClick.RemoveAllListeners ();
			AmountRequiredWing.text = "MAX";
			wingUpgradeBtn.interactable = false;
		}


	}




}
