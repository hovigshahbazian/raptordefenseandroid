﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusLevelBEnd : MonoBehaviour {
	
	public bool StartEnding;
	public GameObject EndingScreen;
	public GM LevelManager;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (StartEnding) {
			if (Fade.fadeingOut == false) {
				Fade.fadeinstart = true;
				StartEnding = false;
				EndingScreen.SetActive (true);
			}
		}
	}


	public void Exit(){
		//Debug.Log ("Exiting");
		GM.Exiting=true;
		GM.completed=false;
		GM.scene="-1";
		CloudsEnd.endlevel=true;
		Fade.fadeingOut = true;
		PlaneEnd.rolltimer=10;

	}



	public void PlayEnding(){
		GM.Exiting=true;
		GM.completed=false;
		GM.scene="-1";
		CloudsEnd.endlevel=true;
		Fade.fadeingOut = true;
		PlaneEnd.rolltimer=10;
		StartEnding = true;
	}


	public void ReturnBackToCampaign(){
		LevelManager.GotoScene("CampaignMap");
	}

}
