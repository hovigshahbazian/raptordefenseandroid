﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class ProximityPlayerSlow : MonoBehaviour {
	PlatformerCharacter2D player;
	public int SpeedReduction = 10;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	void OnTriggerEnter2D(Collider2D col){
		player = col.GetComponent<PlatformerCharacter2D> ();


			if(player != null){
			player.m_MaxSpeed -= SpeedReduction;
			}
	}

	void OnTriggerExit2D(Collider2D col){
		if(player != null){
			player.m_MaxSpeed += SpeedReduction;
		}

	}


}
