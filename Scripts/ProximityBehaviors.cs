﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityBehaviors : MonoBehaviour {

	public MonoBehaviour[] scripts;
	public bool Active;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void  OnTriggerEnter2D(){
		foreach (MonoBehaviour m in scripts){
			m.enabled = Active;
		}
	}

	public void  OnTriggerExit2D(){
		foreach (MonoBehaviour m in scripts){
			m.enabled = !Active;
		}
	}


}
