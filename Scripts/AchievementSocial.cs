﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementSocial : MonoBehaviour {

	private Button btn;

	// Use this for initialization
	void Start () {
		btn = GetComponent<Button> ();


		btn.onClick.AddListener (() => GooglePlayGameScript.OnShowAcheivements());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
