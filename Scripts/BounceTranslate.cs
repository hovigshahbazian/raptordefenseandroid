﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class BounceTranslate : MonoBehaviour {
	
	public float disableTime;
	public float bounceStrength = 10000;
	//public Vector2 BounceDirection;
	//public Health health;
	//public bool isGround = true;
	private bool controlsDisabled = false;
	private float disableTimer;
	private PlatformerCharacter2D pltfrmr;

	private Collider2D col;
	// Use this for initialization
	void Start () {
		disableTimer = disableTime;
		col = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (controlsDisabled) {

			disableTimer -= Time.deltaTime;


			if (disableTimer <= 0.0f) {

				disableTimer = disableTime;
				controlsDisabled = false;
				col.enabled = true;
			}
		}
	}



	void OnTriggerEnter2D(Collider2D othercol){
		//Debug.Log("Trigger bounce");


		pltfrmr = othercol.GetComponent<PlatformerCharacter2D> ();


		if (pltfrmr != null) {
		//	Debug.Log((pltfrmr.transform.position - transform.position).normalized);
			//pltfrmr.Bounce(BounceDirection*bounceStrength);
			pltfrmr.Bounce((new Vector2(0f,(pltfrmr.transform.position - transform.position).normalized.y)*bounceStrength));
			controlsDisabled = true;
		}

	}


	void OnTriggerStay2D(Collider2D othercol){
		//Debug.Log("Trigger bounce");


		pltfrmr = othercol.GetComponent<PlatformerCharacter2D> ();


		if (pltfrmr != null) {
			//	Debug.Log((pltfrmr.transform.position - transform.position).normalized);
			pltfrmr.Bounce((new Vector2(0f,(pltfrmr.transform.position - transform.position).normalized.y)*bounceStrength));
			//pltfrmr.Bounce(BounceDirection*bounceStrength);
			controlsDisabled = true;
		}

	}




}
