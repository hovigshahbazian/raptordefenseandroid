﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenCargoEvent : MonoBehaviour {
	
	public string OpenCargoEventName;
	public bool AnimIsPlaying;
	private Button _button;

	// Use this for initialization
	void Start () {
		_button = GetComponent<Button>();
		if (_button) _button.onClick.AddListener (delegate() { EventManager.TriggerEvent (OpenCargoEventName); });
	}
	
	// Update is called once per frame
	void Update () {
		if (!AnimIsPlaying) {
			_button.interactable = true;
		} else {
			_button.interactable = false;
		}
			
	}

	public void SetAnimationOn(){
		AnimIsPlaying = true;
	}
	public void SetAnimationOff(){
		AnimIsPlaying = false;
	}

	//void OnDisable() {
		
	//	if (_button)
	//		_button.onClick.RemoveAllListeners ();
	//}
}
