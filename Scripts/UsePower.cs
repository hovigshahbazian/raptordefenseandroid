﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UsePower : MonoBehaviour {

	public CargoMenu cargoMenu;
	//private PlayerGameData playerData;
	public Button buttonUsePower;
	public Text text;


	public RectTransform InfoWeaponWarningUI;
	public RectTransform InfoAirplaneWarningUI;
	public RectTransform InfoReplaceWarningUI;


	[SerializeField]
	private AudioSource setPowerSound;


	// Use this for initialization
	void Start () {

		//playerData = GameDataManager.instance.loadedPlayerData;
		buttonUsePower.interactable= false;
		InfoWeaponWarningUI.gameObject.SetActive (false);

		InfoAirplaneWarningUI.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void SetFullAuto(){


		if (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);

			buttonUsePower.interactable = false;
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
			InfoReplaceWarningUI.gameObject.SetActive(false);
			buttonUsePower.interactable = true;
		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);

		setPowerSound.Play ();


		if (GameDataManager.instance.LoadedTempPlayerData.FullAutoPowerUps > 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.FullAuto));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.FullAuto));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}

	}

	public void SetMultiShot(){

		setPowerSound.Play ();

		if (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);
			buttonUsePower.interactable = false;
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
			buttonUsePower.interactable = true;

		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);




		if (GameDataManager.instance.LoadedTempPlayerData.MultishotPowerUps > 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.Multishot));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.Multishot));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}

	}

	public void SetLaserBeam(){

		setPowerSound.Play ();

		if (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);
			buttonUsePower.interactable = false;
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
			buttonUsePower.interactable = true;
		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);



		if (GameDataManager.instance.LoadedTempPlayerData.LaserbeamPowerUps > 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.Laserbeam));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.Laserbeam));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetExplodingBullet(){

		setPowerSound.Play ();


		if (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);
			buttonUsePower.interactable = false;
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
			buttonUsePower.interactable = true;
		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);




		if (GameDataManager.instance.LoadedTempPlayerData.ExplodingBulletsPowerUps > 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.ExplodingBullets));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.ExplodingBullets));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetInvulnerability(){
		setPowerSound.Play ();


		if (GameDataManager.instance.LoadedTempPlayerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);
			buttonUsePower.interactable = false;
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
			buttonUsePower.interactable = true;
		}

		InfoWeaponWarningUI.gameObject.SetActive (false);



		if (GameDataManager.instance.LoadedTempPlayerData.InvulnerabilityPowerUps> 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.Invulnerability));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.Invulnerability));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetSpeedUp(){

		setPowerSound.Play ();


		if (GameDataManager.instance.LoadedTempPlayerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);
			buttonUsePower.interactable = false;
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
			buttonUsePower.interactable = true;

		}

		InfoWeaponWarningUI.gameObject.SetActive (false);




		if (GameDataManager.instance.LoadedTempPlayerData.SpeedUpPowerUps> 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.SpeedUp));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.SpeedUp));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetInvisibility(){
		setPowerSound.Play ();


		if (GameDataManager.instance.LoadedTempPlayerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);
			buttonUsePower.interactable = false;
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
			buttonUsePower.interactable = true;
		}

		InfoWeaponWarningUI.gameObject.SetActive (false);



		if (GameDataManager.instance.LoadedTempPlayerData.InvisibilityPowerUps> 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.Invisibility));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.Invisibility));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}


	public void SetShield(){
		setPowerSound.Play ();


		if (GameDataManager.instance.LoadedTempPlayerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
			InfoReplaceWarningUI.gameObject.SetActive(true);
			buttonUsePower.interactable = false;
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
			buttonUsePower.interactable = true;
		}

		InfoWeaponWarningUI.gameObject.SetActive (false);


		if (GameDataManager.instance.LoadedTempPlayerData.ShieldPowerUps> 0) {
			//buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.Shield));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.Shield));
			text.text = "Equip Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}




}
