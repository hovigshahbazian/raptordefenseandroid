﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AircraftPowerUpImage : MonoBehaviour {
	//private PlayerGameData playerdata;

	public Sprite[] airPowerupSprites;
	private Image airPowerImage;

	// Use this for initialization


	void Start () {
		//playerdata = GameDataManager.instance.loadedPlayerData;

		airPowerImage = GetComponent<Image> ();

		switch (GameDataManager.instance.LoadedTempPlayerData.StartingAirPower) {

		case PlayerPowerState.AircraftPower.Invulnerability:
			airPowerImage.sprite = airPowerupSprites [0];
			airPowerImage.color = Color.white;
			break;

		case PlayerPowerState.AircraftPower.SpeedUp:
			airPowerImage.sprite = airPowerupSprites [1];
			airPowerImage.color = Color.white;
			break;

		case PlayerPowerState.AircraftPower.Invisibility:
			airPowerImage.sprite = airPowerupSprites [2];
			airPowerImage.color = Color.white;
			break;

		case PlayerPowerState.AircraftPower.Shield:
			airPowerImage.sprite = airPowerupSprites [3];
			airPowerImage.color = Color.white;
			break;

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public  void SetCurAircraftImage(){

		switch (GameDataManager.instance.LoadedTempPlayerData.StartingAirPower) {

		case PlayerPowerState.AircraftPower.Invulnerability:
			airPowerImage.sprite = airPowerupSprites [0];
			airPowerImage.color = Color.white;
			break;

		case PlayerPowerState.AircraftPower.SpeedUp:
			airPowerImage.sprite = airPowerupSprites [1];
			airPowerImage.color = Color.white;
			break;

		case PlayerPowerState.AircraftPower.Invisibility:
			airPowerImage.sprite = airPowerupSprites [2];
			airPowerImage.color = Color.white;
			break;

		case PlayerPowerState.AircraftPower.Shield:
			airPowerImage.sprite = airPowerupSprites [3];
			airPowerImage.color = Color.white;
			break;

		}

	}


}
