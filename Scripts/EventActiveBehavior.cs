﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventActiveBehavior : MonoBehaviour {

	public Behaviour targetBehaviour;

	public string EventString;

	public bool active;

	// Use this for initialization
	void Start () {
		if(EventString != "")
			EventManager.StartListening(EventString, DisableScript);
	}

	public void DisableScript(){
		targetBehaviour.enabled = active;
	}

}
