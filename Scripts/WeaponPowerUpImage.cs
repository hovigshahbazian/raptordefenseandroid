﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponPowerUpImage : MonoBehaviour {


	//private PlayerGameData playerdata;

	public Sprite[] weaponsPowerupSprites;
	private Image weaponPowerImage;
	// Use this for initialization
	void Start () {
		//playerdata = GameDataManager.instance.loadedPlayerData;
		weaponPowerImage = GetComponent<Image> ();

		switch (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower) {

		case PlayerPowerState.WeaponPower.FullAuto:
			weaponPowerImage.sprite = weaponsPowerupSprites [0];
			weaponPowerImage.color = Color.white;
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			weaponPowerImage.sprite = weaponsPowerupSprites [1];
			weaponPowerImage.color = Color.white;
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			weaponPowerImage.sprite = weaponsPowerupSprites [2];
			weaponPowerImage.color = Color.white;
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			weaponPowerImage.sprite = weaponsPowerupSprites [3];
			weaponPowerImage.color = Color.white;
			break;
		default:
			break;
		}



	}
	
	// Update is called once per frame
	void Update () {
	





	}



	public  void SetCurWeaponImage(){

		switch (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower) {

		case PlayerPowerState.WeaponPower.FullAuto:
			weaponPowerImage.sprite = weaponsPowerupSprites [0];
			weaponPowerImage.color = Color.white;
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			weaponPowerImage.sprite = weaponsPowerupSprites [1];
			weaponPowerImage.color = Color.white;
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			weaponPowerImage.sprite = weaponsPowerupSprites [2];
			weaponPowerImage.color = Color.white;
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			weaponPowerImage.sprite = weaponsPowerupSprites [3];
			weaponPowerImage.color = Color.white;
			break;
		default:
			break;
		}




	}


}
