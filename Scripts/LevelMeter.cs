﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelMeter : MonoBehaviour {

	public Sprite[] LevelSprites;
	Image image;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetUpLevelMeter(){
		image.overrideSprite = LevelSprites [1];
	}

	public void SetUpLevelMeter(int i){
		image.overrideSprite = LevelSprites [i];
	}
}
