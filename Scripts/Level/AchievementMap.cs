using UnityEngine;
using System.Collections;

public class AchievementMap : MonoBehaviour {


	static public int enemyunits_Air;
	static public int secondary_kills;
	static public int enemyunits_Terrain;
	static public int enemyunits_Infantry;
	static public int powerups_count;
	static public int diamonds_count;
	static public int completed;
	static public int bonuscompleted;
	static public int secondaryreloads;
	static public int allies_protected;
	static public int achievements;
	static public int stages;



	public int enemyunits_AirInspec;
	public int secondary_killsInspec;
	public int enemyunits_TerrainInspec;
	public int enemyunits_InfantryInspec;
	public int powerups_countInspec;
	public int diamonds_countInspec;
	public int completedInspec;
	public int bonuscompletedInspec;
	public int secondaryreloadsInspec;
	public int allies_protectedInspec;
	public int achievementsInspec;
	public int stagesInspec;



	public float size;
	public bool update;


	public AchievementUI[] Achievements;


	// Use this for initialization
	void Start () {
		
	}



	public void GatherLevelAchievementData(){

		enemyunits_Air=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level3].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level4].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level5].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level6].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level7].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level8].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level9].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level10].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelA].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelB].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Tutorial].AirEnemiesKilled;
		enemyunits_Air+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Endless].AirEnemiesKilled;


		enemyunits_Terrain=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level3].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level4].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level5].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level6].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level7].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level8].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level9].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level10].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelA].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelB].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Tutorial].GroundEnemiesKilled;
		enemyunits_Terrain+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Endless].GroundEnemiesKilled;



		powerups_count=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level3].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level4].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level5].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level6].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level7].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level8].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level9].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level10].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelA].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelB].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Tutorial].PowerUpsCollected;
		powerups_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Endless].PowerUpsCollected;


		diamonds_count=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level3].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level4].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level5].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level6].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level7].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level8].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level9].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level10].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelA].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelB].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Tutorial].GoldEmblemsColleted;
		diamonds_count+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Endless].GoldEmblemsColleted;


		secondaryreloads=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level3].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level4].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level5].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level6].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level7].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level8].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level9].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level10].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelA].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelB].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Tutorial].SecondaryAmmoCollected;
		secondaryreloads+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Endless].SecondaryAmmoCollected;



		allies_protected=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;
		allies_protected+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].AlliesSaved;


		secondary_kills = GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level3].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level4].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level5].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level6].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level7].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level8].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level9].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level10].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelA].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelB].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Tutorial].SecondaryKills;
		secondary_kills+=GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Endless].SecondaryKills;


		completed=0;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level1].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level2].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level3].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level4].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level5].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level6].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level7].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level8].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level9].Completed)
			completed++;
		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.Level10].Completed)
			completed++;






		achievements=0;
		bonuscompleted=0;
		stages=0;

		update=true;



		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelA].Completed) {
			bonuscompleted++;
		}

		if (GameDataManager.instance.LoadedTempLevelData[(int)LevelName.LevelB].Completed) {
			bonuscompleted++;
		}

		foreach (AchievementUI a in Achievements) {

			a.CheckAchievementStatus ();
			a.CheckAwardClaims ();


		}

		/*
		foreach (AchievementUI a in Achievements) {

			if (a.Maxed) {
				achievements = 3;
			} else {
				achievements = a.currentTier;
			}

		}*/



	}




	void Update () {
		if (update) {
	    	Wings.update=true;
			update=false;
		}



		enemyunits_AirInspec = enemyunits_Air;
		secondary_killsInspec = secondary_kills;
		enemyunits_TerrainInspec = enemyunits_Terrain;
		enemyunits_InfantryInspec = enemyunits_Infantry;
		powerups_countInspec = powerups_count ;
		diamonds_countInspec = diamonds_count;
		completedInspec = completed;
		bonuscompletedInspec = bonuscompleted;
		secondaryreloadsInspec = secondaryreloads;
		allies_protectedInspec = allies_protected;
		achievementsInspec = achievements;
		stagesInspec = stages ;





		achievements = 0;

		foreach (AchievementUI a in Achievements) {

			if (a.Maxed) {
				achievements += 3;
			} else {
				achievements += a.currentTier;
			}

		}



	}
}
