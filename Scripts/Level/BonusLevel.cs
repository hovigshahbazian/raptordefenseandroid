﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BonusLevel : MonoBehaviour {

	public Toggle UnlockRequirements;
	public Sprite levelUnfinished;
	public Sprite LevelFinished;
	public Sprite LevelSelected;
	public Sprite oneDiamondComplete;
	public Sprite twoDiamondComplete;
	public Sprite threeDiamondComplete;
	public bool Selected;

	//public LevelData lD1;
	//public LevelData lD2;
	//public LevelData lD3;
	//public LevelData lD4;
	//public LevelData lD5;

	public LevelName LevelRef1;
	public LevelName LevelRef2;
	public LevelName LevelRef3;
	public LevelName LevelRef4;
	public LevelName LevelRef5;

	public int  NumofYDCollected;

	//public LevelGameData bonusStage;
	public LevelName levelName;
	private Button button;
	private Image levelIcon;
	public Image starsIcon;
	public RectTransform DisplayPrompt;

	public StartLevel startButton;
	// Use this for initialization
	void Start () {

		//GameDataManager.instance.GetTempLevelData(LevelRef1, ref lD1);
		//GameDataManager.instance.GetTempLevelData(LevelRef2, ref lD2);
		//GameDataManager.instance.GetTempLevelData(LevelRef3, ref lD3);
		//GameDataManager.instance.GetTempLevelData(LevelRef4, ref lD4);
		//GameDataManager.instance.GetTempLevelData(LevelRef5, ref lD5);

		//Debug.Log (gameObject.name + " Level Unlocked:" + bonusStage.Unlocked);
		//Debug.Log (gameObject.name + " Level Completed:" + bonusStage.Completed);



		NumofYDCollected = GameDataManager.instance.LoadedTempLevelData[(int)LevelRef1].GoldEmblemsColleted 
			             + GameDataManager.instance.LoadedTempLevelData[(int)LevelRef2].GoldEmblemsColleted 
						 + GameDataManager.instance.LoadedTempLevelData[(int)LevelRef3].GoldEmblemsColleted 
						 + GameDataManager.instance.LoadedTempLevelData[(int)LevelRef4].GoldEmblemsColleted 
						 + GameDataManager.instance.LoadedTempLevelData[(int)LevelRef5].GoldEmblemsColleted;
		
		button = GetComponent<Button> ();
		levelIcon = GetComponent<Image> ();

		if (NumofYDCollected >= 15) {
			GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked = true;
		} else {
			GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked = false;
		}


		if (!GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked) {
			levelIcon.sprite = levelUnfinished;
			button.interactable = false;
		} else {
			levelIcon.sprite = levelUnfinished;
			button.interactable = true;
		}

		/*else if (bonusStage.Completed) {

			if (bonusStage.YellowDiamondsColleted == 0)
				levelIcon.sprite = LevelFinished;
			else if (bonusStage.YellowDiamondsColleted == 1)
				levelIcon.sprite = oneDiamondComplete;
			else if (bonusStage.YellowDiamondsColleted == 2)
				levelIcon.sprite = twoDiamondComplete;
			else if (bonusStage.YellowDiamondsColleted == 3)
				levelIcon.sprite = threeDiamondComplete;

		} */


	}
	
	// Update is called once per frame
	void Update () {
		/*
		NumofYDCollected = GameDataManager.instance.LoadedTempLevelData[(int)LevelRef1].GoldEmblemsColleted 
			+ GameDataManager.instance.LoadedTempLevelData[(int)LevelRef2].GoldEmblemsColleted 
			+ GameDataManager.instance.LoadedTempLevelData[(int)LevelRef3].GoldEmblemsColleted 
			+ GameDataManager.instance.LoadedTempLevelData[(int)LevelRef4].GoldEmblemsColleted 
			+ GameDataManager.instance.LoadedTempLevelData[(int)LevelRef5].GoldEmblemsColleted;
		*/



		if (!GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked) {
			levelIcon.sprite = levelUnfinished;
			button.interactable = false;
		} else {
			levelIcon.sprite = levelUnfinished;
			button.interactable = true;
		}

		if (GameDataManager.instance.LoadedTempLevelData[(int)levelName].Completed) {
			levelIcon.sprite = LevelFinished;
		} else {
			
			if(Selected){
				levelIcon.sprite = LevelSelected;
			}else{
			levelIcon.sprite = levelUnfinished;
			}

		}




	}



	public void SelectLevel(){

		//if (!GameDataManager.IsPaid ()) {
			

			if (startButton.CheckIfEnoughFuelCells ()) {

				startButton.GetComponent<Button> ().interactable = true;
				DisplayPrompt.gameObject.SetActive (false);
			} else {
				startButton.GetComponent<Button> ().interactable = false;
				DisplayPrompt.gameObject.SetActive (true);
			}
		//} else {
		//	startButton.GetComponent<Button> ().interactable = true;
		//	DisplayPrompt.gameObject.SetActive (false);
		//}


	}


	public void InvokeClickFunction(){

		if(button.interactable)
		button.onClick.Invoke();
	}


	public void RevealRequirements(){
		if (!button.interactable) {
			UnlockRequirements.onValueChanged.Invoke (UnlockRequirements.isOn);
			UnlockRequirements.isOn = !UnlockRequirements.isOn;
		}
	}

}
