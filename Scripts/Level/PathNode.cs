﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

[RequireComponent (typeof(CircleCollider2D))]
public class PathNode : MonoBehaviour {

	public PathNode nextNode;
	public bool SpawnNewNode;

	private PathNodeSpawner nodespawner;

	// Use this for initialization
	void Start () {
		nodespawner = GetComponent<PathNodeSpawner> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D(Collider2D col){

		FollowTarget followScript = col.GetComponent<FollowTarget> ();

		if (followScript != null && col.CompareTag("Seeker")) {
			
			if (SpawnNewNode && nextNode == null) {
				GameObject obj = nodespawner.SpawnNewNode ();
				nextNode = obj.GetComponent<PathNode> ();
				Debug.Log (nextNode.name);
			} 
				
			followScript.target = nextNode.transform;
		}








	}




}
