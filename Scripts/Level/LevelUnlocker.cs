﻿
using UnityEngine;
using System.Collections;

public class LevelUnlocker : MonoBehaviour {

	//private LevelData nextLevel;

	public LevelName levelName;

	public void UnlockNextLevel(){

		GameDataManager.instance.LoadedTempLevelData[(int)levelName].Unlocked = true;

		GameDataManager.instance.SaveTempLevel((int)levelName);
	

	}

}
