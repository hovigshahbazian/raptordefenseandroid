using UnityEngine;
using System.Collections;


public class MM : MonoBehaviour {
	static int soundon;
	static public int soundplay;
	static public int musicplay;
	static public int musicstop;
	static public int musicnewsong;
//	static int audioend;
	static public int delay;
	static float audio1Volume;
//	static float audio1Volumereset;

	static public AudioClip[] musicAudio = new AudioClip[10];

	static public AudioClip[] soundAudio = new AudioClip[10];

	[SerializeField]
	AudioSource music;
	[SerializeField]
	AudioSource sound;

	static public bool musicvolumechange;
	static public bool soundvolumechange;
	static public float musicvolumechangevalue = 0.3f;
	static public float soundvolumechangevalue = 0.3f;

void Awake () {
		musicplay=-1;
		musicAudio[0] = Resources.Load("Audio/Give Away") as AudioClip;

		soundAudio[0] = Resources.Load("Audio/Sounds/beep") as AudioClip;
		soundAudio[1] = Resources.Load("Audio/Sounds/beep") as AudioClip;
}

void Start () {
	soundplay=-1;
	musicstop=0;
//	audioend=0;
//	audio1Volumereset=0;

	audio1Volume=1.0f;
//	audio1Volumereset=1.0f;

	musicnewsong=0;
	//music = GetComponent<AudioSource> ();

	delay=0;



		PlayerPrefs.SetFloat ("musicvolumechangevalue", 0.5f);
		PlayerPrefs.SetFloat ("soundvolumechangevalue", 0.5f);

	musicnewsong = 0;

}

void Update () {

	
	if (musicplay!=-1) {

		musicvolumechangevalue = PlayerPrefs.GetFloat("musicvolumechangevalue");

		if (musicplay==0) {
			music.clip = musicAudio[musicnewsong];
		}
		else
			music.clip = musicAudio[musicplay];

		audio1Volume=0.3f;
		musicplay=-1;
    	musicstop=2;
		music.volume=audio1Volume;
		music.Play ();
	}

	if (musicstop==1) {
		fadeOut(.5f);
	}
	if (musicstop==2) {
		fadeIn();
	}

	if (musicstop==3) {
		fadeOutNew(2f,musicnewsong);
	}

	if (soundplay!=-1) {
		SoundPlay(soundplay);
			//Debug.Log ("Playingsound");
		soundplay=-1;
	}

	if (musicvolumechange) {
		musicvolumechange=false;
		audio1Volume = musicvolumechangevalue;
		music.volume=audio1Volume;
		//print("audio1Volume "+audio1Volume);
	}

	if (soundvolumechange) {
		soundplay=1;
			//Debug.Log("Playing sound");
		soundvolumechange=false;
	}

} 

void SoundPlay(int clip) {
    audio1Volume = soundvolumechangevalue;	

		if (!sound.isPlaying) {
			sound.clip = soundAudio [clip];
			sound.volume = audio1Volume;
			sound.Play ();
		}
}

void fadeOut(float speed) {
    if(audio1Volume > 0)
    {
        audio1Volume -= speed * Time.deltaTime;
        music.volume = audio1Volume;
    }  
	else {
        music.volume = 0;
		musicstop=0;
		music.Stop();
	}
}

void fadeOutNew(float speed, int song) {
    if(audio1Volume > 0)
    {
        audio1Volume -= speed * Time.deltaTime;
        music.volume = audio1Volume;
    }  
	else {
        music.volume = 0;
		musicstop=0;
		music.Stop();
		musicplay=song;
	}
}




void fadeIn() {
    if(audio1Volume < musicvolumechangevalue)
    {
        audio1Volume += 0.15f * Time.deltaTime;
    }  
	else {
		audio1Volume = musicvolumechangevalue;
		musicstop=0;

	}

	music.volume = audio1Volume;
}


	void OnDisable(){

		PlayerPrefs.SetFloat ("musicvolumechangevalue", musicvolumechangevalue);
		PlayerPrefs.SetFloat ("soundvolumechangevalue", soundvolumechangevalue);
		PlayerPrefs.Save ();

	}




}


