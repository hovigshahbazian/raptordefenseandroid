﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets._2D;

public class BounceWall : MonoBehaviour {

	public float disableTime;
	public float BounceAngle;
	//public Health health;
	//public bool isGround = true;
	private bool controlsDisabled = false;
	private float disableTimer;
	private PlatformerCharacter2D pltfrmr;

	private Collider2D col;

	// Use this for initialization
	void Start () {
		disableTimer = disableTime;
		col = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {


		if (controlsDisabled) {

			disableTimer -= Time.deltaTime;


			if (disableTimer <= 0.0f) {

				disableTimer = disableTime;
				controlsDisabled = false;
				col.enabled = true;
			}
		}
	


	}


	void OnTriggerEnter2D(Collider2D othercol){


			//Rigidbody2D rgd = othercol.GetComponent<Rigidbody2D> ();
			//othercol.enabled = false;

		//if (rgd != null)
		//	rgd.MoveRotation (BounceAngle);

			pltfrmr = othercol.GetComponent<PlatformerCharacter2D> ();


		if (pltfrmr != null) {

			pltfrmr.Bounce (BounceAngle);
		
			controlsDisabled = true;
		}

	}


	void OnTriggerStay2D(Collider2D othercol){


		//Rigidbody2D rgd = othercol.GetComponent<Rigidbody2D> ();
		//othercol.enabled = false;

		//if (rgd != null)
		//	rgd.MoveRotation (BounceAngle);

		pltfrmr = othercol.GetComponent<PlatformerCharacter2D> ();


		if (pltfrmr != null) {
			pltfrmr.Bounce (BounceAngle);
			controlsDisabled = true;
		}

	}




}
