using UnityEngine;
using System.Collections;

public class Bars : MonoBehaviour {

    public GameObject[] bars;
    public GameObject[] covers;
    public GameObject[] barcovers;

	public float scalev;
	public float startx;
	public float startxc;
	public float starty;
	public float startyc;
	public float multy;
	public float value;
	public int display;

	public int screenwoffset;
	public int screenhoffset;
	public int heightoffset;
	public int widthoffset;

	public GUIStyle customButton;

	Vector3 scale;

	public float size;
	private Vector3 coords;
	private Vector3 change;

	public bool update;


	public string string_show;

	int[] score_value = new int[9];
	int[] range = new int[9];

	void Start () {

		screenwoffset=Screen.width;
		screenhoffset=Screen.height;

		heightoffset=387;
		widthoffset=765;
		size=37.5f;

		coords = Camera.main.WorldToScreenPoint(transform.position);

		bars = GameObject.FindGameObjectsWithTag("bars");
		covers = GameObject.FindGameObjectsWithTag("covers");
		barcovers = GameObject.FindGameObjectsWithTag("barcover");

		display=0;

		startx=-.8f;
		startxc=-4.2f;
		starty=.55f;
		startyc=.55f;
		multy=.70f;

		scalev=2f;

		update=false;
			


	}

private void OnGUI () {

		customButton.fontSize = Mathf.RoundToInt (11f * Screen.height / (heightoffset * 1.0f));
		for (int i=0;i!=9;i++) {  

		range[i] = 50;
		if (score_value[i]>50)
			range[i]=200;
		if (score_value[i]>200)
			range[i]=500;

		if (i==2)
			range[i]=12;

		if (i==3)
			range[i]=2;

		if (i==8)
			range[i]=24;

		string_show=""+score_value[i]+"/"+range[i];

		GUI.Label(new Rect(coords.x+40*screenhoffset / (heightoffset * 1.0f), coords.y-((151-(i*size))*screenhoffset / (heightoffset * 1.0f)), 1, 300*screenhoffset / (heightoffset * 1.0f)), string_show, customButton);

	}

}


	void Update () {

    	if (Input.GetKey(KeyCode.Keypad0)) {
	    	AchievementMap.enemyunits_Air++;
			if (AchievementMap.enemyunits_Air>500)
				AchievementMap.enemyunits_Air=500;
			score_value[0]=AchievementMap.enemyunits_Air;
			update=true;
		}
    	if (Input.GetKey(KeyCode.Keypad1)) {
	    	AchievementMap.secondary_kills++;
			if (AchievementMap.secondary_kills>500)
				AchievementMap.secondary_kills=500;
			score_value[1]=AchievementMap.secondary_kills;
			update=true;
		}

    	if (Input.GetKey(KeyCode.Keypad2)) {
	    	AchievementMap.completed++;
			if (AchievementMap.completed>12)
				AchievementMap.completed=12;
			score_value[2]=AchievementMap.completed;
			AchievementMap.stages=AchievementMap.completed/4;
			update=true;
		}


    	if (Input.GetKey(KeyCode.Keypad3)) {
			AchievementMap.bonuscompleted++;
			if (AchievementMap.bonuscompleted>2)
				AchievementMap.bonuscompleted=2;
			score_value[3]=AchievementMap.bonuscompleted;
			update=true;
		}

    	if (Input.GetKey(KeyCode.Keypad4)) {
	    	AchievementMap.secondaryreloads++;
			if (AchievementMap.secondaryreloads>500)
				AchievementMap.secondaryreloads=500;

			score_value[4]=AchievementMap.secondaryreloads;
			update=true;
		}

    	if (Input.GetKey(KeyCode.Keypad5)) {
	    	AchievementMap.powerups_count++;
			if (AchievementMap.powerups_count>500)
				AchievementMap.powerups_count=500;

			score_value[5]=AchievementMap.powerups_count;
			update=true;
		}


    	if (Input.GetKey(KeyCode.Keypad6)) {
	    	AchievementMap.enemyunits_Terrain++;
			if (AchievementMap.enemyunits_Terrain>500)
				AchievementMap.enemyunits_Terrain=500;

			score_value[6]=AchievementMap.enemyunits_Terrain;
			update=true;
		}
    	if (Input.GetKey(KeyCode.Keypad7)) {
	    	AchievementMap.allies_protected++;
			if (AchievementMap.allies_protected>500)
				AchievementMap.allies_protected=500;

			score_value[7]=AchievementMap.allies_protected;
			update=true;
		}


		if (update) {
			Wings.update=true;
			AchievementMap.achievements=0;

			for (int i=0;i!=9;i++) {  

				if (score_value[i]>=500) {
					AchievementMap.achievements++;
					score_value[i]=500;
				}

				if (score_value[i] < 51) {
					scalev=score_value[i]*1.55f;
				}
				if (score_value[i] > 50) {
					scalev=score_value[i]*1.55f;
					scalev=scalev/4;
					AchievementMap.achievements++;
				}
				if (score_value[i] > 200) {
					scalev=score_value[i]*1.55f;
					scalev=scalev/10;
					AchievementMap.achievements++;
				}

				if (i==2) {
					AchievementMap.achievements+=AchievementMap.stages;
					scalev=score_value[i]*1.55f;
					scalev=scalev*4.16f;

				}

				if (i==3) {

					if (AchievementMap.bonuscompleted>=1) {
						AchievementMap.achievements++;

					}
					if (AchievementMap.bonuscompleted==2){
						AchievementMap.achievements+=2;
					}
                    scalev=score_value[i]*1.55f;
					scalev=scalev*24.9f;
				}

				if (i==8) {
					score_value[i]=AchievementMap.achievements;
					scalev=score_value[i]*1.55f;
					scalev=scalev*2.08f;
				}


				value=(scalev)*.025f;

				change =covers[i].transform.localPosition;
				change.x=startxc+value;
				covers[i].transform.localPosition=change;

				scale=new Vector3( scalev,1f, 1f );
				covers[i].transform.localScale=scale;

			}
		}
		if (display==0) {
	    	display=1;
			update=true;
			for (int i=0;i!=9;i++) {  

				barcovers[i].SetActive(true);
				bars[i].SetActive(true);
				covers[i].SetActive(true);

				barcovers[i].transform.Translate(startx,starty-(i*multy),0, Space.World);
				bars[i].transform.Translate(startx,starty-(i*multy),0, Space.World);

				score_value[i]=0;
				if (i==0)
					score_value[i]=AchievementMap.enemyunits_Air;
				if (i==1)
					score_value[i]=AchievementMap.secondary_kills;
				if (i==2)
					score_value[i]=AchievementMap.completed;
				if (i==3)
					score_value[i]=AchievementMap.bonuscompleted;
				if (i==4)
					score_value[i]=AchievementMap.secondaryreloads;
				if (i==5)
					score_value[i]=AchievementMap.powerups_count;
				if (i==6)
					score_value[i]=AchievementMap.enemyunits_Terrain;
				if (i==7)
					score_value[i]=AchievementMap.allies_protected;
				if (i==8)
					score_value[i]=AchievementMap.achievements;

			}

			for (int i=0;i!=9;i++) {  

				if (score_value[i] < 51) {
					scalev=score_value[i]*1.55f;
				}
				if (score_value[i] > 50) {
					scalev=score_value[i]*1.55f;
					scalev=scalev/4;
				}
				if (score_value[i] > 200) {
					scalev=score_value[i]*1.55f;
					scalev=scalev/10;
				}
				if (i==2) {
					scalev=score_value[i]*1.55f;
					scalev=scalev*1.66f;
				}

				if (i==3) {
					scalev=scalev*25;
				}

				if (i==8) {
					scalev=scalev*2.08f;
				}

				value=(scalev)*.025f;
				covers[i].transform.Translate(startxc+value,startyc-(i*multy),0, Space.World);
				scale=new Vector3( scalev,1f, 1f );
				covers[i].transform.localScale=scale;
	    	}
		}
    }
}


