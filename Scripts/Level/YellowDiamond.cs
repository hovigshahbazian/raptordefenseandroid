﻿using UnityEngine;
using System.Collections;

public class YellowDiamond : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){

		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();

		//Debug.Log (col.gameObject.name);

		if (cd != null) {
			cd.PlayCargoAnim (CollectDisplay.DisplayEvent.Diamond);

			gameObject.SetActive (false);
			EventManager.TriggerEvent ("AcquireYellowDiamond");
		}
	}
}
