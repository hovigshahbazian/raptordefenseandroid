using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class Help : MonoBehaviour {

	public float screenwoffset;
	public float screenhoffset;
	public float heightoffset;
	public float widthoffset;
    static public float offset;

	private Vector3 coords;

	public GUIStyle customButton;
	public GUIStyle customButton0;
	public GUIStyle customButton1;
	public GUIStyle customButton2;

	static public bool active;
	static public bool stopped;
	static public int page,newi;
	static public int oldlength;
	static public float scrollpad;
	static public float scrollpadlast;

	public int i,j,xoff1,xoff2,yoff1,yoff2,width;

	private Vector3 temp;
	private Vector3 temporig;


    public Renderer rend;

	float y;
	float oneLineHeight;
	float answerHeight;

    public TextAsset asset;

	string[] info;

	int test;

    static public int[] newisave = new int[20];

	void Start () {

		rend = GetComponent<Renderer>();

		screenwoffset=Screen.width;
		screenhoffset=Screen.height; 

  //  	print (""+Screen.height);
   // 	print (""+Screen.width);

		heightoffset=387;
		widthoffset=765;
		offset=screenhoffset / heightoffset;

		scrollpad=0;
		scrollpadlast=0;

		stopped=false;

		temporig = transform.position; 

		yoff1=20;
		yoff2=20;
		xoff1=-2;
		xoff2=160;
		width=330;

		page=0;
		newi=0;
		oldlength=0;


		info = asset.text.Split("\n"[0]);
	    oldlength=info.Length-1;

		for(i=0;i<20;i++) {
			newisave[i]=0;
		}
	}

private void OnGUI () {

	if (active) {
		coords = Camera.main.WorldToScreenPoint(transform.position);

		customButton.fontSize = Mathf.RoundToInt (18f * offset);
		customButton0.fontSize = Mathf.RoundToInt (14f * offset);
		customButton1.fontSize = Mathf.RoundToInt (13f * offset);
		customButton2.fontSize = Mathf.RoundToInt (11f * offset);

		oneLineHeight = customButton2.CalcHeight(new GUIContent(""), 500);

		y=500+10;

		GUI.Label(new Rect(coords.x-xoff1*offset, screenhoffset-coords.y-(y*offset), 1, 300*offset), "Fighter Town USA", customButton);
		y=y-yoff1;
		//GUI.Label(new Rect(coords.x-xoff1*offset, screenhoffset-coords.y-(y*offset), 1, 300*offset), "Stage 1", customButton);
		y=y-yoff2;
		y=y-yoff2;
		
		stopped=false;


		for(i=newi;i<info.Length-1;i++) {
			if (stopped==false) {
				test = System.Int32.Parse(info[i]);
				i++;
			 //   print(""+i);
		    	if (y-answerHeight > -480) {
					if (test==0) {
						GUI.Label(new Rect(coords.x-xoff1*offset, screenhoffset-coords.y-(y*offset), 1, 300*offset), info[i], customButton0);
						y=y-yoff2;
					}
					if (test==1) {
						GUI.Label(new Rect(coords.x-xoff1*offset, screenhoffset-coords.y-(y*offset), 1, 300*offset), info[i], customButton1);
						y=y-yoff2;
					}

					if (test==2) {
						answerHeight = customButton2.CalcHeight(new GUIContent(info[i]), 420);
						answerHeight= (answerHeight/oneLineHeight)/offset;
						answerHeight=(answerHeight*oneLineHeight)/offset;
						answerHeight+=(30*offset);
						GUI.Label(new Rect(coords.x-xoff2*offset, screenhoffset-coords.y-(y*offset), width*offset, 300*offset), info[i], customButton2);
						y=y-answerHeight;
						newisave[page]=i+1;
					}

					if (test==3) {
						y=y+30;
						GUI.Label(new Rect(coords.x-xoff2*offset, screenhoffset-coords.y-(y*offset), width*offset, 300*offset), info[i], customButton2);
						y=y-yoff2;
					}		   
					if (test==4) {
						y=y+20;
						GUI.Label(new Rect(coords.x-xoff2*offset, screenhoffset-coords.y-(y*offset), width*offset, 300*offset), info[i], customButton2);
						y=y-yoff2;
					}		 
					if (test==5) {
						GUI.Label(new Rect(coords.x-xoff2*offset, screenhoffset-coords.y-(y*offset), width*offset, 300*offset), info[i], customButton2);
						y=y-yoff2;
					}		   
	    		}
			}
		}
 //   	print("y "+y);
	}
}

	void Update () {
		if (active || Credits.active) {
			rend.enabled = true;
		}
		else
			rend.enabled = false;
		
		if (active) {
			if (scrollpad!=scrollpadlast) {
				scrollpadlast=scrollpad;
				temp = temporig;
				temp.y+=(scrollpad*offset);
				transform.position=temp;
			}
		}

	}


}


