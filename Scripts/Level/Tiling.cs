using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]

public class Tiling : MonoBehaviour {

	public float offsetX = 2;			// the offset so that we don't get any weird errors

	public bool hasARightBuddy = false;
	public bool hasALeftBuddy = false;

	public bool reverseScale = true;	// used if the object is not tilable

	//reference to the object pool
	public ObjectPool pool;

	private float spriteWidth = 0f;		// the width of our element
	private Camera cam;
	private Transform myTransform;

	public Transform rightBuddy;
	public Transform leftBuddy;

	void Awake () {
		cam = Camera.main;
		myTransform = transform;
	}

	// Use this for initialization
	void Start () {
		SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
    	spriteWidth = sRenderer.sprite.bounds.size.x;
	
	}
	// Update is called once per frame
	void Update () {
		// does it still need buddies? If not do nothing
		if (hasALeftBuddy == false || hasARightBuddy == false) {
			// calculate the cameras extend (half the width) of what the camera can see in world coordinates
			float camHorizontalExtend = cam.orthographicSize * Screen.width/Screen.height;

			// calculate the x position where the camera can see the edge of the sprite (element)
			float edgeVisiblePositionRight = (myTransform.position.x + spriteWidth/2) - camHorizontalExtend;
			float edgeVisiblePositionLeft = (myTransform.position.x - spriteWidth/2) + camHorizontalExtend;

			// checking if we can see the edge of the element and then calling MakeNewBuddy if we can
			if (cam.transform.position.x >= edgeVisiblePositionRight - offsetX && hasARightBuddy == false)
			{

				MakeNewBuddy (1);
				hasARightBuddy = true;

			}
			else if (cam.transform.position.x <= edgeVisiblePositionLeft + offsetX && hasALeftBuddy == false)
			{

				MakeNewBuddy (-1);
				hasALeftBuddy = true;
			}
		}


		//remove left tiling when camrea goes to the right
		if(cam.transform.position.x >= (myTransform.position.x + spriteWidth) && rightBuddy != null){
			rightBuddy.GetComponent<Tiling> ().hasALeftBuddy = false;
			rightBuddy.GetComponent<Tiling> ().leftBuddy = null;
			//rightBuddy.gameObject.SetActive (true);
			RemoveItself ();

			if (leftBuddy != null) {
				leftBuddy.GetComponent<Tiling> ().RemoveItself ();

			}
		}

		//remove right tiling when camrea goes to the left
		if(cam.transform.position.x <= (myTransform.position.x - spriteWidth) && leftBuddy !=null){
			leftBuddy.GetComponent<Tiling> ().hasARightBuddy = false;
			leftBuddy.GetComponent<Tiling> ().rightBuddy = null;
		//	leftBuddy.gameObject.SetActive (true);
			RemoveItself ();


			//hasALeftBuddy = false;

			if (rightBuddy != null) {
				rightBuddy.GetComponent<Tiling> ().RemoveItself ();
			}

		}

	}
	// a function that creates a buddy on the side required
	void MakeNewBuddy (int rightOrLeft) {
		// calculating the new position for our new buddy
		Vector3 newPosition = new Vector3 (myTransform.position.x + (spriteWidth + offsetX) * rightOrLeft, myTransform.position.y, myTransform.position.z);
		// instantating our new body and storing him in a variable
		//Transform newBuddy = Instantiate (myTransform, newPosition, myTransform.rotation) as Transform;

		GameObject newBuddy = pool.GetPooledObject();

		//if (newBuddy == null) {
		//	return;
		//}

		newBuddy.transform.position = newPosition;
		newBuddy.transform.rotation = myTransform.transform.rotation;
		//newBuddy.transform.localScale = myTransform.localScale;

		newBuddy.transform.localScale = Vector3.one;

		newBuddy.GetComponent<Tiling> ().hasALeftBuddy = false;
		newBuddy.GetComponent<Tiling> ().hasARightBuddy = false;

		//if (newBuddy.GetComponent<Tiling> ().reverseScale) {
		//	newBuddy.transform.localScale = new Vector3 (newBuddy.transform.localScale.x * -1, newBuddy.transform.localScale.y, newBuddy.transform.localScale.z);
		//	reverseScale = false;
		//}

		//Debug.Log ("Making new buddy");
		newBuddy.SetActive (true);


		//pass along the reference to the object pool
		//to the instantiated object
		newBuddy.GetComponent<Tiling> ().pool = pool;


		// if not tilable let's reverse the x size og our object to get rid of ugly seams
		//Note:changed it so that the next buddy is always the opposite scale of the buddy next to it.
		if (reverseScale == true) {
			newBuddy.transform.localScale = new Vector3 ( -1, 1, 1);
		}

   //     newBuddy.parent = myTransform;
		newBuddy.transform.parent = transform.parent;

		if (rightOrLeft == 1) {
			newBuddy.GetComponent<Tiling>().hasALeftBuddy = true;
			rightBuddy = newBuddy.transform;
			//Debug.Log ("Right Set");
		}
		else if ( rightOrLeft == -1){
			newBuddy.GetComponent<Tiling>().hasARightBuddy = true;
			leftBuddy = newBuddy.transform;
			//Debug.Log ("LeftSet");
		}
	}


	public void RemoveItself(){
		gameObject.SetActive (false);
		GetComponent<Tiling> ().hasALeftBuddy = false;
		GetComponent<Tiling> ().hasARightBuddy = false;
		GetComponent<Tiling> ().rightBuddy = null;
		GetComponent<Tiling> ().leftBuddy = null;

	}

}
