﻿using UnityEngine;
using System.Collections;

public class AC130Anim : MonoBehaviour {

	Animator anim;
	public float animDelayTime;
	public float animDelayTimeTwo;
	public SecondaryWeapon paratrooperDeploy;
	float animDelayTimer;
	bool playAnim;
	bool playAnimTwo;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		animDelayTimer = animDelayTime;
	}
	
	// Update is called once per frame
	void Update () {
		if(paratrooperDeploy.ShotFired){
			anim.SetBool("Deploy",true);

			playAnim = true;
		}

		if(playAnim){
			animDelayTimer -= Time.deltaTime;


			if(animDelayTimer <= 0){
				playAnim = false;
				anim.SetBool("Retract",true);
				animDelayTimer = animDelayTimeTwo;
				playAnimTwo = true;
			}

		}


		if(playAnimTwo){
			animDelayTimer -= Time.deltaTime;


			if(animDelayTimer <= 0){
				anim.SetBool("Retract",false);
				anim.SetBool("Deploy",false);
				animDelayTimer = animDelayTime;
				playAnimTwo = false;
			}

		}




	}
}
