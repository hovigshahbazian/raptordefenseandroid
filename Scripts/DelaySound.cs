﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelaySound : MonoBehaviour {

	public AudioSource sound;
	public float SoundDelay;

	// Use this for initialization
	void Start () {
		sound.PlayDelayed(SoundDelay);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
