﻿using UnityEngine;
using System.Collections;

public class PriorityTarget : MonoBehaviour {

	public Collider2D col;

	void Start(){
		col = GetComponent<Collider2D>();
	}

	void OnTriggerEnter2D(Collider2D col){

		if(enabled){
			FaceTowardObject targeter = col.GetComponent<FaceTowardObject> ();

			if (targeter != null) 
				targeter.AcquireTarget (transform);
		}
	}

	public void SetOn(){
		col.enabled = true;
	}

	public void SetOff(){
		col.enabled = false;
	}


	void OnEnable(){
		SetOn();
	}

	void OnDisable(){
		SetOff();
	}
}
