﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAddFuel : MonoBehaviour {

	//private PlayerGameData playerdata;
	public string AddFuelEvent;
	public int addfuelAmt;

	// Use this for initialization
	void Start () {
		EventManager.StartListening (AddFuelEvent, AddFuel );

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void AddFuel(){
		GameDataManager.instance.LoadedTempPlayerData.IncrementFuelCell (addfuelAmt);
		GameDataManager.instance.SaveTempPlayer();
	}

}
