﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartLevel : MonoBehaviour {
	//private PlayerGameData playerdata;
	public Button startButton;
	public GameObject levelManager;
	public GameObject DisplayPrompt;

	// Use this for initialization
	void Start () {
		//playerdata = GameDataManager.instance.loadedPlayerData;
		startButton.onClick.AddListener (() => levelManager.SendMessage ("LoadLevelAsync"));

	}
	
	// Update is called once per frame
	void Update () {
        /*
		if (GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells > 0) {
			DisplayPrompt.SetActive (false);
			startButton.interactable = true;
		} else if(LevelSelectorButton.currentLevel == 0){
			DisplayPrompt.SetActive (false);
			startButton.interactable = true;
		}else{
			DisplayPrompt.SetActive (true);
			startButton.interactable = false;
		}
        */

	}


	public bool CheckIfEnoughFuelCells(){


		if (GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells > 0) {
			return true;
		}

		//if (GameDataManager.IsPaid ()) {
		//	return true;
		//}


		return false;
	}


}
