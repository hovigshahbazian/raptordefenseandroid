﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushEnableDisable : MonoBehaviour {

	public PushPosition pushObject;
	public MonoBehaviour script;
	public GameObject obj;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (pushObject.pushing) {

			if (script != null) {
				script.enabled = true;
			}

			if (obj != null) {
				obj.SetActive (true);
			}

		} else {
			if (script != null) {
				script.enabled = false;
			}

			if (obj != null) {
				obj.SetActive (false);
			}
		}



	}
}
