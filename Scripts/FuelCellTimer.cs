﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FuelCellTimer : MonoBehaviour {
	
	//public PlayerGameData playerData;
	public bool CargoReady;

	public NumberUI secondsUI;
	public NumberUI minutesUI;
	public Text TimerText;
	public float TimePerCell = 900f;

	public float TimeLimit = 900f;
	public bool timesUp;
	public string eventName;
	public string message;
	public Animation fuelcellEmptyAnim;
	float timer;
	int seconds;
	int  minutes;

	public int fuelCellLimit;

	// Use this for initialization
	void Start () {

		timer = TimeLimit;

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		//secondsUI.number = seconds;
		//minutesUI.number = minutes;

		TimerText.text = string.Format("{0:00}:{1:00}",minutes,seconds);

		if (timesUp) {
			

		} else {


		}

	}
	
	// Update is called once per frame
	void Update () {

		if (!timesUp) {
			timer -= Time.deltaTime;

			if (timer <= 0.0f) {
				timesUp = true;

				AcquireFuelCell ();

				//we restart the timer after the fuel cell is acquired

				RestartTimer (TimePerCell);

				if (eventName != "")
					EventManager.TriggerEvent (eventName);

				if (message != "")
					SendMessage (message);

			}
		}


		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		//secondsUI.number = seconds;
		//minutesUI.number = minutes;

		TimerText.text = string.Format("{0:00}:{1:00}",minutes,seconds);
	}

	public void RestartTimer(){
		timer = TimeLimit;
	}

	public void RestartTimer(float time){
		TimeLimit = time;
		timer = TimeLimit;


		timesUp = false;
	}

	public void UseFuelCell(){
		GameDataManager.instance.LoadedTempPlayerData.DecrementFuelCell ();
		GameDataManager.instance.SaveTempPlayer();
		//Debug.Log ("Used A fuel Cell");
	}


	public void  AcquireFuelCell(){

		GameDataManager.instance.LoadedTempPlayerData.IncrementFuelCell ();
		GameDataManager.instance.SaveTempPlayer();
		//Debug.Log ("Acquired A fuel Cell");
	}



	public void ReduceTime(float seconds){
		int cells;
		float remainaderTime;

		//number of fuel cells  = seconds past/ timerpercell 
		cells = (int)(seconds/TimePerCell);
		remainaderTime = seconds % TimePerCell;

		GameDataManager.instance.LoadedTempPlayerData.IncrementFuelCell (cells);
		//GameDataManager.instance.SaveTempPlayer();
		if (cells > 0) {
			
			TimeLimit = TimePerCell - remainaderTime;
		} else {

			TimeLimit -= remainaderTime;
		}


	}

	public float GetTimeLeft(){
		return timer;
	}


	public void PlayEmptyFuelCell(){
		fuelcellEmptyAnim.Play ();
	}

}
