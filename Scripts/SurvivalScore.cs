﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SurvivalScore : MonoBehaviour {


	public Text survivalScore;

	// Use this for initialization
	void Start () {

		EventManager.StartListening ("PlayerDeath", RestartGame);

		EventManager.StartListening ("KilledAirEnemy", AddScore);
		EventManager.StartListening ("KilledGroundEnemy", AddScore);
		EventManager.StartListening ("BossDestroyed", AddScore);



	}


	void Update() {

		survivalScore.text = SurvivalManager.SurvivalScore.ToString();
	}

	public void RestartGame(){
		GameDataManager.instance.HighScore = SurvivalManager.SurvivalScore;
		Debug.Log("Setting data");
		SurvivalManager.Instance.RestartGame ();
	}

	public void AddScore(){

		SurvivalManager.Instance.AddScore (1);

	}






}
