﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatWarning : MonoBehaviour {


	public Animator anim;
	public bool alarmSounded;
	public AudioSource warning;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if(anim.GetBool("ThirdQuarter") && !alarmSounded){
			warning.Play();
			alarmSounded = true;
		}

		if(!anim.GetBool("ThirdQuarter")){
			alarmSounded = false;
			warning.Stop();
		}



	}
}
