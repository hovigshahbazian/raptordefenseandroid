﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenCargoButton : MonoBehaviour {

	public CargoTypes cargoType;
	public Button openBoxBtn;
	private Button openMenuBtn;

	private CargoTimer cargoTime;
	//private PlayerGameData playerData;
	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		openMenuBtn = GetComponent<Button> ();
		cargoTime = GameDataManager.instance.cargoTime;

		switch (cargoType) {
		case CargoTypes.Common:
			if (GameDataManager.instance.LoadedTempPlayerData.CommonCargoBoxes > 0) {
				openMenuBtn.interactable = true;
		
			} else {
				openMenuBtn.interactable = false;
			
			}
		
			openBoxBtn.onClick.AddListener (() => OpenCargoBox (CargoTypes.Common));
			break;

		case CargoTypes.Rare:
			if (GameDataManager.instance.LoadedTempPlayerData.RareCargoBoxes > 0) {
				openMenuBtn.interactable = true;
			} else {
				openMenuBtn.interactable = false;
			}

			openBoxBtn.onClick.AddListener (() => OpenCargoBox (CargoTypes.Rare));
			break;

		case CargoTypes.Epic:
			if (GameDataManager.instance.LoadedTempPlayerData.EpicCargoBoxes > 0) {
				openMenuBtn.interactable = true;
			} else {
				openMenuBtn.interactable = false;
			}

			openBoxBtn.onClick.AddListener (() => OpenCargoBox (CargoTypes.Epic));
			break;
		}

	





	}
	
	// Update is called once per frame
	void Update () {


		switch (cargoType) {
		case CargoTypes.Common:
			if (GameDataManager.instance.LoadedTempPlayerData.CommonCargoBoxes > 0) {
				openMenuBtn.interactable = true;

			} else {
				openMenuBtn.interactable = false;

			}


			break;

		case CargoTypes.Rare:
			if (GameDataManager.instance.LoadedTempPlayerData.RareCargoBoxes > 0) {
				openMenuBtn.interactable = true;
			} else {
				openMenuBtn.interactable = false;
			}


			break;

		case CargoTypes.Epic:
			if (GameDataManager.instance.LoadedTempPlayerData.EpicCargoBoxes > 0) {
				openMenuBtn.interactable = true;
			} else {
				openMenuBtn.interactable = false;
			}



			break;
		}






	}





	public void OpenCargoBox(CargoTypes cargoType){
		
		cargoTime.UseCargoBox (cargoType);
	}


}
