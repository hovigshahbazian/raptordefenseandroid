﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawningArea : MonoBehaviour {


	public void OnTriggerEnter2D(Collider2D other){
		//Debug.Log ("Despawning...");
		other.gameObject.SetActive (false);
		//other.gameObject.

	}

	public void OnTriggerStay2D(Collider2D other){
		//Debug.Log ("Despawning...");
		other.gameObject.SetActive (false);
	}

}
