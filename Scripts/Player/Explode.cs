using UnityEngine;
using System.Collections;

public class Explode : MonoBehaviour {

public static ParticleSystem ps1;
	void Start () {
		ps1 = GetComponent<ParticleSystem>();
		ps1.GetComponent<Renderer>().sortingLayerName = "front";
	}
	
	// Update is called once per frame
	void Update () {						 //for flat bottom
		if (GM.explodetype==1) {
			if (GM.explodeflag==true){
				transform.position = GM.planepos;
				Vector3 temp = GM.planepos; // copy to an auxiliary variable...
		    	temp.y-=1;
				transform.position=temp;
				ps1.Play();
				GM.explodeflag=false;
				GM.explodetype=0;
				GM.smokeflag=true;
			}
		}
	}
}
