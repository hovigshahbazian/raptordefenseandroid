using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : MonoBehaviour
    {
		//public PlayerGameData playerGameData;
		public bool useSkin = true;
        public float m_MaxSpeed = 20f;                  
		public bool ReverseScale;
        private Rigidbody2D m_Rigidbody2D;
		public bool UseAltMove = false;
	//	public int acRotation = 15;
		public float rotationSpeed = 100f;
		public float speed = 0;
		public bool dead;		
		public bool exit;		
		public bool bouncing;	

		static public bool falling=false;
		static public bool cameraon = true;
		static public float speeddec = 0;
		static public float mh = 0;
		static public float mv = 0;
		static public Animator m_Anim;    
		static public int acRotation = 3;
		static public float acRotationvalue;

		public bool doroll = false;
		public bool dorolldemo;
		public bool dorolldemostarted;
		public int rolltimer;
		public int deadcount;
		public int flashcount;
		public int delayroll;

		public bool toohigh=false;
		public int toohightimer;

		public bool stopped=false;
		public bool stealth=false;
		public int stealthtimer;
		public bool fallleft;
		public bool spinning;
		public int presstime;
		public bool ReverseDirection = false;
        public Color mycolor;
		public SpriteRenderer spriteRender;

		[SerializeField]
		private float disableTime;
		private float disableTimer;


		private bool isMovingVert;
		private bool isRotatingAngle;
		private bool isBoth;

		public bool IsMoving{
			get {return isMovingVert;}
			set { isMovingVert = value; }
		}
		public bool IsRotating{
			get {return isRotatingAngle;}
			set {isRotatingAngle = value;}
		}

		public bool IsBoth{
			get {return isBoth;}
			set {isBoth = value;}
		}


        void Awake()
        {
            // Setting up references.
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
			spriteRender = GetComponent<SpriteRenderer> ();
			mh=1;
			mv=0;
			speeddec=0;

			//if (useSkin) {
			//	spriteRender.sprite = GameDataManager.instance.Skins [GM.skinsave];
			//	Debug.Log("Setting Skin");
		//	}
			
        }

        private void Exit() {
			//Debug.Log ("Exiting");
			GM.Exiting=true;
			GM.completed=false;
			GM.scene="-1";
	    	stopped=true;
			Health health = m_Rigidbody2D.GetComponent<Health> ();
			health.invulnerble=true;
			CloudsEnd.endlevel=true;
			Fade.fadeingOut = true;
			//m_Anim.SetBool("spin", false);
			PlaneEnd.rolltimer=10;

		}

         void Start()
		{
			dead=false;
			stopped=false;
			spinning=false;
			acRotation=3;
			cameraon=true;
			falling=false;
			toohigh = false;
			ReverseScale = ControlTypeOption.ReverseStyle;


	    	rolltimer=50;
	    	dorolldemo=false;
	    	Exhaust.exhaust_start=true;

			//mycolor=transform.GetComponent<Renderer>().material.color;

			if (PlaneEnd.show==true)
				PlaneEnd.kill=true;


			//GM.intunnel=false;

			dorolldemostarted=false;

			EventManager.StartListening("PlayerDeath",Stop);

			if(useSkin)
				spriteRender.sprite = GameDataManager.instance.Skins [GM.skinsave];

		}
	

		void LateUpdate () {



			if (useSkin) {
				if (spriteRender.sprite == null) {
					spriteRender.sprite = GameDataManager.instance.Skins [GM.skinsave];
				}
			}

	


		}

		private void OnTriggerEnter2D(Collider2D other) {

			if (other.tag == "sky" && !toohigh) {		
		
				toohigh=true;
				toohightimer=150;
				Exhaust.exhaust_end=true;
			}

		}
		private void OnTriggerExit2D(Collider2D other) {
			if (other.tag == "purple") {		
				
	    		cameraon=true;
			}

			if (other.tag == "sky") {		

				toohigh=false;
				toohightimer=150;
				Exhaust.exhaust_end=false;
				Exhaust.exhaust_start = true;
				transform.GetComponent<Renderer>().material.color = Color.white;
			}

				
		}

        private void Flash() {							 
			flashcount++;
			if (flashcount==20) {
				flashcount=0;
			}
			if (flashcount>10)
				transform.GetComponent<Renderer>().material.color = Color.red;
			else
				transform.GetComponent<Renderer>().material.color = mycolor;
		}

		public void DoAltRotate() {

			if (spinning == false && !bouncing) {

				#if UNITY_STANDALONE_WIN


				//on key up check if position is reversed

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

				GM.button_pressed_type = 0;

				transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
				GM.button_pressed_type = 0;

				transform.Rotate (0, 0, (-1 * acRotation));
				}
				#endif 	
				/*
				#if UNITY_EDITOR

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, (-1 * acRotation));
				}
					
				#endif
				*/

				#if UNITY_ANDROID
				if (GM.button_pressed_type == 1){
					GM.button_pressed_type = 0;
					transform.Rotate (0, 0, 15);
				}else if (GM.button_pressed_type == 2){
					GM.button_pressed_type = 0;
					transform.Rotate (0, 0, -15);
				}
				#endif

				Vector3 eulerAngles = transform.rotation.eulerAngles;

				acRotationvalue	= eulerAngles.z;

				if (acRotationvalue >=0 && acRotationvalue <91) {
					mh=1-(.0111f*acRotationvalue);
					mv=.0111f*acRotationvalue;
					//transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=false;
				}

				if (acRotationvalue >90 && acRotationvalue <181) {
					mh=-(acRotationvalue-90)*.0111f;
					mv=1-(.0111f*(acRotationvalue-90));

					//if (ReverseScale) 
						//transform.localScale = new Vector3(1f, -1f, 1f);
					//else
						//transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=true;

				}

				if (acRotationvalue >180 && acRotationvalue <271) {
					mh=-(1-(.0111f*(acRotationvalue-180)));
					mv=-(.0111f*(acRotationvalue-180));


					//if (ReverseScale) 
						//transform.localScale = new Vector3(1f, -1f, 1f);
					//else
						//transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=true;
				}


				if (acRotationvalue >270 && acRotationvalue <360) {
					mh=(acRotationvalue-270)*.0111f;
					mv=-(1-(.0111f*(acRotationvalue-270)));
					//transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=false;
				}
			}


		}

		//ignore
        public void DoRotate() {
			
			if (spinning==false && !bouncing) {

				#if UNITY_STANDALONE_WIN


				//on key up check if position is reversed

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, (-1 * acRotation));
				}
				#endif 	
				/*
				#if UNITY_EDITOR

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, (-1 * acRotation));
				}
					
				#endif
				*/

				#if UNITY_ANDROID

				float increment = 0;

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) >= 0)
					increment = CrossPlatformInputManager.GetAxis ("Vertical");

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) < 0)
					increment = CrossPlatformInputManager.GetAxis ("Vertical");


				if(ReverseDirection){
					acRotation = (int)transform.rotation.z;
					m_Rigidbody2D.angularVelocity = acRotation + increment * rotationSpeed;

				}
				else {
					acRotation = (int)transform.rotation.z;
					//transform.Rotate (0, 0, acRotation + increment * rotationSpeed);
					m_Rigidbody2D.angularVelocity = acRotation + increment * rotationSpeed;
				}
				
				#endif

				#if UNITY_IPHONE

				float increment = 0;

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) >= 0)
				increment = CrossPlatformInputManager.GetAxis ("Vertical");

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) < 0)
				increment = CrossPlatformInputManager.GetAxis ("Vertical");


				if(ReverseDirection){
					acRotation = (int)transform.rotation.z;
					//transform.Rotate (0, 0, acRotation +  increment * -2.5f);
					//m_Rigidbody2D.MoveRotation(acRotation +  increment * 2.5f);

				}
				else {
					acRotation = (int)transform.rotation.z;
					//transform.Rotate (0, 0, acRotation + increment * 2.5f);
					//m_Rigidbody2D.MoveRotation(acRotation +  increment * 10.5f);
					m_Rigidbody2D.rotation += (acRotation +  increment * 2.5f);
				}

				#endif





			}
				
			Vector3 eulerAngles = transform.rotation.eulerAngles;

			acRotationvalue	= eulerAngles.z;

			if (acRotationvalue >=0 && acRotationvalue <91) {
				mh=1-(.0111f*acRotationvalue);
				mv=.0111f*acRotationvalue;
				//transform.localScale = new Vector3(1f, 1f, 1f);
			
				fallleft=false;
			}

			if (acRotationvalue >90 && acRotationvalue <181) {
				mh=-(acRotationvalue-90)*.0111f;
				mv=1-(.0111f*(acRotationvalue-90));

				//if (ReverseScale) 
				//	transform.localScale = new Vector3(1f, -1f, 1f);
				//else
				//	transform.localScale = new Vector3(1f, 1f, 1f);
				
				fallleft=true;

			}

			if (acRotationvalue >180 && acRotationvalue <271) {
				mh=-(1-(.0111f*(acRotationvalue-180)));
				mv=-(.0111f*(acRotationvalue-180));


				//if (ReverseScale) 
				//	transform.localScale = new Vector3(1f, -1f, 1f);
				//else
				//	transform.localScale = new Vector3(1f, 1f, 1f);
			
				fallleft=true;
			}


			if (acRotationvalue >270 && acRotationvalue <360) {
				mh=(acRotationvalue-270)*.0111f;
				mv=-(1-(.0111f*(acRotationvalue-270)));
			//	transform.localScale = new Vector3(1f, 1f, 1f);
		
				fallleft=false;
			}
		}

		public void DoMovement(){


		
			m_Rigidbody2D.velocity	+= (Vector2.up * CrossPlatformInputManager.GetAxis ("Vertical") * 20f);
		}




		public void CheckReversePosition(){
			if (transform.localScale.y == -1) {
				ReverseDirection = true;
			} else {
				ReverseDirection = false;
			}

		}




        private void FixedUpdate()
        {
			Move();

	    	if (CloudsEnd.show==true)
	    		this.gameObject.SetActive (false);


			if (toohigh) {
				toohightimer--;
				Flash();
				if (toohightimer==0) {
					//print("fall");
                    m_Rigidbody2D.gravityScale=200;
                    m_Rigidbody2D.isKinematic=false;
					falling=true;
					toohigh=false;
					EventManager.TriggerEvent("Falling");
					Health health = m_Rigidbody2D.GetComponent<Health> ();
					health.HealthPoints = 1;
					//Debug.Log("TriggeringFalling");
				}

			}



			if (bouncing) {

				disableTimer -= Time.deltaTime;


				if (disableTimer <= 0.0f) {

					disableTimer = disableTime;
				
					bouncing = false;
					GetComponent<Collider2D>().enabled = true;
				}

			}





			/*
			if (IsBoth) {
				DoRotate ();
				DoMovement ();
			}
			else if (IsRotating) {
				DoRotate ();
			} 
			else if (IsMoving) {
				DoMovement ();
			}*/

			if (UseAltMove) {
				DoAltRotate ();
			} else {

				DoRotate ();
			}


			if (falling) {
				if (fallleft==true) {
					transform.Rotate(0, 0, (10));
					if (acRotationvalue>260)
						falling=false;
				}
				else {
					transform.Rotate(0, 0, (-10));
					if (acRotationvalue < 280 && acRotationvalue >180)
						falling=false;
				}
				if (falling==false) {
					spinning=true;
					//m_Anim.SetBool("spin", true);
					PlatformerCharacter2D.m_Anim.speed=2;

				}

			}


			if (presstime!=0)
				presstime--;



			if (dead==false) {

				Health health = m_Rigidbody2D.GetComponent<Health> ();


		 //   	if (health.HealthPoints<5) {
		 //   		Flash();
		 //   	}

				
				if (health.isAlive==false) {

					GM.explodeflag=true;
					dead=true;
				//	print("dead "+dead);
					deadcount=40;
					m_Rigidbody2D.gravityScale=0;
					falling=false;
					//m_Anim.SetBool("spin", false);
				}
			}
			else {
				if (speeddec<m_MaxSpeed) {
					speeddec+=1f;
					if (deadcount!=0) {
						deadcount--;
						if (deadcount==0) {
							this.gameObject.SetActive (false);
						}
					}
				}
			}

		}


		public void Move()
        {
			PlayerPowerState powerState = GetComponent<PlayerPowerState> ();

			speed=m_MaxSpeed-speeddec;



			if (GM.completed) {
				Exit();
				//Debug.Log ("Exiting");
			}
	  //  	if (stopped)
	  //  		speed=0;
			if (falling)
				speed=0;


			m_Rigidbody2D.velocity = new Vector2(mh*speed, mv*speed);

        }


		public void Bounce(float angle){


			if (!spinning) {
				m_Rigidbody2D.rotation = angle;
				bouncing = true;
			}
		}


		public void Bounce(Vector2 vector){
			if (!spinning) {
				m_Rigidbody2D.AddForce (vector);
				bouncing = true;
			}

		}

		public void Stop(){
		//	dead = true;
			GetComponent<Collider2D>().enabled = false;
			m_MaxSpeed = 0;
			if(GetComponent<SpeedBoost>() != null){
				GetComponent<SpeedBoost>().enabled = false;
			}
		}

    }
}
