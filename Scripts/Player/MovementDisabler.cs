﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class MovementDisabler : MonoBehaviour {

	public string targetTag;
	public string EventName;

	void OnTriggerStay2D(Collider2D col){

		FollowTarget moveScript = col.GetComponent<FollowTarget> ();

		if (moveScript != null) {
			moveScript.enabled = false;
		}



		if(col.tag == targetTag){
			EventManager.TriggerEvent(EventName);
		}

		//Debug.Log (col.gameObject.name);

	}


}
