using UnityEngine;
using System.Collections;

public class Flare : MonoBehaviour {


	public static bool flare;

	public static ParticleSystem ps;


	void Start () {
    	ps = GetComponent<ParticleSystem>();
		ps.GetComponent<Renderer>().sortingLayerName = "front";
	}

	void Update () {
    	if (flare) {
    		ps.Play();
    		flare=false;
    	}
    }
}

