﻿using UnityEngine;
using System.Collections;

public class InvincibilityFrameAnim : MonoBehaviour {

	public Health health;

	public Color inviciColor;

	public SpriteRenderer render;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (health.invulnerble) {
			render.color = new Color(0.9f,0.2f,0.2f,1f);
			//render.color = inviciColor;
			//Debug.Log ("isInvulnerable");
		} else {
			render.color = new Color(1f,1f,1f,1f);
			//render.color = inviciColor;
		}
	}
}
