﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDataUI : MonoBehaviour {

	public enum PlayerDataField { SurivalHighScore, FuelCells, CommonCargoBoxes, RareCargoBoxes,EpicCargoBoxes,SurivalRecord, FullAuto,Multishot,Laserbeam, ExplodingBullets,Invulnerability,SpeedUP, Shield, Invisibility}

	public PlayerDataField data;
	public bool update = true;
	public Text playerDataTxt;


	// Use this for initialization
	void Start () {
		SetUpUI ();
	}
	
	// Update is called once per frame
	void Update () {
		if(update){
			SetUpUI ();
		}
	}


	public void SetUpUI(){

		switch (data) {

		case PlayerDataField.SurivalHighScore:
			#if UNITY_ANDROID
			int  googlescore = GooglePlayGameScript.GetPlayerScore ();

				if(googlescore == 0){
					playerDataTxt.text = GameDataManager.instance.LoadedTempPlayerData.HighScore.ToString();

				}else{
					playerDataTxt.text = GooglePlayGameScript.GetPlayerScore ().ToString ();
				}


			


			Debug.Log("Getting playerScore");
			#endif

			break;
		case PlayerDataField.CommonCargoBoxes:
			playerDataTxt.text = GameDataManager.instance.LoadedTempPlayerData.CommonCargoBoxes.ToString();
			break;
		case PlayerDataField.RareCargoBoxes:
			playerDataTxt.text = GameDataManager.instance.LoadedTempPlayerData.RareCargoBoxes.ToString();
			break;
		case PlayerDataField.EpicCargoBoxes:
			playerDataTxt.text = GameDataManager.instance.LoadedTempPlayerData.EpicCargoBoxes.ToString();
			break;
		case PlayerDataField.FuelCells:
			playerDataTxt.text = GameDataManager.instance.LoadedTempPlayerData.NumOfFuelCells.ToString();
			break;
		case PlayerDataField.SurivalRecord:
			float time = GameDataManager.instance.LoadedTempPlayerData.EndlessRecordtime;
			float secs = (int)time % 60;
			float minutes = (int)time / 60;

			float millisecs = (int)(time * 100) % 100;

			playerDataTxt.text = string.Format("{0:00}:{1:00}:{2:00}",minutes,secs,millisecs);
			break;
		case PlayerDataField.FullAuto:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.FullAutoPowerUps);
			break;
		case PlayerDataField.Multishot:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.MultishotPowerUps);
			break;
		case PlayerDataField.Laserbeam:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.LaserbeamPowerUps);
			break;
		case PlayerDataField.ExplodingBullets:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.ExplodingBulletsPowerUps);
			break;
		case PlayerDataField.Invulnerability:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.InvulnerabilityPowerUps);
			break;
		case PlayerDataField.Invisibility:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.InvisibilityPowerUps);
			break;
		case PlayerDataField.SpeedUP:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.SpeedUpPowerUps);
			break;
		case PlayerDataField.Shield:
			playerDataTxt.text = string.Format("{0:00}",GameDataManager.instance.LoadedTempPlayerData.ShieldPowerUps);

			break;




		}



	}


}
