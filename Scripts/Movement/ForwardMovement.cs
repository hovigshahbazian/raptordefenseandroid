﻿using UnityEngine;
using System.Collections;

public class ForwardMovement : MonoBehaviour {

	public Vector3 direction;
	public int speed;


	public bool stops;
	public float moveTime;

	private bool isMoving;
	private float moveTimer;

	private Transform trans;
	private Rigidbody2D rgd;

	void OnEnable(){
		isMoving = true;

		moveTimer = moveTime;
		trans = transform;
		rgd = GetComponent<Rigidbody2D> ();
	}


	// Use this for initialization
	void Start () {


		rgd = GetComponent<Rigidbody2D> ();
		isMoving = true;

		moveTimer = moveTime;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (stops) {
			if (isMoving) {



				if (rgd != null) {
					rgd.position -= (Vector2)(direction * Time.deltaTime * speed);

				} else {
					trans.Translate (direction * Time.deltaTime * speed);
				}


	

				moveTimer -= Time.deltaTime;

				if (moveTimer <= 0.0f) {
					isMoving = false;
				}
			}
		} else {

			if (rgd != null) {
				rgd.position += (Vector2)((transform.right + direction)* Time.deltaTime * speed);


			} else {
				trans.Translate (direction * Time.deltaTime * speed);
			}


		}

	}
}
