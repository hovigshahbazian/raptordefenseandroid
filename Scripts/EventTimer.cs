﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTimer : MonoBehaviour {
	
	public TimedEvent time;
	public string EventName;
	// Use this for initialization

	void Start () {
		EventManager.StartListening (EventName, StartTimer);
	}
	
	// Update is called once per frame
	void Update () {
		

	}


	public void StartTimer(){
		time.ResetTimer ();
		time.StartTimer ();
		time.enabled = true;
	}


}
