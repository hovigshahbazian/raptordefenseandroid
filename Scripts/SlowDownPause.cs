﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class SlowDownPause : MonoBehaviour {

	public enum PauseState{ Normal, Speedup, Freezing,Paused };
	public PauseState state;
	public RectTransform pauseWindow;
	public float timeReduction;

	public float timeUntilFreeze;
	public float timeUntilPause;

	public float AlphaAddition;
	public Image image;
	float currentTimeScale;
	float AlphaScale;

	float freezeTimer;
	float PauseTimer;

	bool freezing;
	bool pausing;

	void Start () {
		currentTimeScale = Time.timeScale;
		image = GetComponent<Image>();
		freezeTimer = timeUntilFreeze;
		PauseTimer = timeUntilPause;

	}
	
	// Update is called once per frame
	void Update () {

		if (state == PauseState.Freezing) {
			currentTimeScale -= timeReduction * Time.deltaTime;
			AlphaScale += AlphaAddition * Time.deltaTime;
			AlphaScale = Mathf.Clamp (AlphaScale, 0, 0.5f);
			currentTimeScale = Mathf.Clamp (currentTimeScale, 0.1f, 100f);
			pausing = true;
		} else if (state == PauseState.Speedup) {


		} else if (state == PauseState.Normal) {

			freezing = true;
		} else if (state == PauseState.Paused) {
			
		}


		image.color = new Color(0f,0f,0f,AlphaScale);
		Time.timeScale = currentTimeScale;


		if(GM.button_pressed != 0 || CrossPlatformInputManager.GetAxis("Vertical") != 0f){
			freezing = false;
			pausing = false;
			state = PauseState.Normal;
			reset ();
		}


		if(freezing){
			freezeTimer -= Time.deltaTime;
			if(freezeTimer <= 0f){
				freezing = false;
				state = PauseState.Freezing;
			}
		}

		if (pausing) {
			PauseTimer -= Time.deltaTime;
			if (PauseTimer <= 0f) {
				pausing = false;
				pauseWindow.gameObject.SetActive (true);
				currentTimeScale = 0.0f;
				PauseTimer = timeUntilPause;
				state = PauseState.Paused;
			}
		}
	}

	public void reset(){

		currentTimeScale = 1.00f;
		AlphaScale = 0f;
		freezeTimer = timeUntilFreeze;
		PauseTimer = timeUntilPause;
		pauseWindow.gameObject.SetActive (false);
	}



}
