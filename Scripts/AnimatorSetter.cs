﻿using UnityEngine;
using System.Collections;

public class AnimatorSetter : MonoBehaviour {

	public Animator animator;

	public string triggerName = "OpenChestUpgrade";

	public string AnimName;


	public void SetTriggerName(string trigger){
		triggerName = trigger;
	}


	public void SetTrigger(){
		animator.SetTrigger(triggerName);
	}


	public void PlayAnim(){
		animator.Play (AnimName);
	}



}
