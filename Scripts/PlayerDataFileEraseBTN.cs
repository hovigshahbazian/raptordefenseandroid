﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDataFileEraseBTN : MonoBehaviour {
	
	private Button playerDataEraseBtn;

	// Use this for initialization
	void Start () {
		playerDataEraseBtn = GetComponent<Button> ();

		playerDataEraseBtn.onClick.AddListener (() => ErasePlayerDataFileClick());
	}




	public void ErasePlayerDataFileClick(){
		GameDataManager.instance.ErasePlayerGameFile();
	}


}
