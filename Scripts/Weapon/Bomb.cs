﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {


	public Explosion explosion;
	public float lifetime;
	public float detonateTime;
	public bool isDetonating;
	public bool isTraveling;
	public float detonateTimer;
	private SpriteRenderer sprite;
	private Rigidbody2D rgd;
	//public Rigidbody2D ShooterBody;
	//public LayerMask whatTohit;
	//public LayerMask whatTohit2;
	private Collider2D collider2d;
	private AudioSource fallingsound;
	public float Speed;
	void OnEnable(){
		sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();


		collider2d = GetComponent<Collider2D> ();
		fallingsound = GetComponent<AudioSource>();

		collider2d.enabled = true;

		sprite.enabled = true;
		if (isTraveling) {
			//Debug.Log ("Setting Velocity onEnable");
			ApplyVelocity (transform.right);
		}


		detonateTimer = detonateTime;

		if (detonateTime > 0.0f) {
			isDetonating = true;
		} 

		Invoke ("Destroy", lifetime);
		Refresh ();


	}


	// Use this for initialization
	void Start () {
		detonateTimer = detonateTime;
		sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();
		collider2d = GetComponent<Collider2D> ();


		collider2d.enabled = true;
		//if (isTraveling) {
		//	ApplyVelocity (transform.right);
		//	Debug.Log ("Setting Velocity OnStart");
		//}
		
		if (detonateTime > 0.0f) {
			isDetonating = true;
		} 

		//Invoke ("Destroy", lifetime);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (isDetonating) {
			detonateTimer -= Time.deltaTime;
			//Debug.Log (detonateTimer.ToString());
			if (detonateTimer <= 0.0f) {
				isDetonating = false;
				detonateTimer = detonateTime;
				Detonate ();

				if(explosion !=null)
				explosion.BeginExplosion ();

				if(fallingsound)
					fallingsound.Stop();
			
			}

		}

	}

	void Dissappear(){
		if(sprite != null)
			sprite.enabled = false;
	}

	void Stop(){

		if (rgd != null) {
			rgd.isKinematic = true;
			rgd.velocity = Vector3.zero;
		}
	}

	void Detonate(){
		Dissappear ();
		Stop ();

	}

	void Refresh(){
		sprite.enabled = true;
		rgd.isKinematic = false;
	}

	void OnTriggerEnter2D(Collider2D other){

		//Debug.Log ("start Esplosion trigger");

		  Stop();


		rgd.velocity = Vector2.zero;

		if (explosion.expState == Explosion.ExplosionState.Inactive) {
			if (detonateTime <= 0.0f) {
				Dissappear ();
				explosion.BeginExplosion ();
				if(fallingsound)
					fallingsound.Stop();
			} else {
				isDetonating = true;

			}
		}



		collider2d.enabled = false;
	//	Debug.Log (other.gameObject.name);
	}

	void OnCollisionEnter2D(Collision2D other){
		//Debug.Log ("start Esplosion col");
		Stop();

		//isDetonating = true;
		rgd.velocity = Vector2.zero;
		if (detonateTime <= 0.0f) {
			Dissappear ();
			explosion.BeginExplosion ();

		} else {
			isDetonating = true;
		}
		//}
		collider2d.enabled = false;
		//Debug.Log (other.gameObject.name);
	}





	void Destroy(){

		if(explosion != null)
			explosion.EndExplosion ();

		gameObject.SetActive (false);
		explosion.expState = Explosion.ExplosionState.Inactive;
	}

	void OnDisable(){
		CancelInvoke ();
		sprite.enabled = true;
		explosion.expState = Explosion.ExplosionState.Inactive;
	}


	public void ApplyVelocity(Vector3 direction)
	{
		rgd.velocity = (direction * Speed);

		//if (ShooterBody != null) {
		//	rgd.velocity += ShooterBody.velocity;
		//}
	}


	public void ApplyMomentumVelocity(Rigidbody2D r,float scale){
		//Debug.Log ("current velocity" + rgd.velocity);
		//Debug.Log ("Adding " + r.velocity * scale);
		rgd.velocity +=  (r.velocity*scale);
		//Debug.Log ("new velocity" + rgd.velocity);
	}

}
