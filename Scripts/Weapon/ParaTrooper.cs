﻿using UnityEngine;
using System.Collections;

public class ParaTrooper : MonoBehaviour {
	public AudioSource groundSound;

	public bool isFalling;
	public bool isGrounded;
	public bool isDead;
	//private SpriteRenderer sprite;
	private Rigidbody2D rgd;
	private Health health;

	void OnEnable(){
		//sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();
		rgd.bodyType = RigidbodyType2D.Dynamic;

		isDead = false;
	}



	// Use this for initialization
	void Start () {
		//sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();
		health = GetComponent<Health> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!health.isAlive) {
			rgd.isKinematic = false;
		}


		transform.parent = null;
		transform.localRotation = Quaternion.identity;

	}



	void OnTriggerEnter2D(Collider2D other){
		
		if (other.gameObject.tag == "ParatrooperSpot") {

			rgd.bodyType = RigidbodyType2D.Static;
			isGrounded = true;

		}
	}


	void OnTriggerStay2D(Collider2D other){

		if (other.gameObject.tag == "ParatrooperSpot") {

			rgd.bodyType = RigidbodyType2D.Static;
			isGrounded = true;


		}
	}



	void OnCollisionEnter2D(Collision2D col){

		//Debug.Log (col.gameObject.name + " Collided with paratrooper");

		if (col.gameObject.tag == "ParatrooperSpot") {

			rgd.isKinematic = true;

			if(groundSound)
				groundSound.Play ();

		}
			
	}



	void OnDisable(){
		rgd.isKinematic = false;
	
	}


	void OnParatrooperDeath(){
		EventManager.TriggerEvent ("ScientistDied");
		Debug.Log ("Sceintist Dies");
		isDead = true;
		rgd.bodyType = RigidbodyType2D.Static;
	}

}
