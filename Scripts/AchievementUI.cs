﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
public enum RewardType { WeaponPowerUp, CommonCargo, Upgrade, AircraftPowerUp, RareCargo, EpicCargo, FighterBlue, CamoGreen, ArticLeopard,BlackFighter,DesertEagle, FlyingBeagle};
public enum Achievement { Ace,DeathFromAbove,CampaignCommander,ExtraMile,AlwaysLoaded,OverPowered,EarthMover,GuardianAngel,MrTopGun}

public class AchievementUI : MonoBehaviour {

	public RewardsUI rewardUi;
	public Animator RewardAnim;
	//private PlayerGameData playerData;
	public GameObject AchevementNote;
	public Achievement acheviement;
	public bool Maxed;
	public int[] TierAmt;
	public int currentTier;
	public Text curAmtTxt;
	public int curAmt;
	public Slider achievementSlider;
	public Button button;

	public Image StarUI;
	public Sprite[] Stars;

	public RewardTier curRewardTier;


	public int[] RewardAmountPerTier;
	public RewardType[] RewardPerTier;

	public PlayerPowerState.AircraftPower aircraftPower;
	public PlayerPowerState.WeaponPower weaponPower;




	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
	}


	// Update is called once per frame
	void Update () {
		//
		CheckAchievementStatus ();
		CheckAwardClaims ();

	}

	public void SetUpAchievementUI(){


	}

	public void ResetAchievement(){
		curAmt = 0;
		curRewardTier = RewardTier.None;
	}



	public void CheckAchievementStatus(){
		switch (acheviement) {
		case Achievement.Ace:
			curAmt = AchievementMap.enemyunits_Air;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.enemyunits_Air;
			break;
		case Achievement.DeathFromAbove:
			curAmt = AchievementMap.secondary_kills;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.secondary_kills;
			break;
		case Achievement.CampaignCommander:
			curAmt = AchievementMap.completed;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.completed;
			break;
		case Achievement.ExtraMile:
			curAmt = AchievementMap.bonuscompleted;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.bonuscompleted;
			break;
		case Achievement.AlwaysLoaded:
			curAmt = AchievementMap.secondaryreloads;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.secondaryreloads;
			break;
		case Achievement.OverPowered:
			curAmt = AchievementMap.powerups_count;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.powerups_count;
			break;
		case Achievement.EarthMover:
			curAmt = AchievementMap.enemyunits_Terrain;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.enemyunits_Terrain;
			break;
		case Achievement.GuardianAngel:
			curAmt = AchievementMap.allies_protected;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.allies_protected;
			break;
		case Achievement.MrTopGun:
			curAmt = AchievementMap.achievements;
			curAmtTxt.text = curAmt.ToString ();
			achievementSlider.value = AchievementMap.achievements;
			break;
		default:
			break;


		}


		if (curAmt < TierAmt [currentTier]) {
			curAmtTxt.text += "/" + TierAmt [currentTier].ToString ();
			achievementSlider.maxValue = TierAmt [currentTier];
			StarUI.overrideSprite = Stars [currentTier];
		} else if (curAmt >= TierAmt [currentTier]) {

			if (currentTier == TierAmt.Length-1) {

				curAmtTxt.text  = TierAmt [currentTier].ToString () + "/" + TierAmt [currentTier].ToString ();
				achievementSlider.maxValue = TierAmt [currentTier];
				StarUI.overrideSprite = Stars [currentTier+1];
				Maxed = true;
			} else {
				currentTier++;
				curAmtTxt.text  += "/" + TierAmt [currentTier].ToString ();
				achievementSlider.maxValue = TierAmt [currentTier];
				StarUI.overrideSprite = Stars [currentTier];
			}
		}
			
	}



	public void CheckAwardClaims(){
		//Debug.Log ("Checking Award Claims...");
		button.interactable = false;
		button.onClick.RemoveAllListeners ();


		AchevementNote.SetActive(false);
		switch (currentTier) {



		case 2:
			if (Maxed) {
				curRewardTier = RewardTier.ThirdTier;
			} else {
				curRewardTier = RewardTier.SecondTier;
				//Debug.Log ("Assign reward tier...");
			}
			break;
		case 1:
			curRewardTier = RewardTier.FirstTier;
			break;
		case 0:
			curRewardTier = RewardTier.None;
			break;
		default:
			break;
		}

		//Debug.Log (curRewardTier.ToString ());
		//Debug.Log (currentTier.ToString ());

		//Debug.Log(RewardAnim.GetCurrentAnimatorStateInfo(1).IsName("PopUpReward").ToString());

		if(RewardAnim.GetCurrentAnimatorStateInfo(1).IsName("PopUpReward")){
			button.onClick.AddListener(() => RewardAnim.SetTrigger("PopUpReset"));
		}

		button.onClick.AddListener(() => RewardAnim.SetTrigger("PopUpShow"));
		switch (acheviement) {

		case Achievement.Ace:


			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				
				if (GameDataManager.instance.LoadedTempPlayerData.AceRT == RewardTier.None) {

					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);



				}
			}
				
			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.AceRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
				
				}
			}


			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.AceRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);

					
					
				}

			}

			break;
		case Achievement.DeathFromAbove:

			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.DeathFromAboveRT == RewardTier.None) {
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
				
					
				}
			}

			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.DeathFromAboveRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}


			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.DeathFromAboveRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
				}

			}
			break;
		case Achievement.CampaignCommander:

			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.CampaignCommanderRT== RewardTier.None) {
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
				}
			}

			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.CampaignCommanderRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
				}
			}


			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.CampaignCommanderRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}

			}


			break;
		case Achievement.ExtraMile:
			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.ExtraMileRT == RewardTier.None) {
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
					
				}
			}
			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.ExtraMileRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
				}
			}
			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.ExtraMileRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}

			}
			break;
		case Achievement.AlwaysLoaded:
			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.AlwaysLoadedRT == RewardTier.None) {
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}

			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.AlwaysLoadedRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}


			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.AlwaysLoadedRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}

			}
			break;
		case Achievement.OverPowered:
			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.OverPoweredRT == RewardTier.None) {
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}

			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.OverPoweredRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}
				
			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.OverPoweredRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}

			}
			break;
		case Achievement.EarthMover:

			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.EarthMoverRT == RewardTier.None) {
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}

			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.EarthMoverRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}


			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.EarthMoverRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
					
				}

			}
			break;
		case Achievement.GuardianAngel:
			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.GuardianAngelRT == RewardTier.None) {
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);

				}
			}

			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.GuardianAngelRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}


			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.GuardianAngelRT == RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
				}

			}

			break;
		case Achievement.MrTopGun:
			if ((curRewardTier == RewardTier.FirstTier) || (curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {

				if (GameDataManager.instance.LoadedTempPlayerData.MrTopGunRT == RewardTier.None) {
					
					button.onClick.AddListener (() => FirstTierPrizeAward ());
					button.interactable = true;

					AchevementNote.SetActive(true);
				}
			}

			if ((curRewardTier == RewardTier.SecondTier) || (curRewardTier == RewardTier.ThirdTier)) {
				if (GameDataManager.instance.LoadedTempPlayerData.MrTopGunRT == RewardTier.FirstTier) {
					button.onClick.AddListener (() => SecondTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}
			}


			if (curRewardTier == RewardTier.ThirdTier) {

				if (GameDataManager.instance.LoadedTempPlayerData.MrTopGunRT== RewardTier.SecondTier) {
					button.onClick.AddListener (() => ThirdTierPrizeAward ());
					button.interactable = true;
					AchevementNote.SetActive(true);
					
				}

			}

			break;

		}
	}



	public void FirstTierPrizeAward(){
		button.interactable = false;
		button.onClick.RemoveAllListeners ();

		switch (RewardPerTier [0]) {
		case RewardType.CommonCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainCommonCargoBox (RewardAmountPerTier [0]);
			rewardUi.SetUpCommonCargoReward (RewardAmountPerTier [0]);
			break;
		case RewardType.Upgrade:
			GameDataManager.instance.LoadedTempPlayerData.GainShipUpgrades (RewardAmountPerTier [0]);
			rewardUi.SetUpUpgradeReward(RewardAmountPerTier [0]);
			break;
		case RewardType.WeaponPowerUp:
			GameDataManager.instance.LoadedTempPlayerData.GainWeaponPowerUps (weaponPower, RewardAmountPerTier [0]);
			rewardUi.SetUpWeaponPowerReward (weaponPower, RewardAmountPerTier [0]);
			break;
		case RewardType.AircraftPowerUp:
			GameDataManager.instance.LoadedTempPlayerData.GainAircraftPowerUps (aircraftPower, RewardAmountPerTier [0]);
			rewardUi.SetUpAircraftPowerReward (aircraftPower, RewardAmountPerTier [0]);
			break;
		case RewardType.RareCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainRareCargoBox (RewardAmountPerTier [0]);
			rewardUi.SetUpRareCargoReward (RewardAmountPerTier [0]);
			break;
		case RewardType.EpicCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainEpicCargoBox (RewardAmountPerTier [0]);
			rewardUi.SetUpEpicCargoReward (RewardAmountPerTier [0]);
			break;
		case RewardType.FighterBlue:
			GameDataManager.instance.LoadedTempPlayerData.FighterBlueSkin = true;
			rewardUi.SetUpSkinReward (Skins.FighterBlue);
			break;
		case RewardType.CamoGreen:
			GameDataManager.instance.LoadedTempPlayerData.CamoGreenSkin = true;
			rewardUi.SetUpSkinReward (Skins.Camogreen);
			break;
		case RewardType.ArticLeopard:
			GameDataManager.instance.LoadedTempPlayerData.ArticLeopardSkin = true;
			rewardUi.SetUpSkinReward (Skins.ArticLeopard);
			break;
		case RewardType.BlackFighter:
			GameDataManager.instance.LoadedTempPlayerData.BlackTigerSkin = true;
			rewardUi.SetUpSkinReward (Skins.BlackTiger);
			break;
		case RewardType.DesertEagle:
			GameDataManager.instance.LoadedTempPlayerData.DesertEagleSkin = true;
			rewardUi.SetUpSkinReward (Skins.DesertEagle);
			break;
		case RewardType.FlyingBeagle:
			GameDataManager.instance.LoadedTempPlayerData.FlyingBeagleSkin = true;
			rewardUi.SetUpSkinReward (Skins.FlyingBeagle);
			break;
		default:
			break;
		}

		switch (acheviement) {

		case Achievement.Ace:
			GameDataManager.instance.LoadedTempPlayerData.AceRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_ace_lieutenant,100f,(bool success) => { Debug.Log("Ace Lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_ace_lieutenant,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.DeathFromAbove:
			GameDataManager.instance.LoadedTempPlayerData.DeathFromAboveRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_death_from_above_lieutenant,100f,(bool success) => { Debug.Log("DeathFromAbove Lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_death_from_above_lieutenant,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.CampaignCommander:
			GameDataManager.instance.LoadedTempPlayerData.CampaignCommanderRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_campaign_commander_lieutenant,100f,(bool success) => { Debug.Log("Campaign commander Lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_campaign_commander_lieutenant,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.ExtraMile:
			GameDataManager.instance.LoadedTempPlayerData.ExtraMileRT = RewardTier.FirstTier;
			break;
		case Achievement.AlwaysLoaded:
			GameDataManager.instance.LoadedTempPlayerData.AlwaysLoadedRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_always_loaded_lieutenant,100f,(bool success) => { Debug.Log("Always Loaded lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_always_loaded_lieutenant,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.OverPowered:
			GameDataManager.instance.LoadedTempPlayerData.OverPoweredRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_over_powered_lieutenant,100f,(bool success) => { Debug.Log("Over Powered lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_over_powered_lieutenant,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.EarthMover:
			GameDataManager.instance.LoadedTempPlayerData.EarthMoverRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_earth_mover_lieutenant,100f,(bool success) => { Debug.Log("Earth Mover lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_earth_mover_lieutenant,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.GuardianAngel:
			GameDataManager.instance.LoadedTempPlayerData.GuardianAngelRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_guardian_angel_lieutenant,100f,(bool success) => { Debug.Log("Guardian Angel lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_guardian_angel_lieutenant,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.MrTopGun:
			GameDataManager.instance.LoadedTempPlayerData.MrTopGunRT = RewardTier.FirstTier;
			#if UNITY_ANDROID
        	((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_mr_top_gun_lieutenant,100f,(bool success) => { Debug.Log("Mr Top Gun lieutenant Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
         	Social.Active.ReportProgress(GPGSIds.achievement_mr_top_gun_lieutenant,100f,(bool success) => {});
			#endif
			break;
		}
		//rewardUi.gameObject.SetActive (false);
		//rewardUi.gameObject.SetActive (true);
		GameDataManager.instance.SaveTempPlayer();
		CheckAwardClaims ();
	}


	public void SecondTierPrizeAward(){

		button.interactable = false;
		button.onClick.RemoveAllListeners ();

		switch (RewardPerTier [1]) {

		case RewardType.CommonCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainCommonCargoBox (RewardAmountPerTier [1]);
			rewardUi.SetUpCommonCargoReward (RewardAmountPerTier [1]);
			break;
		case RewardType.Upgrade:
			GameDataManager.instance.LoadedTempPlayerData.GainShipUpgrades (RewardAmountPerTier [1]);
			rewardUi.SetUpUpgradeReward(RewardAmountPerTier [1]);
			break;
		case RewardType.WeaponPowerUp:
			GameDataManager.instance.LoadedTempPlayerData.GainWeaponPowerUps (weaponPower, RewardAmountPerTier [1]);
			rewardUi.SetUpWeaponPowerReward (weaponPower, RewardAmountPerTier [1]);
			break;
		case RewardType.AircraftPowerUp:
			GameDataManager.instance.LoadedTempPlayerData.GainAircraftPowerUps (aircraftPower, RewardAmountPerTier [1]);
			rewardUi.SetUpAircraftPowerReward (aircraftPower, RewardAmountPerTier [1]);
			break;
		case RewardType.RareCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainRareCargoBox (RewardAmountPerTier [0]);
			rewardUi.SetUpRareCargoReward (RewardAmountPerTier [0]);
			break;
		case RewardType.EpicCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainEpicCargoBox (RewardAmountPerTier [0]);
			rewardUi.SetUpEpicCargoReward (RewardAmountPerTier [0]);
			break;
		case RewardType.FighterBlue:
			GameDataManager.instance.LoadedTempPlayerData.FighterBlueSkin = true;
			rewardUi.SetUpSkinReward (Skins.FighterBlue);
			break;
		case RewardType.CamoGreen:
			GameDataManager.instance.LoadedTempPlayerData.CamoGreenSkin = true;
			rewardUi.SetUpSkinReward (Skins.Camogreen);
			break;
		case RewardType.ArticLeopard:
			GameDataManager.instance.LoadedTempPlayerData.ArticLeopardSkin = true;
			rewardUi.SetUpSkinReward (Skins.ArticLeopard);
			break;
		case RewardType.BlackFighter:
			GameDataManager.instance.LoadedTempPlayerData.BlackTigerSkin = true;
			rewardUi.SetUpSkinReward (Skins.BlackTiger);
			break;
		case RewardType.DesertEagle:
			GameDataManager.instance.LoadedTempPlayerData.DesertEagleSkin = true;
			rewardUi.SetUpSkinReward (Skins.DesertEagle);
			break;
		case RewardType.FlyingBeagle:
			GameDataManager.instance.LoadedTempPlayerData.FlyingBeagleSkin = true;
			rewardUi.SetUpSkinReward (Skins.FlyingBeagle);
			break;
		default:
			break;
		}

		switch (acheviement) {

		case Achievement.Ace:
			GameDataManager.instance.LoadedTempPlayerData.AceRT = RewardTier.SecondTier;

			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_ace_colonel,100f,(bool success) => { Debug.Log("Ace colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_ace_colonel,100f,(bool success) => {});
			#endif

			break;
		case Achievement.DeathFromAbove:
			GameDataManager.instance.LoadedTempPlayerData.DeathFromAboveRT = RewardTier.SecondTier;

			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_death_from_above_colonel,100f,(bool success) => { Debug.Log("death from above colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_death_from_above_colonel,100f,(bool success) => {}); 
			#endif


			break;
		case Achievement.CampaignCommander:
			GameDataManager.instance.LoadedTempPlayerData.CampaignCommanderRT = RewardTier.SecondTier;

			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_campaign_commander_colonel,100f,(bool success) => { Debug.Log("campaign commander colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_campaign_commander_colonel,100f,(bool success) => {}); 
			#endif

			break;
		case Achievement.ExtraMile:
			GameDataManager.instance.LoadedTempPlayerData.ExtraMileRT = RewardTier.SecondTier;

			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_extra_mile_colonel,100f,(bool success) => { Debug.Log("extra mile colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_extra_mile_colonel,100f,(bool success) => {}); 
			#endif

			break;
		case Achievement.AlwaysLoaded:
			GameDataManager.instance.LoadedTempPlayerData.AlwaysLoadedRT = RewardTier.SecondTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_always_loaded_colonel,100f,(bool success) => { Debug.Log(" always loaded colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			//Social.Active.ReportProgress(GPGSIds.achievement_always_loaded_colonel,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.OverPowered:
			GameDataManager.instance.LoadedTempPlayerData.OverPoweredRT = RewardTier.SecondTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_over_powered_colonel,100f,(bool success) => { Debug.Log("over powered colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_over_powered_colonel,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.EarthMover:
			GameDataManager.instance.LoadedTempPlayerData.EarthMoverRT = RewardTier.SecondTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_earth_mover_colonel,100f,(bool success) => { Debug.Log("earth mover colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_earth_mover_colonel,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.GuardianAngel:
			GameDataManager.instance.LoadedTempPlayerData.GuardianAngelRT = RewardTier.SecondTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_guardian_angel_colonel,100f,(bool success) => { Debug.Log("Guardian angel colonel Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_guardian_angel_colonel,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.MrTopGun:
			GameDataManager.instance.LoadedTempPlayerData.MrTopGunRT = RewardTier.SecondTier;
            #if UNITY_ANDROID
                ((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_mr_top_gun_colonel, 100f, (bool success) => { Debug.Log("Guardian angel colonel Acheivement:" + success); }); // Show current (Active) leaderboard 
            #endif
            #if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_mr_top_gun_colonel,100f,(bool success) => {}); 
            #endif
            break;
		}
		//rewardUi.gameObject.SetActive (false);
		//rewardUi.gameObject.SetActive (true);
		//button.onClick.AddListener(() => RewardAnim.SetTrigger("PopUpReset"));
		GameDataManager.instance.SaveTempPlayer();
		CheckAwardClaims ();
	}


	public void ThirdTierPrizeAward(){

		button.interactable = false;
		button.onClick.RemoveAllListeners ();

		switch (RewardPerTier [2]) {
		case RewardType.CommonCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainCommonCargoBox (RewardAmountPerTier [2]);
			rewardUi.SetUpCommonCargoReward (RewardAmountPerTier [2]);
			break;
		case RewardType.Upgrade:
			GameDataManager.instance.LoadedTempPlayerData.GainShipUpgrades (RewardAmountPerTier [2]);
			rewardUi.SetUpUpgradeReward(RewardAmountPerTier [2]);
			break;
		case RewardType.WeaponPowerUp:
			GameDataManager.instance.LoadedTempPlayerData.GainWeaponPowerUps (weaponPower, RewardAmountPerTier [2]);
			rewardUi.SetUpWeaponPowerReward (weaponPower, RewardAmountPerTier [2]);
			break;
		case RewardType.AircraftPowerUp:
			GameDataManager.instance.LoadedTempPlayerData.GainAircraftPowerUps (aircraftPower, RewardAmountPerTier [2]);
			rewardUi.SetUpAircraftPowerReward (aircraftPower, RewardAmountPerTier [2]);
			break;
		case RewardType.RareCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainRareCargoBox (RewardAmountPerTier [0]);
			rewardUi.SetUpRareCargoReward (RewardAmountPerTier [0]);
			break;
		case RewardType.EpicCargo:
			GameDataManager.instance.LoadedTempPlayerData.GainEpicCargoBox (RewardAmountPerTier [0]);
			rewardUi.SetUpEpicCargoReward (RewardAmountPerTier [0]);
			break;
		case RewardType.FighterBlue:
			GameDataManager.instance.LoadedTempPlayerData.FighterBlueSkin = true;
			rewardUi.SetUpSkinReward (Skins.FighterBlue);
			break;
		case RewardType.CamoGreen:
			GameDataManager.instance.LoadedTempPlayerData.CamoGreenSkin = true;
			rewardUi.SetUpSkinReward (Skins.Camogreen);
			break;
		case RewardType.ArticLeopard:
			GameDataManager.instance.LoadedTempPlayerData.ArticLeopardSkin = true;
			rewardUi.SetUpSkinReward (Skins.ArticLeopard);
			break;
		case RewardType.BlackFighter:
			GameDataManager.instance.LoadedTempPlayerData.BlackTigerSkin = true;
			rewardUi.SetUpSkinReward (Skins.BlackTiger);
			break;
		case RewardType.DesertEagle:
			GameDataManager.instance.LoadedTempPlayerData.DesertEagleSkin = true;
			rewardUi.SetUpSkinReward (Skins.DesertEagle);
			break;
		case RewardType.FlyingBeagle:
			GameDataManager.instance.LoadedTempPlayerData.FlyingBeagleSkin = true;
			rewardUi.SetUpSkinReward (Skins.FlyingBeagle);
			break;
		default:
			break;
		}

		switch (acheviement) {

		case Achievement.Ace:
			GameDataManager.instance.LoadedTempPlayerData.AceRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_ace_general,100f,(bool success) => { Debug.Log("Ace General Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif

			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_ace_general,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.DeathFromAbove:
			GameDataManager.instance.LoadedTempPlayerData.DeathFromAboveRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_death_from_above_general,100f,(bool success) => { Debug.Log("death from Above general Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_death_from_above_general,100f,(bool success) => {}); 
			#endif
		
			break;
		case Achievement.CampaignCommander:
			GameDataManager.instance.LoadedTempPlayerData.CampaignCommanderRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_campaign_commander_general,100f,(bool success) => { Debug.Log("campaign commander general Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_campaign_commander_general,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.ExtraMile:
			GameDataManager.instance.LoadedTempPlayerData.ExtraMileRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_extra_mile_general,100f,(bool success) => { Debug.Log("extra mile general Acheivement:" + success);}); // Show current (Active) leaderboard 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_extra_mile_general,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.AlwaysLoaded:
			GameDataManager.instance.LoadedTempPlayerData.AlwaysLoadedRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_always_loaded_general,100f,(bool success) => { Debug.Log("always loaded general Acheivement:" + success);}); 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_always_loaded_general,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.OverPowered:
			GameDataManager.instance.LoadedTempPlayerData.OverPoweredRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_over_powered_general,100f,(bool success) => { Debug.Log("over powered general Acheivement:" + success);}); 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_over_powered_general,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.EarthMover:
			GameDataManager.instance.LoadedTempPlayerData.EarthMoverRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_earth_mover_general,100f,(bool success) => { Debug.Log("earth mover general Acheivement:" + success);}); 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_earth_mover_general,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.GuardianAngel:
			GameDataManager.instance.LoadedTempPlayerData.GuardianAngelRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_guardian_angel_general,100f,(bool success) => { Debug.Log("guardian angel general Acheivement:" + success);}); 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_guardian_angel_general,100f,(bool success) => {}); 
			#endif
			break;
		case Achievement.MrTopGun:
			GameDataManager.instance.LoadedTempPlayerData.MrTopGunRT = RewardTier.ThirdTier;
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ReportProgress(GPGSIds.achievement_mr_top_gun_general,100f,(bool success) => { Debug.Log("mr top gun general Acheivement:" + success);}); 
			#endif
			#if UNITY_IPHONE
			Social.Active.ReportProgress(GPGSIds.achievement_mr_top_gun_general,100f,(bool success) => {}); 
			#endif
			break;
		}
	
		//rewardUi.gameObject.SetActive (false);
		//rewardUi.gameObject.SetActive (true);
		GameDataManager.instance.SaveTempPlayer();
		CheckAwardClaims ();
	}








}
