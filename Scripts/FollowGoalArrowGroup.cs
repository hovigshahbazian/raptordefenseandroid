﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FollowGoalArrowGroup : MonoBehaviour {

	private ObjectPool arrowpool;
	public RectTransform arrowPivotTransfrom;

	// Use this for initialization
	void Start () {
		arrowpool = GetComponent<ObjectPool> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void SetFollowArrow(Transform t,GoalFlag g){

		GameObject obj = arrowpool.GetPooledObject ();

		if (obj == null)
			return;
		
		obj.transform.GetChild(0).GetComponent<FollowArrowUI>().faceToObject.target = t;
		obj.transform.GetChild(0).GetComponent<FollowArrowUI>().goalFlag = g;
		obj.GetComponent<RectTransform> ().SetParent (transform);
		obj.GetComponent<RectTransform> ().localScale = arrowPivotTransfrom.localScale;
		obj.GetComponent<RectTransform> ().position = arrowPivotTransfrom.position;
		obj.SetActive (true);
	}

	public void SetFollowArrow(Transform t){
	}




	public void SetFollowArrow(int i, Transform t, GoalFlag g){
		//arrows [i].faceToObject.target = t;
		//arrows [i].goalFlag = g;
	}

}
