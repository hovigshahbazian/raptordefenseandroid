﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewIconUI : MonoBehaviour {

	public Button[] buttons;
	public Image icon;
	public Text text;

	// Use this for initialization
	void Start () {
		foreach (Button b in buttons) {


			if (b.interactable) {
				icon.enabled = true;
				text.enabled = true;
				break;
			} else {
				icon.enabled = false;
				text.enabled = false;
			}

		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RefreshIcon(){
        foreach (Button b in buttons)
        {


            if (b.interactable)
            {
                icon.enabled = true;
                text.enabled = true;
                break;
            }
            else
            {
                icon.enabled = false;
                text.enabled = false;
            }

        }


    }
}
