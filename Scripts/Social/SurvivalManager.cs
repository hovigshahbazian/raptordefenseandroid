﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.SceneManagement;

public class SurvivalManager : MonoBehaviour {

	public static SurvivalManager Instance;
	public static int SurvivalScore = 0;
	public GameObject leadeboardBTN;
	// Use this for initialization
	void Start () {
		Instance = this;

	}

	void Update(){

		if(leadeboardBTN != null){
			if(SceneManager.GetActiveScene().name != "CampaignMap"){
				leadeboardBTN.SetActive(false);
			}
			else{
				leadeboardBTN.SetActive(true);
			}
		}
	}


	public void AddScore(int s){
		SurvivalScore += s;
	}


	public void RestartGame(){

		#if UNITY_ANDROID
		//Debug.Log("leaderboard score sent! " + SurvivalScore);
		GooglePlayGameScript.OnOnAddScoreToLeaderBoard (SurvivalScore);
		SurvivalScore = 0;
		#endif


		#if UNITY_IPHONE
		//Debug.Log("leaderboard score sent! " + SurvivalScore);
		GooglePlayGameScript.OnOnAddScoreToLeaderBoard (SurvivalScore);
		SurvivalScore = 0;
		#endif

	}





}
