﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSwitch : MonoBehaviour {



	public GameObject[] Objects;
	public string SwitchEventName;
	private int currentActiveObject = 0;

	// Use this for initialization
	void Start () {
		


		EventManager.StartListening (SwitchEventName, Switch);

		if (Objects != null) {
			Objects [currentActiveObject].SetActive (true);
			//Debug.Log (Objects.Length);
		}
		
	}

	// Update is called once per frame
	void Update () {
		
	}


	public void Switch(){

		Objects [currentActiveObject].SetActive (false);
		currentActiveObject++;

		if (currentActiveObject >= Objects.Length) {
			currentActiveObject  %= Objects.Length;
		}

		Objects [currentActiveObject].SetActive (true);

	}

}
