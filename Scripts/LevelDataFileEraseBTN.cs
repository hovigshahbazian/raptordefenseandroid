﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelDataFileEraseBTN : MonoBehaviour {
	private Button levelDataEraseBtn;

	// Use this for initialization
	void Start () {
		levelDataEraseBtn = GetComponent<Button> ();

		levelDataEraseBtn.onClick.AddListener (() => EraseLevelDataFileClick());
	}




	public void EraseLevelDataFileClick(){
		GameDataManager.instance.EraseAllLevelGameFiles();
	}
}
