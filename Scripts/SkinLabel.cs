﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinLabel : MonoBehaviour {
	//private PlayerGameData playerData;
	public Text label;
	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
	}
	
	// Update is called once per frame
	void Update () {


		if (GameDataManager.instance.LoadedTempPlayerData.CurrentSkin == 0) {
			label.text = "Fighter Gray";
		}
		else if (GameDataManager.instance.LoadedTempPlayerData.CurrentSkin == 1) {
			label.text = "Fighter Blue";
		}
		else if (GameDataManager.instance.LoadedTempPlayerData.CurrentSkin == 2) {
			label.text = "Camo Green";
		}
		else if (GameDataManager.instance.LoadedTempPlayerData.CurrentSkin == 3) {
			label.text = "Artic Leopard";
		}
		else if (GameDataManager.instance.LoadedTempPlayerData.CurrentSkin == 4) {
			label.text = "Black Tiger";
		}
		else if (GameDataManager.instance.LoadedTempPlayerData.CurrentSkin == 5) {
			label.text = "Desert Eagle";
		}
		else if (GameDataManager.instance.LoadedTempPlayerData.CurrentSkin == 6) {
			label.text = "Flying Beagle";
		}



	}
}
