﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets._2D;

public class PlayerPowerState : MonoBehaviour {

	public int PowerUpsAcquired;

	[System.Serializable]
	public enum WeaponPower { Normal, FullAuto, Multishot, Laserbeam, ExplodingBullets };

	[System.Serializable]
	public enum AircraftPower { Normal, Invulnerability, SpeedUp, Invisibility, Shield}

	[SerializeField]
	public WeaponPower curWeaponPower;
	[SerializeField]
	public AircraftPower curAircraftPower;

	public float weaponPowerTime;
	public float aircraftPowerTime;

	public bool weaponPowerActive;
	public bool aircraftPowerActive;

	public Weapon weapon;
	public Health health;
	public GameObject sheild;

//	public float playerSpeedMulti = 1;

	private int playerSpeedBoost = 10;
	private float oriFireSpeed;


	public  Sprite invisiSprite;

	private Sprite defSprite;

	private Animator playerAnimator;


//	private SpriteRenderer sprite;

	public WeaponPowerUpStateUI powerstateUI;


	public AudioSource AutoFireSound;
	public AudioSource MultiShotSound;
	public AudioSource ExplodeBulletSound;

	public AudioSource SpeedUpSound;
	public AudioSource InvisibilitySound;



	// Use this for initialization
	void Start () {
		curWeaponPower = WeaponPower.Normal;
		curAircraftPower = AircraftPower.Normal;
		PowerUpsAcquired = 0;
		health = GetComponent<Health> ();
	//	anim = GetComponent<Animator> ();
		//sprite = GetComponent<SpriteRenderer> ();

		playerAnimator = GetComponent<Animator> ();


	}
	
	// Update is called once per frame
	void Update () {

		if (weaponPowerActive) {

			weaponPowerTime -= Time.deltaTime;
			if (weaponPowerTime <= 0) {
				EndWeaponPower(curWeaponPower);
			}
		}

		if (aircraftPowerActive) {
			
			aircraftPowerTime -= Time.deltaTime;
			if (aircraftPowerTime <= 0) {
				EndAircraftPower (curAircraftPower);
			}
		}
	
	}

	void StartWeaponPower(){
		weaponPowerActive = true;
		PowerUpsAcquired++;

		if (powerstateUI != null && powerstateUI.image != null) {
			//Debug.Log(curWeaponPower);
			powerstateUI.SetWeaponUI (curWeaponPower);
		}
	
	}

	void StartAircraftPower(){
		aircraftPowerActive = true;
		PowerUpsAcquired++;
	
	}

	void EndWeaponPower(WeaponPower wp){
		weaponPowerActive = false;
		weaponPowerTime = 0;
		curWeaponPower = WeaponPower.Normal;


		if (powerstateUI != null && powerstateUI.image != null) {
			powerstateUI.SetWeaponUI (curWeaponPower);
		}


		if (wp == WeaponPower.FullAuto) {
			weapon.fireRate = oriFireSpeed;
			weapon.heatEnabled = true;
		} else if (wp == WeaponPower.Multishot) {
			weapon.tripleShootMode = false;
		} else if (wp == WeaponPower.Laserbeam) {
			weapon.laserShootMode = false;
			weapon.lazerBullet.StopLazer();
		} else if (wp == WeaponPower.ExplodingBullets) {
			weapon.explodeShootMode = false;
		}	

	}

	void EndAircraftPower(AircraftPower ap){
		aircraftPowerActive = false;
		aircraftPowerTime = 0;
		curAircraftPower = AircraftPower.Normal;


		if (ap == AircraftPower.Invulnerability)
		{
			health.invulnerble = false;
			gameObject.tag= "Player";
			playerAnimator.SetTrigger("UnInvulnerable");
		}
		else if (ap == AircraftPower.SpeedUp)
		{
			//playerSpeedMulti = 1;
			GetComponent<PlatformerCharacter2D>().m_MaxSpeed -= playerSpeedBoost;
			//GetComponent<SpeedBoost> ().speedIncrease = 10;
		} 
		else if (ap == AircraftPower.Invisibility)
		{
			gameObject.tag= "Player";
			//anim.SetBool ("stealth", false);
			playerAnimator.SetTrigger("unStealth");
			//sprite.sprite = defSprite;
		
		}


	}

	public void NormalWeapon(){
		curWeaponPower = WeaponPower.Normal;
	}

	public void FullAuto(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.FullAuto;
	//	Debug.Log(curWeaponPower.ToString());
		weaponPowerTime += time;
		if(AutoFireSound)
			AutoFireSound.Play();
	//	Debug.Log("Full Auto activated");
		oriFireSpeed = weapon.fireRate;
		weapon.fireRate = 10;
		weapon.heatEnabled = false;

		StartWeaponPower ();
	}

	public void Multishot(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.Multishot;
		weaponPowerTime += time;

		if(MultiShotSound)
			MultiShotSound.Play();
		//Debug.Log("Mult shot activated");
		weapon.tripleShootMode = true;
		StartWeaponPower ();

	}

	public void LaserBeam(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.Laserbeam;
		weapon.lazerBullet.ChargeUp();
		weapon.laserShootMode = true;
		weaponPowerTime += time;
		StartWeaponPower ();
	}
		
	public void ExplodingBullets(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.ExplodingBullets;
		weaponPowerTime += time;
		weapon.explodeShootMode = true;
		StartWeaponPower ();
	}

	public void NormalAircraft(float time){
		curAircraftPower = AircraftPower.Normal;
	}

	public void Invulnerabiliy(float time){

		if (health == null)
			return;
        Debug.Log("Invulnerable");
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.Invulnerability;
		aircraftPowerTime += time;
		health.invulnerble = true;
		gameObject.tag= "PlayerInvul";
		playerAnimator.SetTrigger("Invulnerable");
		StartAircraftPower ();
	}

	public void Speedup(float time){
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.SpeedUp;
		aircraftPowerTime += time;
		//playerSpeedMulti = playerSpeedBoost;

		GetComponent<PlatformerCharacter2D>().m_MaxSpeed += playerSpeedBoost;
		//GetComponent<SpeedBoost> ().speedIncrease = 0;
		StartAircraftPower ();

		if(SpeedUpSound)
			SpeedUpSound.Play();
	}

	public void Invisibility(float time){
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.Invisibility;
		aircraftPowerTime += time;
		gameObject.tag= "PlayerStealthed";
    
		playerAnimator.SetTrigger("Stealth");
		StartAircraftPower ();

		if (InvisibilitySound)
			InvisibilitySound.Play ();
	}

	public void Sheild(float time){
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.Shield;
		sheild.SetActive (true);
		sheild.GetComponent<Health> ().isAlive = true;
		sheild.GetComponent<SpriteRenderer> ().enabled = true;
		aircraftPowerTime += time;
		StartAircraftPower ();
	}




	public void DisplayPowerUpActivated(PowerUp.PowerUps power){


		switch (power) {
		case PowerUp.PowerUps.FullAuto:
			
			break;
		case PowerUp.PowerUps.Multishot:
	
			break;
		case PowerUp.PowerUps.Laserbeam:
			
			break;
		case PowerUp.PowerUps.ExplodingBullets:
			
			break;
		case PowerUp.PowerUps.Invulnerability:

			break;
		case PowerUp.PowerUps.SpeedUp:

			break;
		case PowerUp.PowerUps.Invisibility:
			
			break;
		case PowerUp.PowerUps.Sheild:

			break;
		default:
			break;
		}





	}





}
