﻿using UnityEngine;
using System.Collections;

public class AllyPlane : MonoBehaviour {

	public ParticleSystem explosion;
	public string message;


	public void OnAllyDestroy(){
		explosion.Play();
		EventManager.TriggerEvent ("AllyDestroyed");

		if (message != "") {
			SendMessage (message);
		}
	}



}
