﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int HealthPoints;
	public int MaxHealth = 5;
	public bool isAlive;
	public bool invulnerble;
	public string OnHitFunctionName;
	public string OnDeathFuncName;
	public string OnPercentHealthFunc;
	[Range(0f,1f)]
	public float PercentHealthTrigger;
	public int deathDurTime;

	public AudioSource DamageSound;
	public AudioSource DeathSound;
	private SpriteRenderer sprite;

	public  bool haveInvincibilityFrames = false;
	private bool invincibiltyFramesActive = false;
	public float invincibilityTime = 0;
	private float invincibilityTimer = 0;

	private bool percentHealthTriggred = false;
	void OnEnable(){

		HealthPoints = MaxHealth;
		isAlive = true;

		invincibilityTimer = invincibilityTime;

		if (sprite != null) {
			sprite.enabled = true;
		}

	}
		
	// Use this for initialization
	void Start () {
		HealthPoints = MaxHealth;

		invincibilityTimer = invincibilityTime;

		sprite = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (invincibiltyFramesActive) {

			invincibilityTimer -= Time.deltaTime;

			if (invincibilityTimer <= 0f) {
				invincibiltyFramesActive = false;
				invulnerble = false;
				invincibilityTimer = invincibilityTime;

			}
				
		}

	}

	//Reduce health by 1
	public void Damage(){

		if (invulnerble) {
			return;
		}

		SetInvulnerability ();

		if (OnHitFunctionName != "") {
			this.SendMessage (OnHitFunctionName);
		}



		HealthPoints--;
		HealthPoints = Mathf.Clamp (HealthPoints, 0, MaxHealth);




		if(OnPercentHealthFunc != "" && !percentHealthTriggred){
		//	Debug.Log( PercentHealthTrigger.ToString() + "/" + ((float)HealthPoints/(float)MaxHealth).ToString());
			if(PercentHealthTrigger >=  ((float)HealthPoints/(float)MaxHealth)){
				
				percentHealthTriggred = true;
				this.SendMessage(OnPercentHealthFunc);
			}
		}


		if (HealthPoints == 0 && isAlive) {
			//Debug.Log ("Am Dying");
			Dying();
		}
	}

	//Reduce Health by damage amount
	public void Damage(int damage){

		if (invulnerble) {
			return;
		}

		if (haveInvincibilityFrames) {
			invulnerble = true;
			invincibiltyFramesActive = true;
		}

		if (OnHitFunctionName != "") {
			this.SendMessage (OnHitFunctionName);
		}


		if(DamageSound!=null && HealthPoints > 0)
			DamageSound.Play ();

	

		HealthPoints -= damage;

		HealthPoints = Mathf.Clamp (HealthPoints, 0, MaxHealth);



		if(OnPercentHealthFunc != "" && !percentHealthTriggred){
			//Debug.Log(PercentHealthTrigger.ToString() + "/" + ((float)HealthPoints/(float)MaxHealth).ToString());

			if(PercentHealthTrigger >=  ((float)HealthPoints/(float)MaxHealth)){
				percentHealthTriggred = true;

				this.SendMessage(OnPercentHealthFunc);
			}
		}


		if (HealthPoints == 0 && isAlive) {
			Dying ();
			//Debug.Log ("Am Dying");
		}
			
	}


	public void Die(){
			HealthPoints = 0;
			this.gameObject.SetActive (false);
	//Debug.Log("Enemykilled");
	}

	public void PlayerDeath(){
		EventManager.TriggerEvent ("PlayerDeath");
		EventManager.TriggerEvent("PlayerCrash");
		GetComponent<Collider2D> ().enabled = false;
	}

	public void BossDeath(){
		EventManager.TriggerEvent ("BossDeath");
	}

	public void HalfBossHealth(){
		EventManager.TriggerEvent ("HalfBossHealth");
	}

	public void Dying(){

		    HealthPoints = 0;
			isAlive = false;

		if(OnDeathFuncName != "")
		    this.SendMessage (OnDeathFuncName);

		Invoke("Die",deathDurTime);
	
		if (DeathSound != null) {
			if(!DeathSound.isPlaying)
				DeathSound.Play ();
		}

		if (sprite != null) {
			sprite.enabled = false;
		}

			if (transform.childCount >= 0) {
				foreach (Transform t in transform.GetComponentInChildren<Transform>()) {
					t.gameObject.SetActive (false);
				}
			}
		}



	public void SetInvulnerability(){

		if (haveInvincibilityFrames) {
			invulnerble = true;
			invincibiltyFramesActive = true;
		}
	}

	public void RecoverHealth(int amt){

		HealthPoints += amt;

		HealthPoints = Mathf.Clamp (HealthPoints, 0, MaxHealth);

	}
}
