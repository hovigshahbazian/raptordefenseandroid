﻿using UnityEngine;
using System.Collections;

public class TimeReward : MonoBehaviour {

	public float secondsRequiredforReward;

	public TimerUI timer;
	public string EventToListen;
	public string eventToTrigger;

	// Use this for initialization
	void Start () {
		EventManager.StartListening(EventToListen,CheckRequirement);
	}
	
	void CheckRequirement(){

		if( timer.GetTimeLeft() >= secondsRequiredforReward)
			EventManager.TriggerEvent(eventToTrigger);
		

	}



}
