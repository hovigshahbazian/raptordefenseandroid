﻿using UnityEngine;
using System.Collections;

public class EventActiveScript : MonoBehaviour {

	public MonoBehaviour script;

	public string EventString;

	public bool active;

	// Use this for initialization
	void Start () {
		if(EventString != null)
		EventManager.StartListening(EventString, DisableScript);
	}
	
	public void DisableScript(){
		script.enabled = active;
	}

	

}
