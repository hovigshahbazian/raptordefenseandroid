﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class EventSound : MonoBehaviour {
	public GameObject Dialogue;
	private AudioSource sound;
	public string eventName;
	public bool AbleToPlay = true;

	public bool Delayed;
	private bool Stopped;
	public float Delaytime;
	public float delayTimer;
	//public bool OnlyOnce;
	//private bool Triggered;
	// Use this for initialization
	void Start () {
		sound = GetComponent<AudioSource> ();




		if(sound !=null && !sound.isPlaying)
			EventManager.StartListening (eventName,PlaySound);

	}

	void Update(){
		if (Delayed) {

			if (!AbleToPlay) {
				delayTimer -= Time.deltaTime;
			}

				if (delayTimer <= 0f) {
					AbleToPlay = true;
				} else {
					AbleToPlay = false;
				}


		}

	}



	void PlaySound(){

		if (Dialogue != null) {
			Dialogue.SetActive (true);


			if (Dialogue.GetComponent<Animator> ()) {
			//	Dialogue.GetComponent<Animator> ().SetTrigger ("Open");
			}

		}

		if(!sound.isPlaying && AbleToPlay){
			sound.Play ();


			if (Delayed) {
				delayTimer = Delaytime;
				AbleToPlay = false;
			}
		}


	}

	public void Pause(){

		if(sound.isPlaying){
			AbleToPlay = true;
		}

		sound.Pause();

	}

	public void Play(){
		if(!sound.isPlaying && AbleToPlay){
			sound.Play ();

			if (Delayed) {
				AbleToPlay = false;
				delayTimer = Delaytime;
			}

		}
	}

	public void Disable(){

		EventManager.StopListening (eventName,PlaySound);
	}
	
}
