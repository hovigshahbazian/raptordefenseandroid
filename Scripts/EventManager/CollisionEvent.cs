﻿using UnityEngine;
using System.Collections;

public class CollisionEvent : MonoBehaviour {

	public string eventName;
	public bool UseTag;
	public string TagName;
	public LayerMask targetLayerMask;
	public bool OnlyOnce;
	private bool Triggered;
	void OnTriggerEnter2D(Collider2D col){

		if ( targetLayerMask  == (targetLayerMask | (1 << col.gameObject.layer)) && !Triggered) {

			if (!UseTag) {
				EventManager.TriggerEvent (eventName);
			} else {
				if (col.tag == TagName) {
					EventManager.TriggerEvent (eventName);
				}
			}


			//Debug.Log (this.gameObject.name + " Collided with " + col.name);
		}


		if(OnlyOnce){
			Triggered = true;
		}



	}
}

