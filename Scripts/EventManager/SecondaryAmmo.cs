﻿using UnityEngine;
using System.Collections;

public class SecondaryAmmo : MonoBehaviour {
	private AudioSource collectsound;
	private Collider2D col2D;
	private SpriteRenderer sprite;

	// Use this for initialization
	void Start () {
		collectsound = GetComponent<AudioSource> ();
		col2D = GetComponent<Collider2D> ();
		sprite = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){
		EventManager.TriggerEvent ("AcquireSecondaryAmmo");

		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();

		if (cd != null) {
			//cd.SetShipUpgradeText ();
			cd.PlayCargoAnim (CollectDisplay.DisplayEvent.SecondaryAmmo);

		}


		col2D.enabled = false;
		sprite.enabled = false;
		collectsound.Play ();
	}
}
