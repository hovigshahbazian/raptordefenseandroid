﻿using UnityEngine;
using System.Collections;

public class AcheivementCounter : MonoBehaviour {


	//public LevelGameData levelData;

	public GM manager;

	public int enemyAir;
	public int enemyground;
	public int allysaved;
	public int powerUpCollected;
	public int secondaryAmmoCollected;
	public int secondaryKill;
	public int yellowDiamondsCollected;


	// Use this for initialization
	void Start () {
	
		EventManager.StartListening ("KilledAirEnemy", AddAirEnemyCounter);
		EventManager.StartListening ("KilledGroundEnemy", AddGroundEnemyCounter);
		EventManager.StartListening ("AllySaved", AddAllyProtected);
		EventManager.StartListening ("PowerUpCollected", AddPowerUpCounter);
		EventManager.StartListening ("AcquireSecondaryAmmo", AddSecondaryAmmoCounter);
		EventManager.StartListening ("SecondaryKill", AddSecondaryKill);
		EventManager.StartListening ("TallyScore", TallyAcheivements);
		EventManager.StartListening("AcquireYellowDiamond", AddYellowDiamond);
	}
	
	void AddYellowDiamond(){

		yellowDiamondsCollected++;
		yellowDiamondsCollected = Mathf.Clamp (yellowDiamondsCollected, 0, 3);
		/*
		if (GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].GoldEmblemsColleted <= yellowDiamondsCollected) {
			//Debug.Log (yellowDiamondsCollected.ToString ());
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].GoldEmblemsColleted = yellowDiamondsCollected;
		}*/

		//EventManager.TriggerEvent("TallyScore");
		//Debug.Log(yellowDiamondsCollected.ToString() + " Diamonds Collected");
	}

	void AddAirEnemyCounter(){
		enemyAir++;
		//Debug.Log(" air enemy Killed");
	}

	void AddGroundEnemyCounter(){
        Debug.Log("Ground enemy destroyed");
        enemyground++;
	}

	void AddPowerUpCounter(){
		powerUpCollected++;
	}


	void AddAllyProtected(){
		allysaved++;

	}

	void AddSecondaryAmmoCounter(){
		secondaryAmmoCollected++;
	}

	void AddSecondaryKill(){
		secondaryKill++;
		Debug.Log ("Secondary Kill");
	}

	void TallyAcheivements(){


		EventManager.StopListening ("KilledAirEnemy", AddAirEnemyCounter);
		EventManager.StopListening ("KilledGroundEnemy", AddGroundEnemyCounter);
		EventManager.StopListening ("AllySaved", AddAllyProtected);
		EventManager.StopListening ("PowerUpCollected", AddPowerUpCounter);
		EventManager.StopListening ("AcquireSecondaryAmmo", AddSecondaryAmmoCounter);
		EventManager.StopListening ("SecondaryKill", AddSecondaryKill);
		EventManager.StopListening ("TallyScore", TallyAcheivements);
		//EventManager.StopListening("AcquireYellowDiamond", AddYellowDiamond);

	

		LevelSelectorButton.currentLevel++;



		if(manager.levelName == LevelName.Level5){
			int totalStarsCollected = 0;


			for(int i = 0; i < 5;i++){
				totalStarsCollected += GameDataManager.instance.LoadedTempLevelData[i].GoldEmblemsColleted;
			}
		
			if(totalStarsCollected >= 15){
				LevelSelectorButton.currentLevel = 11;
				EventManager.TriggerEvent("BonusUnlock");
			}

		}


		if(manager.levelName == LevelName.LevelA){
			LevelSelectorButton.currentLevel = 6;
		}

		if(manager.levelName == LevelName.LevelB){
			LevelSelectorButton.currentLevel = 12;
		}

		if(manager.levelName == LevelName.Level10){
			LevelSelectorButton.currentLevel = 10;
			int totalStarsCollected = 0;

			for(int i = 5; i < 11;i++){
				totalStarsCollected += GameDataManager.instance.LoadedTempLevelData[i].GoldEmblemsColleted;
			}

			if(totalStarsCollected >= 15){
				LevelSelectorButton.currentLevel = 12;
				EventManager.TriggerEvent("BonusUnlock");
			}

		}


		if (GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].GoldEmblemsColleted < yellowDiamondsCollected) {
			//Debug.Log (yellowDiamondsCollected.ToString ());
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].GoldEmblemsColleted = yellowDiamondsCollected;
		}

		if (GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].AirEnemiesKilled < enemyAir) {
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].AirEnemiesKilled = enemyAir;
		}

		if (GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].GroundEnemiesKilled < enemyground) {
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].GroundEnemiesKilled = enemyground;
		}

		if (GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].AlliesSaved < allysaved) {
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].AlliesSaved = allysaved;

		}
		//Debug.Log ("Allies saved: " + allysaved.ToString());
		if(GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].PowerUpsCollected < powerUpCollected)
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].PowerUpsCollected = powerUpCollected;

		if(GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].SecondaryAmmoCollected < secondaryAmmoCollected)
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].SecondaryAmmoCollected = secondaryAmmoCollected;
	
		if(GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].SecondaryKills < secondaryKill)
			GameDataManager.instance.LoadedTempLevelData[(int)manager.levelName].SecondaryKills = secondaryKill;

		//Debug.Log("Level Saved");
		GameDataManager.instance.SaveTempLevel ((int)manager.levelName);
		GameDataManager.instance.SaveTempPlayer();
	}
}
