﻿using UnityEngine;
using System.Collections;

public class BasicEnemyAI : MonoBehaviour {
	public bool Untargetable;
	public BasicGun gun;
	public Animator HealthUIAnim;
	public FaceTowardObject faceTowardScr;
	public ForwardMovement ForwardMoveScr;
	public WaveMovement waveMoveScr;
	//public ProximityObject
	private bool dead = false;
	public bool goalEnemy;
	public float shootTimer;
	AudioSource deathsound;
	//private Animator anim;
	private Collider2D touchCol;
	public Collider2D damageCollider;
	private ParticleSystem ps;
	//private Emi
	private Health health;
	public SightAI sight;

	public bool autoShoot;

	private bool canSee = false;


	public bool CanSee{
		set { canSee = value;
			if (canSee) {
				if (sight != null) {
					sight.enabled = true;
				}
			} else {
				if(sight != null){
					sight.enabled = false;
				}
			}
		}
	}



	public bool KilledBySecondary;
	public SpriteRenderer Head;
//	public SpriteRenderer Body;

	void OnEnable(){
		dead = false;
		touchCol = GetComponent<Collider2D> ();
		touchCol.enabled = true;

		if(Head != null){
			Head.enabled = true;
		}

		//if(sight != null){
		//	sight.enabled = true;
		//}
		CanSee = true;


		//if (gun.gameObject != null) {
		//	gun.gameObject.SetActive (false);
		//}
		//Body = GetComponent<SpriteRenderer>();
		//Body.enabled = true;
		if(autoShoot){
			//Debug.Log(shootTimer);
			InvokeRepeating("Shoot", shootTimer,shootTimer);
		}

		ps = GetComponent<ParticleSystem> ();
		ps.Stop ();

		if (waveMoveScr != null) {
			waveMoveScr.enabled = true;
		}
		if (faceTowardScr != null)
			faceTowardScr.enabled = true;
		if (ForwardMoveScr!= null)
			ForwardMoveScr.enabled = true;
		if(damageCollider != null){
			damageCollider.enabled = true;
		}


		if (Untargetable)
			touchCol.enabled = false;
		else {
			touchCol.enabled = true;
		}

	}



	// Use this for initialization
	void Start () {
		
		health = GetComponent<Health> ();
		//if(sight != null){
		//	sight.enabled = true;
		//}
		CanSee = true;
		//anim = GetComponent<Animator> ();

		deathsound = GetComponent<AudioSource> ();
		touchCol = GetComponent<Collider2D> ();
		ps = GetComponent<ParticleSystem> ();


		//ps.Simulate(ps.duration);
		if (faceTowardScr != null)
			faceTowardScr.enabled = true;
		if (ForwardMoveScr!= null)
			ForwardMoveScr.enabled = true;
		if (waveMoveScr != null) {
			waveMoveScr.enabled = true;
		}

		if (Untargetable)
			touchCol.enabled = false;
		else {
			touchCol.enabled = true;
		}

		if(autoShoot){
		//Debug.Log(shootTimer);
			InvokeRepeating("Shoot",shootTimer,shootTimer);
		}



	}

	public bool ContainsParam(Animator _Anim, string _ParamName)
	{
		foreach (AnimatorControllerParameter param in _Anim.parameters)
		{
			if (param.name == _ParamName) return true;
		}
		return false;
	}

	// Update is called once per frame
	void Update () {
		/*
		if(SeesEnemy){
			if(anim != null){
				if(ContainsParam(anim,"SeesEnemy"))
					anim.SetBool("SeesEnemy",true);

			}
		}*/

		if (!health.isAlive && !dead) {
			
			//if(sight != null){
			//	sight.enabled = false;
			//}
			CanSee = false;

			if (KilledBySecondary) {
				EventManager.TriggerEvent ("SecondaryKill");
			}
			//Body.enabled = false;

			if (faceTowardScr != null)
				faceTowardScr.enabled = false;
			
			if (ForwardMoveScr!= null)
				ForwardMoveScr.enabled = false;
			
			if (waveMoveScr != null) {
				waveMoveScr.enabled = false;
			}
			if(gun != null)
				gun.CancelInvoke ();
			
			CancelInvoke ();


			if (goalEnemy) {
				if (LayerMask.LayerToName (gameObject.layer) == "enemy") {
					if (gameObject.tag == "Boss") {
						EventManager.TriggerEvent ("BossDestroyed");
					} else {
						EventManager.TriggerEvent ("KilledAirEnemy");
					}
				} else if (LayerMask.LayerToName (gameObject.layer) == "groundEnemy") {
					EventManager.TriggerEvent ("KilledGroundEnemy");
				} 


			}
			StartCoroutine ("DeathAnimateCo");
			dead = true;
			touchCol.enabled = false;

			if(Head != null){
				Head.enabled = false;
			}
			if(damageCollider != null){
				damageCollider.enabled = false;
			}
			if (transform.childCount >= 0) {
				foreach (Transform t in transform.GetComponentInChildren<Transform>()) {
					t.gameObject.SetActive (true);
				}
			}



		}
	}

	void Shoot(){


		if (gun != null) {
				gun.Shoot ();
			//Debug.Log("PlayAnim");

		}
	}
		
	void OnDisable(){
		if(gun != null)
			gun.CancelInvoke ();
		CancelInvoke ();

		//if(sight != null){
	//		sight.enabled = false;
		//}

		CanSee = false;
		//if(anim != null)
		//	anim.SetBool("Dead", false);
		
	}


	void DeathAnimate(){
	//	if(anim != null)
	//		anim.SetBool("Dead", true);
		
		if (!deathsound.isPlaying) {
			deathsound.Play ();
		}
	}


	IEnumerator DeathAnimateCo(){
		//if(anim != null)
		//	anim.SetBool("Dead", true);
		//ps.Simulate(0.5f,false,true);

		ps.Play ();
		if (deathsound != null) {
			//Debug.Log ("Dead");
			if (!deathsound.isPlaying) {
				deathsound.Play ();
			}
		}
//		SendMessage ("SpawnItem");
		yield return new WaitForSeconds(2f);
	}

	void OnTriggerEnter2D(Collider2D col){
	//	Debug.Log(col.name + " Collided with " + this.name);
		//InvokeRepeating("Shoot", shootTimer,shootTimer);
		//SeesEnemy = true;

		/*
		if (col.gameObject.layer == LayerMask.NameToLayer("playerbullet")){
			if(HealthUIAnim !=null)
				HealthUIAnim.SetTrigger ("Damage");
		}*/


		if (col.tag == "SecondaryWeapon") {
			KilledBySecondary = true;
		} else {
			KilledBySecondary = false;
		}

	}





}
