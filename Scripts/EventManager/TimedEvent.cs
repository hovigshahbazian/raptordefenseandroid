﻿using UnityEngine;
using System.Collections;

public class TimedEvent : MonoBehaviour {

	public float secondsTillEvent;
	public string Message;
	public string eventToTrigger;

	public float timeLeft;

	public bool TimerActive = true;
	public bool RestartOnEnable;
	public bool StopTimerOnEnable;

	void OnEnable(){
		if (RestartOnEnable) {
			ResetTimer ();
			StartTimer ();

		}

		if (StopTimerOnEnable) {
			StopTimer ();
		}


	}

	void OnAwake(){
		
	}

	// Use this for initialization
	void Start () {
		timeLeft = secondsTillEvent;
	}

	void Update(){

		if (TimerActive) {
			timeLeft -= Time.deltaTime;


			if (timeLeft <= 0.0f) {
				TimerActive = false;


				if(!RestartOnEnable)
				    enabled = false;

				if(Message != "")
					SendMessage (Message);

				if (eventToTrigger != "")
				EventManager.TriggerEvent (eventToTrigger);
			}
		}


	}

	public void ResetTimer(){
		timeLeft = secondsTillEvent;
	}

	public void StopTimer(){
		TimerActive = false;
	}

	public void StartTimer(){
		TimerActive = true;
	}
		
}
