﻿using UnityEngine;
using System.Collections;

public class EventCounter : MonoBehaviour {


	public int eventCounterLimit;

	//public bool enoughEventsCounter;


	public string EventToListen;
	public string MessageToSend;
	public string eventToTrigger;

	public int eventCounter;
	// Use this for initialization
	void Start () {
		EventManager.StartListening(EventToListen, IncrementEventCounter);
		eventCounter = 0;
	}


	void IncrementEventCounter(){
		eventCounter++;

		if(eventCounter >= eventCounterLimit){

			EventManager.StopListening(EventToListen, IncrementEventCounter);
			if(MessageToSend != "")
				SendMessage(MessageToSend);	

			if(eventToTrigger != "")
				EventManager.TriggerEvent(eventToTrigger);
			
			//Debug.Log (MessageToSend + " Is Triggered");


			//EventManager.StopListening(EventToListen,IncrementEventCounter);
		}
	}

	public void ResetCounter(){
		eventCounter = 0;
	}

	void OnDisable(){
		EventManager.StopListening (EventToListen, IncrementEventCounter);
	}

}
