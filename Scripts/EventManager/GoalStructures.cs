﻿using UnityEngine;
using System.Collections;

public class GoalStructures : MonoBehaviour {


	[SerializeField]
	private ParticleSystem DamageParticle;
	[SerializeField]
	private AudioSource DamageSound;
	[SerializeField]
	private ParticleSystem DeathParticle;
	[SerializeField]
	private AudioSource DeathSound;
	private Collider2D col;


	void Start(){
		col = GetComponent<Collider2D>();
	}

	public void OnStructDestroy(){
		col.enabled = false;
		EventManager.TriggerEvent ("StructureDestroyed");

		PlayDeath();
	}

	public void OnTriggerEnter2D( Collider2D col){
		if(DamageParticle != null)
			DamageParticle.Play();

		if(DamageSound != null){
			if(!DamageSound.isPlaying)
				DamageSound.Play();
		}

	}

	public void PlayDeath(){
		if(DeathParticle != null)
		DeathParticle.Play();
		if(DeathSound != null)
		DeathSound.Play();
	}
		
}
