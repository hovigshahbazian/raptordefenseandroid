﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthNumberUI : MonoBehaviour {
	public Health health;
	public Text numberUi;
	public 
	// Use this for initialization
	void Start () {

		float num = health.HealthPoints/10f;


		numberUi.text = num.ToString()  + "%";

	}
	
	// Update is called once per frame
	void Update () {
		float num = health.HealthPoints/10f;

		numberUi.text = num.ToString () + "%";
	}
}
