﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DistanceActive : MonoBehaviour {

	public Transform Target;

	public float distance;
	public GameObject obj;
	public bool OnlyOnce;

	public string GameObjectTag;

	// Use this for initialization
	void Start () {

		//if (GameObjectTag != "") {
			Target = GameObject.FindGameObjectWithTag (GameObjectTag).transform;
		//}

	}

    // Update is called once per frame
    void Update() {
        if (Target != null) { 
        if (Vector3.Distance(Target.position, transform.position) <= distance) {
            obj.SetActive(true);
            if (OnlyOnce) {
                enabled = false;
            }
        } else {
            obj.SetActive(false);
        }
    }

	}
}
