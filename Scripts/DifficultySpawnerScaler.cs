﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultySpawnerScaler : MonoBehaviour {

	private EnemySpawner spawner;
	public int defaultTime =10;
	public bool higherFeuquency;

	[Range(1,99)]
	public int difficultyMod = 1;

	public void OnEnable(){
		spawner = GetComponent<EnemySpawner> ();

		if(!higherFeuquency){
			spawner.timeTilSpawn = defaultTime +  DifficultyScaler.DifficultyLevel / difficultyMod;
			spawner.spawnDelay =  defaultTime + DifficultyScaler.DifficultyLevel / difficultyMod;
		}
		else{


			spawner.timeTilSpawn = Mathf.Clamp(defaultTime - DifficultyScaler.DifficultyLevel / difficultyMod, 3, defaultTime);
			spawner.spawnDelay = Mathf.Clamp(defaultTime - DifficultyScaler.DifficultyLevel / difficultyMod,3,defaultTime);




		}




	}


	// Use this for initialization
	void Start () {
		



	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
