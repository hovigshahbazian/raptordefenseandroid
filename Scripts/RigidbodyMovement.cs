﻿using UnityEngine;
using System.Collections;

public class RigidbodyMovement : MonoBehaviour {


	Rigidbody2D rb2D;

	// Use this for initialization
	void Start () {
		rb2D = GetComponent<Rigidbody2D> ();
		rb2D.velocity = Vector2.right * 50;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
