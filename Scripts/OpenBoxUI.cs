﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenBoxUI : MonoBehaviour {

	public CargoMenu cargoMenu;
	public Button buttonSelectPower;

	[SerializeField]
	private AnimatorSetter animSetter;
	// Use this for initialization
	void Start () {
		buttonSelectPower.interactable= false;
	}
		
	// Update is called once per frame
	void Update () {
		 
	}


	public void SetFullAuto(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.FullAuto));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestFullAuto"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp (PlayerPowerState.WeaponPower.FullAuto));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}

	public void SetMultiShot(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.Multishot));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestMultiShot"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp (PlayerPowerState.WeaponPower.Multishot));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}

	public void SetLaserBeam(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.Laserbeam));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestLazerBeam"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp (PlayerPowerState.WeaponPower.Laserbeam));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}

	public void SetExplodingBullet(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.ExplodingBullets));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestExplodeShot"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp (PlayerPowerState.WeaponPower.ExplodingBullets));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}

	public void SetInvulnerability(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.Invulnerability));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestInvulnerability"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invulnerability));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}

	public void SetSpeedUp(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.SpeedUp));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestSpeedUp"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp (PlayerPowerState.AircraftPower.SpeedUp));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}

	public void SetInvisibility(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.Invisibility));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestInvisibility"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp (PlayerPowerState.AircraftPower.Invisibility));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}


	public void SetShield(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.Shield));
		buttonSelectPower.onClick.AddListener( () => animSetter.SetTriggerName("OpenChestShield"));
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp (PlayerPowerState.AircraftPower.Shield));
		buttonSelectPower.onClick.AddListener( () =>  animSetter.SetTrigger());
	}








}
