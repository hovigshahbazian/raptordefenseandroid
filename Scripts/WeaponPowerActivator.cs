﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponPowerActivator : MonoBehaviour {

	public PlayerPowerState playerstate;
	//private PlayerGameData playerdata;
	public Button button;
	public int powerUses = 1;
	public float PowerTime;
	void Start(){
		//playerdata = GameDataManager.instance.loadedPlayerData;

		if (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower == PlayerPowerState.WeaponPower.Normal) {
			if (button != null)
				button.interactable = false;
		}

	}

	void Update(){
		
	}



	public void ActivateWeaponPower(){

		if (powerUses > 0) {
			powerUses--;

			if (button != null)
				button.interactable = false;


			switch (GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower) {

			case PlayerPowerState.WeaponPower.FullAuto:
				playerstate.FullAuto (PowerTime);
				break;
			case PlayerPowerState.WeaponPower.Multishot:
				playerstate.Multishot (PowerTime);
				break;
			case PlayerPowerState.WeaponPower.Laserbeam:
				playerstate.LaserBeam (PowerTime);
				break;
			case PlayerPowerState.WeaponPower.ExplodingBullets:
				playerstate.ExplodingBullets (PowerTime);
				break;
			default:
				break;
			}

			GameDataManager.instance.LoadedTempPlayerData.StartingWeaponPower = PlayerPowerState.WeaponPower.Normal;

		}
	}

	public  void EnableWeaponPowerBtn(){
		
	}

}
