﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnerDB : MonoBehaviour {


	[SerializeField]
	private ObjectPool[] goalPool;

	public float CurrentXPostion;
	public float IntervalDistance;

	public float nextSpawnLocationX;

	void OnEnable(){



	}


	// Use this for initialization
	void Start () {
		CurrentXPostion = this.transform.position.x;
		nextSpawnLocationX = CurrentXPostion + IntervalDistance;
	}

	// Update is called once per frame
	void Update () {
		CurrentXPostion = this.transform.position.x;

		if (CurrentXPostion > nextSpawnLocationX) {
			SpawnGoal ();
			nextSpawnLocationX = CurrentXPostion + IntervalDistance;
		}


	}


	void SpawnGoal(){

		int r = Random.Range (0, goalPool.Length);


		GameObject obj = goalPool[r].GetPooledObject ();

		//Debug.Log ("Spawned pool #" + r.ToString ());



		if (obj == null)
			return;

		obj.transform.position = this.transform.position;
		//obj.transform.rotation = this.transform.rotation;
		obj.SetActive (true);
	}





	void OnDisable(){
		CancelInvoke ();
	}


}
