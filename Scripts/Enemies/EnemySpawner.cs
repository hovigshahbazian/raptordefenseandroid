﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	private ObjectPool enemyPool;

	public bool unactive;
	public bool randomSpawning;

	public float timeTilSpawn;
	public float spawnDelay;
	public int randomDistanceX;
	public int randomDistanceY;
	public string SpawnEventName;
	//public bool inheritRotation;

	public FollowGoalArrowGroup arrowGroup;

	void OnEnable(){
		
		enemyPool = GetComponent<ObjectPool> ();

		if (!unactive) {

			if (randomSpawning)
				InvokeRepeating ("RandomlySpawnEnemy", timeTilSpawn, spawnDelay);
			else
				InvokeRepeating ("SpawnEnemy", timeTilSpawn, spawnDelay);


			//Debug.Log (gameObject.name + " Enabled");
		}

	}

	// Use this for initialization
	void Start () {
		/*
		enemyPool = GetComponent<ObjectPool> ();


		CancelInvoke ();
		if (randomSpawning)
			InvokeRepeating ("RandomlySpawnEnemy", timeTilSpawn, spawnDelay);
		else
			InvokeRepeating ("SpawnEnemy", timeTilSpawn, spawnDelay);

		if (unactive)
			CancelInvoke ();
		Debug.Log (gameObject.name +   " Started");
		*/

	}

	// Update is called once per frame
	void Update () {
		
	}

	void SpawnEnemy(){
		
		GameObject obj = enemyPool.GetPooledObject ();

		if (obj == null)
			return;
		
		obj.transform.position = this.transform.position;
		obj.SetActive (true);


		if(SpawnEventName != "")
			EventManager.TriggerEvent (SpawnEventName);

		//Debug.Log ("Spawning enemy:" + obj.name);

		if (arrowGroup != null) {
			arrowGroup.SetFollowArrow (obj.transform, obj.GetComponent<GoalFlag>());
		}
	}


	void RandomlySpawnEnemy(){
		
		GameObject obj = enemyPool.GetPooledObject ();

		if (obj == null)
			return;

		int x = Random.Range (-1, 2) * randomDistanceX;
		int y = Random.Range (-1, 2) * randomDistanceY;

		obj.transform.position = this.transform.position + new Vector3 (x, y);;

		//if(inheritRotation)
		//	obj.transform.rotation = this.transform.rotation;
		
		obj.SetActive (true);

		EventManager.TriggerEvent (SpawnEventName);


		if (arrowGroup != null) {
			arrowGroup.SetFollowArrow (obj.transform, obj.GetComponent<GoalFlag>());
		}

	}

	void OnDisable(){
		CancelInvoke ();
	}



}
