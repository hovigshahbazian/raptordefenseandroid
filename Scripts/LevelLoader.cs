﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {

	AsyncOperation async;
	public GameObject LoadingScreen;
	public Slider loadingbar;
	public Text loadingText;
    public bool loadingLevel;
	// Use this for initialization
	void Start () {
        loadingLevel = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public IEnumerator LoadCampaignMapAsync(){

        if (!loadingLevel)
        {
            async = SceneManager.LoadSceneAsync("CampaignMap");
            loadingLevel = true;
        }
		//async.allowSceneActivation = false;

		while (!async.isDone) {
			LoadingScreen.SetActive (true);
			loadingbar.gameObject.SetActive (true);
			float progress = Mathf.Clamp01 (async.progress / .9f);

			loadingbar.value = progress;
			loadingText.text = ((progress * 100f).ToString("F0") + "%");

			yield return null;
		}

	}
}
