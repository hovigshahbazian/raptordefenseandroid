﻿using UnityEngine;
using System.Collections;

public class MotherShipDeathAnim : MonoBehaviour {

	public ParticleGroup[] expParticles;
	public AudioSource[] expSounds;
	public Animation fallinganim;

	void PlayDeathAnimation(){
		foreach( ParticleGroup p in expParticles){
			p.Play ();
		}
		fallinganim.Play();
	}
}
