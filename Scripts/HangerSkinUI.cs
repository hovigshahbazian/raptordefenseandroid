﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Purchasing;
using System.Collections.Generic;

public class HangerSkinUI : MonoBehaviour {

	public PurchaserUI store;
	public Image planeImage;
	public Dropdown skinDropDown;
	public Animator SkinHangarAnim;
	//private PlayerGameData playerData;
	// Use this for initialization
	public GameObject displayPrompt;

	public bool boughtFighterBlue;
	public bool boughtCamogreen;
	public bool boughtArticLeopard;
	public bool boughtBlackTiger;
	public bool boughtDesertEagle;
	public bool boughtFlyingBeagle;

//	private Product FighterBlue;
	//private Product CamoGreen;
	//private Product ArticLeopard;
	//private Product BlackTiger;
	//private Product DesertEagle;
	//private Product FlyingBeagle;

//	private Product AllSkins_Android;
	//private Product AllSkins_IOS;

	GUIStyle DebugTextStyle;


	void Start () {

		//playerData = GameDataManager.instance.loadedPlayerData;
		DebugTextStyle = new GUIStyle ();
		DebugTextStyle.fontSize = 50;
		DebugTextStyle.normal.textColor = Color.white;

		boughtFighterBlue = GameDataManager.instance.LoadedTempPlayerData.FighterBlueSkin;
		boughtCamogreen = GameDataManager.instance.LoadedTempPlayerData.CamoGreenSkin;
		boughtArticLeopard = GameDataManager.instance.LoadedTempPlayerData.ArticLeopardSkin;
		boughtBlackTiger = GameDataManager.instance.LoadedTempPlayerData.BlackTigerSkin;
		boughtDesertEagle = GameDataManager.instance.LoadedTempPlayerData.DesertEagleSkin;
		boughtFlyingBeagle = GameDataManager.instance.LoadedTempPlayerData.FlyingBeagleSkin;

        //AllSkins_Android = IAPButton.IAPButtonStoreManager.Instance.StoreController.products.WithID (PurchaserUI.All_Skins_Android);
        //AllSkins_IOS = IAPButton.IAPButtonStoreManager.Instance.StoreController.products.WithID (PurchaserUI.All_Skins_IOS);
        skinDropDown.value = GameDataManager.instance.LoadedTempPlayerData.CurrentSkin;

    }
	
	// Update is called once per frame
	void Update () {
		checkAvailableSkins ();
	}

	void OnGUI(){

		//GUI.TextArea (new Rect (1200, 750, 500,500), "Bought Blue SKIN:" + FighterBlue.hasReceipt.ToString() ,DebugTextStyle);
	}


	public void checkAvailableSkins(){
		
		boughtFighterBlue = GameDataManager.instance.LoadedTempPlayerData.FighterBlueSkin;
		boughtCamogreen = GameDataManager.instance.LoadedTempPlayerData.CamoGreenSkin;
		boughtArticLeopard = GameDataManager.instance.LoadedTempPlayerData.ArticLeopardSkin;
		boughtBlackTiger = GameDataManager.instance.LoadedTempPlayerData.BlackTigerSkin;
		boughtDesertEagle = GameDataManager.instance.LoadedTempPlayerData.DesertEagleSkin;
		boughtFlyingBeagle = GameDataManager.instance.LoadedTempPlayerData.FlyingBeagleSkin;

		//if (AllSkins_Android.hasReceipt) {
		//	boughtFighterBlue = true;
		//	boughtCamogreen = true;
		//	boughtArticLeopard = true;
		//	boughtBlackTiger = true;
		//	boughtDesertEagle = true;
		//	boughtFlyingBeagle = true;

		//}

		//if (AllSkins_IOS.hasReceipt) {
		//	boughtFighterBlue = true;
		//	boughtCamogreen = true;
		//	boughtArticLeopard = true;
		//	boughtBlackTiger = true;
		//	boughtDesertEagle = true;
		//	boughtFlyingBeagle = true;

		//}



		//if (GameDataManager.IsPaid()) {

		//	boughtFighterBlue = true;
		//	boughtCamogreen = true;
		//	boughtArticLeopard = true;
		//	boughtBlackTiger = true;
		//	boughtDesertEagle = true;
		//	boughtFlyingBeagle = true;

	//	}
	

	
	}

	public void SelectSkin(){
		
		if (skinDropDown.value == 0) {
			GameDataManager.instance.LoadedTempPlayerData.CurrentSkin = skinDropDown.value;
            ChangeHangarSkin();
            planeImage.color = Color.white;
			displayPrompt.SetActive (false);

		}
		else if (skinDropDown.value == 1) {
			if (boughtFighterBlue) {
				GameDataManager.instance.LoadedTempPlayerData.CurrentSkin = skinDropDown.value;
                ChangeHangarSkin();
                planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}

		} 
		else if (skinDropDown.value == 2) {
			if (boughtCamogreen) {
				GameDataManager.instance.LoadedTempPlayerData.CurrentSkin = skinDropDown.value;
                ChangeHangarSkin();
                planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive(true);
			}
		}
		else if (skinDropDown.value == 3) {
			if (boughtArticLeopard) {
                ChangeHangarSkin();
                SkinHangarAnim.SetInteger ("Skin", GameDataManager.instance.LoadedTempPlayerData.CurrentSkin);
				planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}


		}
		else if (skinDropDown.value == 4) {
			if (boughtBlackTiger) {
				GameDataManager.instance.LoadedTempPlayerData.CurrentSkin = skinDropDown.value;
                ChangeHangarSkin();
                planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}


		}
		else if (skinDropDown.value == 5) {
			if (boughtDesertEagle) {
				GameDataManager.instance.LoadedTempPlayerData.CurrentSkin = skinDropDown.value;
                ChangeHangarSkin();
                planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}	


		}
		else if (skinDropDown.value == 6) {
			if (boughtFlyingBeagle) {
				GameDataManager.instance.LoadedTempPlayerData.CurrentSkin = skinDropDown.value;
                ChangeHangarSkin();
                planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}


		}
	}


    public void ChangeHangarSkin(){
        SkinHangarAnim.SetInteger("Skin", GameDataManager.instance.LoadedTempPlayerData.CurrentSkin);
      
    }

}
