﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalFlag : MonoBehaviour {
	
	public WaveDeathEvent[] deathEvents;
	public bool deathEventAchieved;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (!deathEventAchieved) {
			foreach (WaveDeathEvent w in deathEvents) {
				if (w.eventTriggered) {
					deathEventAchieved = true;
				}
			}
		}


	}


	public void CheckDeathEvents(){

		foreach (WaveDeathEvent w in deathEvents) {
			if (w.eventTriggered) {
				deathEventAchieved = true;
			}
		}
	}

}
