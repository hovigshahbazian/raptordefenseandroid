﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventCounterTextUI : MonoBehaviour {
	public int limitNum;
	public NumberTextUI numbers;
	public string eventName;
	// Use this for initialization
	void Start () {
		EventManager.StartListening (eventName, IncrementNumber);
	}

	// Update is called once per frame
	void Update () {

		if (numbers.number >= limitNum) {
			EventManager.StopListening (eventName, IncrementNumber);
		}
	}


	public void IncrementNumber(){
		numbers.number++;
	}

	public void DecrementNumber(){
		numbers.number--;
	}
}

