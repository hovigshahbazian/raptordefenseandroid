﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultySightShootIntervalSet : MonoBehaviour {
	private SightAI sight;

	[Range(2,10)]
	public int DifficultyMod = 2;

	private float difficultyValue = 1;
	void OnEnable () {
		sight = GetComponent<SightAI>();
		difficultyValue =( (float)(DifficultyScaler.DifficultyLevel) % (float) DifficultyMod) + 1f;
		sight.shootInterval = 1f/difficultyValue;
	}
	

}
