﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelDataEraseBTN : MonoBehaviour {
	private Button levelDataEraseBtn;

	// Use this for initialization
	void Start () {
		levelDataEraseBtn = GetComponent<Button> ();

		levelDataEraseBtn.onClick.AddListener (() => EraseLevelDataClick());
	}




	public void EraseLevelDataClick(){
		//GameDataManager.instance.EraseAllLevelData();
	}

}
