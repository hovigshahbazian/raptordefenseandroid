﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;

[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{
	public string EventAdFinished;
	public string EventAdFailed;
	public string EventAdSkipped;
	public bool AnimPlaying;
	public bool hasAnim;
	public string zoneId;
	private Button _button;
	void Start ()
	{
		_button = GetComponent<Button>();
		if (_button) _button.onClick.AddListener (delegate() { EventManager.TriggerEvent (EventAdFinished); });



	}
	void Update ()
	{
		if (_button) {



			if (string.IsNullOrEmpty (zoneId)) zoneId = null;

			if (hasAnim) {
				if (Advertisement.IsReady (zoneId) && !AnimPlaying) {
					_button.interactable = true;
				} else {
					_button.interactable = false;
				}
			} else {
				if (Advertisement.IsReady (zoneId)) {
					_button.interactable = true;
				} else {
					_button.interactable = false;
				}
			}




		}
	}
	void ShowAdPlacement ()
	{
		//if (string.IsNullOrEmpty (zoneId)) zoneId = null;
		//ShowOptions options = new ShowOptions();
		//options.resultCallback = HandleShowResult;



		//AnimPlaying = true;
		EventManager.TriggerEvent (EventAdFinished);
		//Advertisement.Show (zoneId, options);
	}


	public void SetAnimationOn(){
		AnimPlaying = true;
	}
	public void SetAnimationOff(){
		AnimPlaying = false;
	}


	private void HandleShowResult (ShowResult result)
	{
		
		switch (result)
		{
		case ShowResult.Finished:
			
			if(EventAdFinished != "")
				EventManager.TriggerEvent (EventAdFinished);
				AnimPlaying = true;
			break;
		case ShowResult.Failed:
			
			if (EventAdFailed != "")
				EventManager.TriggerEvent (EventAdFailed);
			
			break;
		case ShowResult.Skipped:

			if(EventAdSkipped != "")
				EventManager.TriggerEvent (EventAdSkipped);
			
			break;
		}
	}






}