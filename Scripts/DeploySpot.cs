﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DeploySpot : MonoBehaviour {
	public AudioSource SavedSound;
	public Health paratrooperHealth;
	public WaveDeathEvent playerSavedEvent;
	private BoxCollider2D col;
	public Text MissionInfo;
	public float AutoSuccessTime;
	private float autoSuccessTimer;
	private bool NeedingRescue;
	// Use this for initialization
	void Start () {
		col = GetComponent<BoxCollider2D> ();
		autoSuccessTimer = AutoSuccessTime;

		EventManager.StartListening ("EndRescueMission",CleanUpDeploySpot);
	}
	
	// Update is called once per frame
	void Update () {


		if (paratrooperHealth != null) {
			if (!paratrooperHealth.isAlive) {
				playerSavedEvent.enabled = false;
				paratrooperHealth = null;
			
				EventManager.TriggerEvent ("EndRescueMission");
				MissionInfo.text = "Failed to Save Scientist... Proceed to Next Location.";
				MissionInfo.color = Color.black;
			}
		}


		if (NeedingRescue) {

			autoSuccessTimer -= Time.deltaTime;

			if (autoSuccessTimer < 0) {
		
				EventManager.TriggerEvent ("EndRescueMission");
				EventManager.TriggerEvent ("ScientistSaved");

			}

		}

	}


	public void CleanUpDeploySpot(){
		NeedingRescue = false;
	}

	void OnCollisionEnter2D(Collision2D other){

		if (other.gameObject.tag == "Paratrooper") {
			col.enabled = false;
			EventManager.TriggerEvent ("StartRescueMission");
			NeedingRescue = true;
			paratrooperHealth = other.gameObject.GetComponent<Health> ();
			MissionInfo.text = "Save the Scientist!";
			MissionInfo.color = Color.red;
		}


	}



	public void SetSaved(){
		MissionInfo.text = "Scientist Saved. Proceed to next Location.";
		MissionInfo.color = Color.green;

		if(SavedSound != null)
			SavedSound.Play ();
	}
}
