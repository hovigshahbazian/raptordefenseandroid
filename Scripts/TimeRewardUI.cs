﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeRewardUI : MonoBehaviour {


	public float secondsRequiredforReward;

	public TimerTextUI timer;
	public string EventToListen;
	public string eventToTrigger;

	// Use this for initialization
	void Start () {
		EventManager.StartListening(EventToListen,CheckRequirement);
	}

	void CheckRequirement(){
		//Debug.Log ("Diamond acquid");
		if (timer.GetSecsLeft () >= secondsRequiredforReward) {
			EventManager.TriggerEvent (eventToTrigger);

		}


	}


}
