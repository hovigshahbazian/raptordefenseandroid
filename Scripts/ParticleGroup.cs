﻿using UnityEngine;
using System.Collections;

public class ParticleGroup : MonoBehaviour {

	public ParticleSystem[] particles;

	public void Play(){

		foreach( ParticleSystem p in particles){
			p.Play ();
		}

	}
}
