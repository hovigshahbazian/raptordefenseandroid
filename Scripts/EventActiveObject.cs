﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventActiveObject : MonoBehaviour {


	public GameObject gObject;

	public string EventString;

	public bool active;

	// Use this for initialization
	void Start () {
		if(EventString != null)
			EventManager.StartListening(EventString, DisableScript);
	}

	public void DisableScript(){
		gObject.SetActive(active);
	}

}
