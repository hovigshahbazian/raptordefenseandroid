﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatGuageShaking : MonoBehaviour {

	public Animator anim;
	public Animation shakeAnim;
	public AudioSource steamsound;
	public AudioSource Alarmsound;
	public HeatGuage heatGuage;

	public bool animPlaying;
	public float AnimDuration;
	public float animTimer;

	// Use this for initialization
	void Start () {
		animTimer = AnimDuration;
	}
	
	// Update is called once per frame
	void Update () {

		if(animPlaying){

			animTimer -= Time.deltaTime;

			if(animTimer <= 0){

				animPlaying = false;
				animTimer = AnimDuration;
				CancelInvoke();

			}


		}
		if (anim.GetBehaviour<OverheatAnim> ().overheated) {
			//Debug.Log ("Setting overheat is true");
		}

		if (anim.GetBehaviour<OverheatAnim> ().overheated && !animPlaying) {
			//Debug.Log ("Plkay Overheat");
			//Debug.Log (anim.GetBehaviour<OverheatAnim> ().overheated);
			PlayAnimation ();
			animPlaying = true;
		} 

		if (!anim.GetBehaviour<OverheatAnim> ().overheated) {
			StopSteamSound ();

			CancelInvoke ();
		}

	}


	public void PlaySteamSound(){
		steamsound.Play();

	}
	public void  StopSteamSound(){
		steamsound.Stop ();
	
	}

	public void PlayAlarmsound(){
		Alarmsound.Play();

	}

	public void PlayAnimation(){
		
		InvokeRepeating("PlaySteamSound",0f,1f);
		PlayAlarmsound();
	
		//shakeAnim.Play();
	}

}
