﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightAI : MonoBehaviour {


	public bool useTag;
	public string targetTag;
	[SerializeField]
	private BasicGun gun;
	[SerializeField]
	private Animator sightAnim;
	private Collider2D col;
	public bool SeesEnemy = false;
	public bool IsShooting = false;
	public float shootInterval = 1;

	// Use this for initialization
	void Start () {
		col = GetComponent<Collider2D>();
		col.enabled = true;

		EventManager.StartListening ("PlayerDeath", DisableGun );
		EventManager.StartListening ("LevelFinished", DisableGun );
	}

	void OnEnable(){
		col = GetComponent<Collider2D>();
		col.enabled = true;
	}

	// Update is called once per frame
	void Update () {




	}

	void OnTriggerEnter2D(Collider2D col){


		if (useTag) {
			if (col.tag == targetTag) {
				
				if(sightAnim  != null)
					sightAnim.SetBool ("SeesEnemy", true);

				IsShooting = true;
				SeesEnemy = true;
				gun.InvokeRepeating ("Shoot", shootInterval, shootInterval);

				//Debug.Log (this.transform.parent.gameObject.name + " Sees " + col.gameObject.name);
			
			}
		} else {
			if(gun != null)
				gun.enabled = true;
		}

	}


	void OnTriggerStay2D(Collider2D col){

		if (useTag) {
			if (col.tag == targetTag) {


				SeesEnemy = true;
			//	gun.Invoke("Shoot",shootInterval);

				//Debug.Log (this.transform.parent.gameObject.name + " Sees " + col.gameObject.name);

			}
		} else {
			if(gun != null)
				gun.enabled = true;
		}

	}


	void OnTriggerExit2D(Collider2D col){
		if (useTag) {
			if (col.tag == targetTag) {
		
				if(sightAnim  != null)
					sightAnim.SetBool ("SeesEnemy", false);

				//if(gun != null)
				//	gun.enabled = false;


				gun.CancelInvoke ();
				SeesEnemy = false;
				IsShooting = false;
			}
		} else {

			if(gun != null)
				gun.enabled = false;
		}



	}

	public void DisableGun(){
		gun.CancelInvoke ();
		col.enabled = false;
	}


	void OnDisable(){

		col.enabled = false;
	}

}
