﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSound : MonoBehaviour {


	public AudioSource src;
	public float TimeUntilSound;
	// Use this for initialization

	void Start () {
		src.PlayDelayed(TimeUntilSound);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
