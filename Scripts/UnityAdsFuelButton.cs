﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;


[RequireComponent(typeof(Button))]
public class UnityAdsFuelButton : MonoBehaviour {

	//private PlayerGameData playerData;
	public string AdFinished;
	public string AdFailed;
	public string AdSkipped;

	public string zoneId;
	private Button _button;
	private bool isPlaying = false;
	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		_button = GetComponent<Button>();
		if (_button) _button.onClick.AddListener (delegate() { ShowAdPlacement(); });
	}
	
	// Update is called once per frame
	void Update () {

		if (string.IsNullOrEmpty (zoneId)) zoneId = null;

	
		if (Advertisement.IsReady (zoneId)) {
				_button.interactable = true;
			} else {
				_button.interactable = false;
			}
	}

	 void ShowAdPlacement ()
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;
		ShowOptions options = new ShowOptions();
		options.resultCallback = HandleShowResult;
		Advertisement.Show (zoneId, options);
		isPlaying = true;
	}





	private void HandleShowResult (ShowResult result)
	{
		
		switch (result)
		{
		case ShowResult.Finished:

			if (AdFinished != "") {
				EventManager.TriggerEvent (AdFinished);
			}
                if (isPlaying)
                {
                    GameDataManager.instance.LoadedTempPlayerData.IncrementFuelCell(2);
                }


			GameDataManager.instance.SaveTempPlayer();

			isPlaying = false;
			_button.interactable = true;

			break;
		case ShowResult.Skipped:

			if (AdFailed != "") {
				EventManager.TriggerEvent (AdSkipped);
			}
			//isPlaying = false;
			_button.interactable = true;
			break;
		case ShowResult.Failed:

			if (AdSkipped != "") {
				EventManager.TriggerEvent (AdFailed);
			}
			//isPlaying = false;
			_button.interactable = true;
			break;
		}
	}

}
