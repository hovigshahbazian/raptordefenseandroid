﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpUI : MonoBehaviour {


	static public bool active;
	static public bool stopped;
	static public int page,newi;
	static public int oldlength;
	static public float scrollpad;
	static public float scrollpadlast;

	public int i,j,xoff1,xoff2,yoff1,yoff2,width;

	private Vector3 temp;
	//private Vector3 temporig;


	public Renderer rend;

	float y;
	float oneLineHeight;
	float answerHeight;

	public TextAsset asset;

	string[] info;

	int test;

	static public int[] newisave = new int[20];
	// Use this for initialization
	void Start () {
		//heightoffset=387;
	//	widthoffset=765;
	//	offset=screenhoffset / heightoffset;

		scrollpad=0;
		scrollpadlast=0;

		stopped=false;

		//temporig = transform.position; 

		yoff1=20;
		yoff2=20;
		xoff1=-2;
		xoff2=160;
		width=330;

		page=0;
		newi=0;
		oldlength=0;


		info = asset.text.Split("\n"[0]);
		oldlength=info.Length-1;

		for(i=0;i<20;i++) {
			newisave[i]=0;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
