﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeNotify : MonoBehaviour {

	public enum Upgrade { Heat,Ammo,Armor,Wing,Any };

	public Upgrade upgrade;

	private Image noteImage;
//	private PlayerGameData playerData;
	// Use this for initialization
	public UpgradeMenu menu;

	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		noteImage = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {

		switch (upgrade) {

		case Upgrade.Ammo:
			if (menu.ammoUpgradeAble) {
				noteImage.enabled = true;
			} else {
				noteImage.enabled = false;
			}
			break;
		case Upgrade.Armor:

			if (menu.armorUpgradeAble) {
				noteImage.enabled = true;
			} else {
				noteImage.enabled = false;
			}
			break;
		case Upgrade.Heat:

			if (menu.heatUpgradeAble) {
				noteImage.enabled = true;
			} else {
				noteImage.enabled = false;
			}
			break;
		case Upgrade.Wing:

			if (menu.wingUpgradeAble) {
				noteImage.enabled = true;
			} else {
				noteImage.enabled = false;
			}
			break;

		case Upgrade.Any:

			noteImage.enabled = false;


			if (menu.wingUpgradeAble) {
				noteImage.enabled = true;
			}
			if (menu.heatUpgradeAble) {
				noteImage.enabled = true;
			}

			if (menu.armorUpgradeAble) {
				noteImage.enabled = true;
			}
			if (menu.ammoUpgradeAble) {
				noteImage.enabled = true;
			}

			break;
		}


	}
}
