﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour {


	public enum BossType { Basic, Tank, DropShip, MotherShip };


	public BossType boss;

	public Animator BossAnimator;
	public string IntroTrigger;
	public string HideTrigger;
	public string AvoidTrigger;

	// Use this for initialization
	void Start () {
		EventManager.StartListening ("BossDeath", Reset);
		EventManager.StartListening ("HalfBossHealth", Avoid);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void PlayIntro(){
		BossAnimator.SetTrigger (IntroTrigger);
	}


	public void Reset(){
		BossAnimator.SetTrigger (HideTrigger);
	}


	public void Avoid(){
		BossAnimator.SetTrigger(AvoidTrigger);
	}

}
