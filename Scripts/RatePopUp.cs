﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatePopUp : MonoBehaviour {

	//private PlayerGameData playerData;


	// Use this for initialization
	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		if (GameDataManager.instance.LoadedTempPlayerData.UserRated) {
			gameObject.SetActive (false);
		} 
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public void UserRated(){
		GameDataManager.instance.LoadedTempPlayerData.UserRated = true;
	}



}
