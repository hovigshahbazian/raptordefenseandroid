﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableEnemyWave : MonoBehaviour {

	public GameObject[] Enemies;

	public void OnEnable(){

		foreach (GameObject g in Enemies) {
			g.SetActive (true);
		}
	}


}
