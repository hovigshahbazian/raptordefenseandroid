﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolume : MonoBehaviour {


	Slider musicSlider;

	public Image Background;

	public Image Fill;
	public Image Handle;

	// Use this for initialization
	void Start () {
		musicSlider = GetComponent<Slider> ();

		musicSlider.value = PlayerPrefs.GetFloat ("musicvolumechangevalue")*10;



	}
	
	// Update is called once per frame
	void Update () {

		if (Settings.active==true) {
			Background.enabled = true;
			Handle.enabled = true;
			Fill.enabled = true;
		}
		else {
			Background.enabled = false;
			Handle.enabled = false;
			Fill.enabled = false;
		}
	}



	public void ChangeMusicVolume(float vol){
		MM.musicvolumechangevalue=vol/10;
		MM.musicvolumechange = true;
	}

}
