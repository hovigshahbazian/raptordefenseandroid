﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyScaler : MonoBehaviour {

	public static int DifficultyLevel = 0;
	[Range(1,99)]
	public int DifficultyMod = 1;
	private Health health;
	//public string OnNextStageEvent;
	public bool isBoss;
	// Use this for initialization



	void OnEnable () {
		health = GetComponent<Health> ();

		if (!isBoss) {
			health.MaxHealth += DifficultyLevel / DifficultyMod;
			//health.MaxHealth -= 1;
			health.HealthPoints += DifficultyLevel / DifficultyMod;
			//health.HealthPoints -= 1;
		} else {
			health.MaxHealth += (DifficultyLevel / DifficultyMod) *10;
			//health.MaxHealth -= 10;
			health.HealthPoints += (DifficultyLevel / DifficultyMod) * 10;
			//health.HealthPoints -= 10;
		}


	}



}
