﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyTripleShootSet : MonoBehaviour {

	[Range(1,15)]
	public int DifficultyMod = 1;
	private BasicGun gun;

	void OnEnable () {
		gun = GetComponent<BasicGun>();

		if(DifficultyScaler.DifficultyLevel >= DifficultyMod){
			gun.tripleShootMode = true;
		}


	}
	

}
