﻿using UnityEngine;
using System.Collections;

public class MessageParticle : MonoBehaviour {

	public ParticleSystem ps;
	public string EventPlay;

	// Use this for initialization
	void Start () {

		if(EventPlay != ""){
			EventManager.StartListening(EventPlay,PlayParticleSystem);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayParticleSystem(){
		ps.Play ();
	}
}
