﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StageManager : MonoBehaviour {

	//private PlayerGameData playerData;
	public int StageTime;
	[System.Serializable]
	public struct SpawnerSet { 
		public bool AirWave1;
		public bool AirWave2;
		public bool SuperFighter;
		public bool GroundTurrent;
		public bool AllySaved;
		public bool BasicBoss;
	};

	public SpawnerSet[] sets;
	public BossController bosses;
	public Text stageText;
	public Text stageAnimText;
	public Text PreviousRecord;
	public Animation StageAnim;
	public TimeRecorderUI recordTime;
	// Use this for initialization

	public GameObject AirWave1Spawner;
	public GameObject AirWave2Spawner;
	public GameObject SuperFighterSpawner;
	public GameObject GroundTurrentSpawner;
	public GameObject AllySaveSpawner;
	public bool maxed = false;
	[SerializeField]
	private int StageCounter = 0;

	void Start () {
		//playerData = GameDataManager.instance.loadedPlayerData;
		DifficultyScaler.DifficultyLevel = 1;
		recordTime.Play ();
		stageText.text = " Stage " + StageCounter;
		stageAnimText.text = " Stage " + StageCounter;
		SetUpSpawners(sets[0]);
		EventManager.StartListening ("BossDeath", ProceedToNextStage);
		PreviousRecord.text = string.Format("{0:00}:{1:00}:{2:00}",(int)GameDataManager.instance.LoadedTempPlayerData.EndlessRecordtime / 60,(int)GameDataManager.instance.LoadedTempPlayerData.EndlessRecordtime % 60 ,(int)(GameDataManager.instance.LoadedTempPlayerData.EndlessRecordtime * 100) % 100);
	}
	
	// Update is called once per frame
	void Update () {
		stageText.text = " Stage " + StageCounter;
		stageAnimText.text = " Stage " + StageCounter;


		if ((recordTime.GetTimeSecs () % StageTime) == StageTime-1 && !StageAnim.isPlaying && recordTime.IsPlaying) {
			StageAnim.Play ();
			StageCounter++;
			DifficultyScaler.DifficultyLevel++;

			if (sets.Length >= StageCounter) {
				SetUpSpawners (sets [StageCounter - 1]);
			} else {

				if (!maxed) {
					maxed = true;
					SetUpSpawners (sets [sets.Length - 2]);
				} else {
					maxed = false;
					SetUpSpawners (sets [sets.Length - 1]);
				}

			}


		}


		if (!recordTime.IsPlaying) {

			if (recordTime.GetTimeSecsF () > GameDataManager.instance.LoadedTempPlayerData.EndlessRecordtime) {
				GameDataManager.instance.LoadedTempPlayerData.EndlessRecordtime = recordTime.GetTimeSecsF ();
			}



		}




	}

	public void SetUpSpawners(SpawnerSet set){

		if (set.AirWave1) {
			AirWave1Spawner.SetActive (true);
		} else {
			AirWave1Spawner.SetActive (false);
		}

		if (set.AirWave2) {
			AirWave2Spawner.SetActive (true);
		} else {
			AirWave2Spawner.SetActive (false);
		}

		if (set.SuperFighter) {
			SuperFighterSpawner.SetActive (true);
		} else {
			SuperFighterSpawner.SetActive (false);
		}

		if (set.GroundTurrent) {
			GroundTurrentSpawner.SetActive (true);
		} else {
			GroundTurrentSpawner.SetActive (false);
		}

		if (set.AllySaved) {
			AllySaveSpawner.SetActive (true);
		} else {
			AllySaveSpawner.SetActive (false);
		}


		if (set.BasicBoss) {
			bosses.PlayIntro ();
			recordTime.Pause ();
		}


	}

	public void ProceedToNextStage(){
		recordTime.Resume ();
	}


}
