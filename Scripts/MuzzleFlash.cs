﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour {

	public SpriteRenderer sprite;
	public float FlashDuration;
	private float flashTimer;
	bool flashing;

	// Use this for initialization
	void Start () {
		flashTimer = FlashDuration;
	}
	
	// Update is called once per frame
	void Update () {
		if(flashing){

			flashTimer -= Time.deltaTime;

			if(flashTimer <= 0){
				flashTimer = FlashDuration;
				flashing = false;
				sprite.enabled = false;
			}
		}
	}

	public void Flash(){
		flashing = true;
		sprite.enabled = true;
	}

	public void TripleFlash(){
		flashing = true;
		sprite.enabled = true;
	}

}
