﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTintInterval : MonoBehaviour {
	public Color colorTint;
	public Color OriColor;
	public float intervalTime;
	private SpriteRenderer SprRender;
	public Material tintedMat;
	public Material oriMat;
	public bool Ongoing = false;
	// Use this for initialization
	void Start () {
		SprRender = GetComponent<SpriteRenderer> ();


		if (Ongoing) {
			Invoke ("TintColor", intervalTime);
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TintColor(){
		SprRender.color = colorTint;
		SprRender.material = tintedMat;
		Invoke ("UntintColor", intervalTime);
	}
		
	public void UntintColor(){
		SprRender.color = OriColor;
		SprRender.material = oriMat;

		if (Ongoing) {
			Invoke ("TintColor", intervalTime);
		} else {
			Invoke ("RevertColor", intervalTime);
		}
	}


	public void RevertColor(){
		SprRender.color = OriColor;
		SprRender.material = oriMat;
	}


}
