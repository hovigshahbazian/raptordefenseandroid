﻿using UnityEngine;
using System.Collections;

public class VolumeControl : MonoBehaviour {

	private float musicVolume; 
	private float soundVolume;
	AudioSource src;

	public bool isMusic;
	public bool isSound;


	// Use this for initialization
	void Start () {

		EventManager.StartListening("GamePaused",PauseSound);

		EventManager.StartListening("GameUnpaused",UnpauseSound);


		src = GetComponent<AudioSource> ();
		soundVolume = MM.soundvolumechangevalue;
		musicVolume = MM.musicvolumechangevalue;

		if (isMusic)
			src.volume = musicVolume;
		else if (isSound)
			src.volume = soundVolume;

		//Debug.Log (soundVolume);
	}




	void UpdateSound(){

		soundVolume = MM.soundvolumechangevalue;
		musicVolume = MM.musicvolumechangevalue;

		if (isMusic){
			if(src)
				src.volume = musicVolume;
		}
		else if (isSound){
			if(src)
				src.volume = soundVolume;
		}
	}



	public void PauseSound(){
				
		src.Pause();

	}
	
	public void UnpauseSound(){

		src.UnPause();
		
	
	}

	public void Play(){
		src.Play();
	}


	public void PlaySound(){

		if (!src.isPlaying) {
			src.Play ();
		}
	}

}
