﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitEvent : MonoBehaviour {


	public string eventName;
    public string tagName = "";
	public LayerMask targetLayerMask;

	void OnTriggerExit2D(Collider2D col){

		if ( targetLayerMask  == (targetLayerMask | (1 << col.gameObject.layer)) ) {
            if (tagName == "")
            {
                if (eventName != null)
                    EventManager.TriggerEvent(eventName);
            }
            else
            {
                if (col.gameObject.tag == tagName)
                {
                    if (eventName != null)
                        EventManager.TriggerEvent(eventName);
                }
            
            }
			
			//Debug.Log (this.gameObject.name + " Collided with " + col.name);
		}


	}
}
