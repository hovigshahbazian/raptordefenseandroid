﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActiveRandomizer : MonoBehaviour {


	public GameObject[] sceneobjects;
	public float Sparcity;
	private float randomNum;

	// Use this for initialization
	void OnEnable () {

		foreach( GameObject g in sceneobjects){
			randomNum = Random.Range(0f,1f);

			if(randomNum <= Sparcity){
				g.SetActive(true);
			}
			else{
				g.SetActive(false);
			}

		}

	}


	
	// Update is called once per frame
	void Update () {
		
	}



}
