﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectDisplay : MonoBehaviour {

	public string CargoText;
	public string ShipUpgradeText;

	public string FullAutoText;
	public string MultishotText;
	public string LazerText;
	public string ExplodingBulletsText;
	public string InvisibilityText;
	public string InvulnerabilityText;
	public string SheildText;
	public string SpeedText;
	public string HangarDestroyedTxt;
	public string YellowDiamond;
	public string SecondaryAmmo;
	public string AllySavedTxt;
	public string HealthCollected = "Health Collected!";

	public Text text;
	public Animator collectAnim;
	public bool Playing = false;

	public enum DisplayEvent { FullAuto, Multishot, Laserbeam, ExplodingBullets,
		Invulnerability, SpeedUp, Invisibility, Sheild, Diamond, CargoBox, SecondaryAmmo, ShipUpgrade, HangarDestroyed, AllySaved, HealthPack};

	Queue<DisplayEvent> displayQueue = new Queue<DisplayEvent> ();

	// Use this for initialization
	void Start () {
		text.text = "";

		EventManager.StartListening ("CollectStopped", SetUpNextDisplay );
		EventManager.StartListening("HangarDestroyed", DisplayHangarDestroyed);
		EventManager.StartListening ("AllySaved", DisplayAllySaved);


	}





	// Update is called once per frame
	void Update () {




	}

	public void SetCargoBoxText(){
		text.text = CargoText;
	}

	public void SetShipUpgradeText(){
		text.text = ShipUpgradeText;
	}
		
	public void SetFullAutoText(){
		text.text = FullAutoText;
	}

	public void SetMultishotText(){
		text.text = MultishotText;
	}

	public void SetLazerBeamText(){
		text.text = LazerText;
	}
		
	public void SetExplodingShotText(){
		text.text = ExplodingBulletsText;
	}
		
	public void SetInvulnerabilityText(){
		text.text = InvulnerabilityText;
	}
		
	public void SetSheildText(){
		text.text = SheildText;
	}

	public void SetSpeedText(){
		text.text = SpeedText;
	}

	public void SetInvisibilityText(){
		text.text = InvisibilityText;
	}

	public void SetYellowDiamondText(){
		text.text = YellowDiamond;
	}

	public void SetSecondaryAmmo(){
		text.text = SecondaryAmmo;
	}

	public void SetHangarDestroyed(){
		text.text = HangarDestroyedTxt;
	}

	public void SetAllySaved(){
		text.text = AllySavedTxt;
	}
	public void SetHealthCollected(){
		text.text = HealthCollected;
	}
	public void SetDisplayText(DisplayEvent power){
		switch (power) {
		case DisplayEvent.CargoBox:
			SetCargoBoxText ();
			break;
		case DisplayEvent.Diamond:
			SetYellowDiamondText ();
			break;
		case DisplayEvent.ShipUpgrade:
			SetShipUpgradeText ();
			break;
		case DisplayEvent.ExplodingBullets:
			SetExplodingShotText ();
			break;
		case DisplayEvent.FullAuto:
			SetFullAutoText ();
			break;
		case DisplayEvent.Invisibility:
			SetInvisibilityText ();
			break;
		case DisplayEvent.Invulnerability:
			SetInvulnerabilityText ();
			break;
		case DisplayEvent.Laserbeam:
			SetLazerBeamText ();
			break;
		case DisplayEvent.Multishot:
			SetMultishotText ();
			break;
		case DisplayEvent.SecondaryAmmo:
			SetSecondaryAmmo ();
			break;
		case DisplayEvent.Sheild:
			SetSheildText ();
			break;
		case DisplayEvent.SpeedUp:
			SetSpeedText ();
			break;
		case DisplayEvent.HangarDestroyed:
			SetHangarDestroyed ();
			break;
		case DisplayEvent.AllySaved:
			SetAllySaved ();
			break;
		case DisplayEvent.HealthPack:
			SetHealthCollected ();
			break;
		}

	}


	public void PlayCargoAnim(){

		collectAnim.GetCurrentAnimatorStateInfo (0).IsName("Neutral");


		collectAnim.SetTrigger ("CargoBoxCollect");

	}


	public void DisplayHangarDestroyed(){
		PlayCargoAnim (DisplayEvent.HangarDestroyed);
	}

	public void DisplayAllySaved(){
		PlayCargoAnim (DisplayEvent.AllySaved);
	}

	public void PlayCargoAnim(DisplayEvent eventtxt){
		displayQueue.Enqueue (eventtxt);

		if (!Playing) {
			SetUpNextDisplay ();
		}

	}

	public void SetUpNextDisplay(){
		
		Playing = false;

		if (displayQueue.Count > 0) {

			DisplayEvent newPower = displayQueue.Dequeue ();
			SetDisplayText (newPower);
			StartDisplay ();
		}
	}


	public void StartDisplay(){
		    Playing = true;
			collectAnim.SetTrigger ("CargoBoxCollect");
	}



}
