﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RewardsUI : MonoBehaviour {

	public RewardType reward;
	public PlayerPowerState.WeaponPower weapon;
	public PlayerPowerState.AircraftPower aircraft;

	public Sprite CommonCargoBoxSpr;
	public Sprite RareCargoBoxSpr;
	public Sprite EpicCargoBoxSpr;
	public Sprite UpgradesSpr;
	public Sprite FullAutoPUSpr;
	public Sprite MultiShotPUSpr;
	public Sprite ExplodeShotPUSpr;
	public Sprite LazerBeamPUSpr;
	public Sprite SpeedUpPUSpr;
	public Sprite InvulnerabilityPUSpr;
	public Sprite SheildPUSpr;
	public Sprite InvisibilityPUSpr;

	public Sprite FighterBlueSpr;
	public Sprite CamogreenSpr;
	public Sprite ArticLeopardSpr;
	public Sprite BlackTigerSpr;
	public Sprite DesertEagleSpr;
	public Sprite FlyingBeagleSpr;

	public int number;
	public Image image;
	public Text text;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetUpCommonCargoReward(int amt){
		image.overrideSprite = CommonCargoBoxSpr;

		image.rectTransform.sizeDelta = new Vector2(200,200);

		if(amt == 1){
		text.text = " You received " + amt + " Common Cargo Box. Open it up in the Cargo menu.";
		}
		else{
			text.text = " You received " + amt + " Common Cargo Boxes. Open them up in the Cargo menu.";
		}


	}

	public void SetUpRareCargoReward(int amt){
		image.overrideSprite =  RareCargoBoxSpr;

		image.rectTransform.sizeDelta = new Vector2(200,200);

		if(amt == 1){
			text.text = " You received " + amt + " Rare Cargo Box. Open it up in the Cargo menu.";
		}
		else{
			text.text = " You received " + amt + " Rare Cargo Boxes. Open them up in the Cargo menu.";
		}
	}

	public void SetUpEpicCargoReward(int amt){
		image.overrideSprite = EpicCargoBoxSpr;

		image.rectTransform.sizeDelta = new Vector2(200,200);
		if(amt == 1){
			text.text = " You received " + amt + " Epic Cargo Box. Open it up in the Cargo menu.";
		}
		else{
			text.text = " You received " + amt + " Epic Cargo Boxes. Open them up in the Cargo menu.";
		}
	}


	public void SetUpSkinReward(Skins skin){
		switch (skin) {
		case Skins.FighterBlue:
			image.overrideSprite = FighterBlueSpr;

			image.rectTransform.sizeDelta = new Vector2(700,400);
			text.text = " You received the Blue Fighter Skin. Equip it in the Hangar.";
			break;
		case Skins.Camogreen:
			image.overrideSprite = CamogreenSpr;

			image.rectTransform.sizeDelta = new Vector2(700,400);
			text.text = " You received the Camo Green Skin. Equip it in the Hangar.";
			break;
		case Skins.ArticLeopard:
			image.overrideSprite = ArticLeopardSpr;

			image.rectTransform.sizeDelta = new Vector2(700,400);
			text.text = " You received the Artic Leopard Skin. Equip it in the Hangar.";
			break;
		case Skins.BlackTiger:
			image.overrideSprite = BlackTigerSpr;

			image.rectTransform.sizeDelta = new Vector2(700,400);
			text.text = " You received the Black Tiger Skin. Equip it in the Hangar.";
			break;
		case Skins.DesertEagle:
			image.overrideSprite = DesertEagleSpr;

			image.rectTransform.sizeDelta = new Vector2(700,400);
			text.text = " You received the Desert Eagle Skin. Equip it in the Hangar.";
			break;
		case Skins.FlyingBeagle:
			image.overrideSprite = FlyingBeagleSpr;

			image.rectTransform.sizeDelta = new Vector2(700,400);
			text.text = " You received the Flying Beagle Skin. Equip it in the Hangar.";
			break;
		}
	}




	public void SetUpUpgradeReward(int amt){
		image.overrideSprite = UpgradesSpr;
		image.rectTransform.sizeDelta = new Vector2(200,200);
		text.text = " You received " + amt + " Ship Upgrades. Upgrade you ship at the Hangar.";
	}

	public void SetUpWeaponPowerReward(PlayerPowerState.WeaponPower power, int amt){
		image.rectTransform.sizeDelta = new Vector2(328,175);
		switch (power) {
		case PlayerPowerState.WeaponPower.FullAuto:
			image.overrideSprite = FullAutoPUSpr;
			text.text = " You received " + amt + " Full-Auto Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			image.overrideSprite = MultiShotPUSpr;
			text.text = " You received " + amt + " Multi-Shot Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			image.overrideSprite = ExplodeShotPUSpr;
			text.text = " You received " + amt + " Explode-Shot Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			image.overrideSprite = LazerBeamPUSpr;
			text.text = " You received " + amt + " Laser Beam Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		default:
			break;
		}
	}

	public void SetUpAircraftPowerReward(PlayerPowerState.AircraftPower power, int amt){
		image.rectTransform.sizeDelta = new Vector2(432,220);
		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			image.overrideSprite = InvisibilityPUSpr;
			text.text = " You received " + amt + " Invulnerability Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			image.overrideSprite = SpeedUpPUSpr;
			text.text = " You received " + amt + " Speed Up Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		case PlayerPowerState.AircraftPower.Shield:
			image.overrideSprite = SheildPUSpr;
			text.text = " You received " + amt + " Shield Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			image.overrideSprite = InvisibilityPUSpr;
			text.text = " You received " + amt + " Invivibility Power Ups. Equip them in the Cargo menu for the next mission";
			break;
		default:
			break;
		}
	}



}
