﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpriteColorSetter : MonoBehaviour {

//	private SpriteRenderer sprite;
	private Image image;
	// Use this for initialization
	void Start () {

		image = GetComponent<Image> ();
	//	sprite = GetComponent<SpriteRenderer> ();
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void SetColor(Color32 color){
		image.color = new Color (color.r, color.g, color.b, color.a);
	}


	public void setFullAlpha(){
		image.color = new Color (1f, 1f, 1f, 1f);
	}
}
