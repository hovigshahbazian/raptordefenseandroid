﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightColorChanger : MonoBehaviour {
	public  Light[] hangarlights;
	public Color stopColor;
	public Color goColor;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetColorStop(){
		foreach (Light l in hangarlights) {
			l.color = stopColor;
		}

	}

	public void SetColorGo(){
		foreach (Light l in hangarlights) {
			l.color = goColor;
		}

	}

}
