﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class StartMenu : MonoBehaviour {


	public float startSecs;
	public string CampaignMap;
	public string Options;

	public Animator stBtnAnim;


	public Animation clip;


	AsyncOperation async;
	AudioSource gmStSound;

	float transitionTimer;
	bool startTimer = false;

	public Slider loadingbar;
	public Text loadingText;
    public bool loadingLevel;
	// Use this for initialization
	void Start () {
		transitionTimer = startSecs;
        //GameDataManager.instance.LoadTempPlayer();

        loadingLevel = false;

        //check how much time has passed scince the game has launched
        //GameDataManager.instance

        gmStSound = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {


		//Debug.Log (async.progress);


		if(Input.GetKeyDown(KeyCode.Escape)){
			QuitGame ();
		}

		//if (startTimer) {
		//	transitionTimer -= Time.deltaTime;


		//	if (transitionTimer <= 0) {
				//StartGame ();
				//async.allowSceneActivation = true;
		//	}
		//}


	}
	 
	public void LoadGame(){
		startTimer = true;
		stBtnAnim.SetBool ("Clicked", true);
		gmStSound.Play ();

	}




	public void StartGame(){
		//SceneManager.LoadScene (CampaignMap);
	}


	public IEnumerator LoadCampaignMapAsync(){
        Debug.Log("Loading campaignMap");


		if(gmStSound != null)
			gmStSound.Play ();

		if(clip != null)
			clip.Play ();

        


        if (!loadingLevel)
        {
            async = SceneManager.LoadSceneAsync("CampaignMap");
            loadingLevel = true;
        }
       
        
            while (!async.isDone) {

                float progress = Mathf.Clamp01 (async.progress / .9f);

                loadingbar.value = progress;
                loadingText.text = ((progress * 100f).ToString("F0") + "%");

                yield return null;
            }
            
        yield return async;
    }
    

	public void QuitGame(){
		Application.Quit();
	}
}
