using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour {

  //  public GameObject[] backgrounds;
    public Transform[] backgrounds;
	private float[] parallaxScales;
	public Transform Ground;
	private float groundparallax;
	private Vector3[] startPositions;

	public float smoothing = 1f;
	public float paraSpeed;
	private Transform cam;
	private Vector3 previousCamPos;
	float parallax = 0;
	void Awake () {
		cam = Camera.main.transform;
	}

	// Use this for initialization
	void Start () {
		previousCamPos = cam.position;
		parallaxScales = new float[backgrounds.Length];
		//startPositions = new Vector3[backgrounds.Length];

		for (int i=0; i<backgrounds.Length; i++) {
			parallaxScales[i]=backgrounds[i].position.z*-1;
		}
	
		if(Ground)
			groundparallax = Ground.position.z * -1;

		//for (int i=0; i<backgrounds.Length; i++) {
		//	startPositions [i] = backgrounds [i].position;
		//}

	}
	
	// Update is called once per frame
	void Update () {
		for (int i=0;i<backgrounds.Length; i++) {



			float parallax = 0;
			float change = (previousCamPos.x - cam.position.x);

			if (change != 0)
				parallax = parallaxScales [i] * Time.deltaTime * paraSpeed * (change / Mathf.Abs (change)); ;


			//float newPosition = Mathf.Repeat(Time.time * scrollSpeed[i], parallaxSizes[i]);
			//backgrounds [i].position = startPositions[i] + Vector3.right * newPosition;

	//		float backgroundTargetPosX= (backgrounds[i].position.x + parallax);

//			Vector3 backgroundTargetPos = new Vector3 (backgroundTargetPosX , backgrounds[i].position.y, backgrounds[i].position.z);
			//backgrounds[i].position = Vector3.Lerp (backgrounds[i].position, backgroundTargetPos,  Time.deltaTime) ;


			backgrounds [i].position = backgrounds[i].position + (Vector3.right * parallax);






		}
		previousCamPos = cam.position;

	}





	void FixedUpdate(){



		float change = (previousCamPos.x - cam.position.x);

		if (change != 0)
			parallax += groundparallax* Time.deltaTime * paraSpeed * (change); ;
		
		if(change >= 0){
			//parallax *= (0.2f);
		}

		parallax = Mathf.Clamp(parallax, -0.5f, 0.5f);

		//float newPosition = Mathf.Repeat(Time.time * scrollSpeed[i], parallaxSizes[i]);
		//backgrounds [i].position = startPositions[i] + Vector3.right * newPosition;

		//		float backgroundTargetPosX= (backgrounds[i].position.x + parallax);

		//			Vector3 backgroundTargetPos = new Vector3 (backgroundTargetPosX , backgrounds[i].position.y, backgrounds[i].position.z);
		//backgrounds[i].position = Vector3.Lerp (backgrounds[i].position, backgroundTargetPos,  Time.deltaTime) ;

		if(Ground)
		Ground.position = Ground.position + (Vector3.right * parallax);




	}









	public void Disable(){
		this.enabled = false;
	}
}
