﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class CampaignMapManager : MonoBehaviour {

	//private PlayerGameData playerData;
	public Image mapImage;
	public AchievementMap achievements;
	public int mapIndex = 0;
    public string currentlySelectedLevel = "Tutorial";


	//public float startSecs;
	AudioSource startSound;
	AsyncOperation async;

	public GameObject LoadingScreen;
	public Slider loadingbar;
	public Text loadingText;


    public bool levelLoading;
    //float transitionTimer;
    public bool FuelDecrementing;

	void Start () {
        levelLoading = false;
        FuelDecrementing = false;
        startSound = GetComponent<AudioSource>();
		/*
		if(GameDataManager.instance != null){



			GameDataManager.instance.LoadTempPlayer();

			GameDataManager.instance.LoadAllLevels();



		}*/

		if (achievements != null) {
			achievements.GatherLevelAchievementData ();
		}
		//playerData = GameDataManager.instance.loadedPlayerData;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public IEnumerator LoadMenusAsync(){

		async = SceneManager.LoadSceneAsync("Menus",LoadSceneMode.Additive);
	
		yield return async;
	}

	public void setLevel(string level){
		currentlySelectedLevel = level;
	}

	public void StartLevel(){



			LoadLevelAsync ();

	}

	public void GotoScene(string name){

		SceneManager.LoadScene (name);
	}


	public void MoveMapLeft()
	{
		if (mapIndex <= 1)
		{
			//newPosition = map.position.x - (mapImage.sprite.bounds.size.x * Screen.width/Screen.height)/2;
		//	TransitionLeftActive = true;
			mapIndex++;
		}
	}

	public void MoveMapRight(){
		if (mapIndex > 0) 
		{
			//newPosition = map.position.x + (mapImage.sprite.bounds.size.x  * Screen.width/Screen.height)/2;

			//TransitionRightActive = true;
			mapIndex--;
		}
	}

    public IEnumerator LoadLevelAsync() {
        startSound.Play();

        if (currentlySelectedLevel != "Tutorial") {

            if (!FuelDecrementing)
            {
                GameDataManager.instance.LoadedTempPlayerData.DecrementFuelCell();
                FuelDecrementing = true;
            }



            GameDataManager.instance.SaveTempPlayer();
        }

        if (!levelLoading)
        {
            async = SceneManager.LoadSceneAsync(currentlySelectedLevel);
            levelLoading = true;
        }
    
	
	     while(!async.isDone){
	

			LoadingScreen.SetActive (true);
			loadingbar.gameObject.SetActive (true);
			float progress = Mathf.Clamp01 (async.progress / .9f);

			loadingbar.value = progress;
			loadingText.text = ((progress * 100f).ToString("F0") + "%");


			yield return null;
		 }

		yield return async;
		//Debug.Log ("Loading Complete");
	}


	public void QuitGame(){
			Application.Quit();
	}

}
