﻿using UnityEngine;

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
//for encoding
using System.Text;
//for extra save ui
using UnityEngine.SocialPlatforms;
//for text, remove
using UnityEngine.UI;
using UnityEngine.Purchasing;


/* 
		 * Level 1 id: 0
		 * Level 2 id: 1
		 * Level 3 id: 2
		 * Level 4 id: 3
		 * Level 5 id: 4
		 * Level 6 id: 5
		 * Level 7 id: 6
		 * Level 8 id: 7
		 * Level 9 id: 8
		 * Level 10 id: 9
		 * Level A id: 10
		 * Level B id: 11
		 * Tutorial id: 12
		 * Survival id: 13
*/
public enum LevelName { Level1 = 0, Level2 = 1, Level3 = 2, Level4 = 3, Level5 = 4,
				         Level6 = 5, Level7 = 6, Level8 = 7, Level9 = 8, Level10 = 9, 
	                     LevelA = 10, LevelB = 11, Tutorial = 12, Endless = 13 }


public class GameDataManager : MonoBehaviour {

	//private static bool PaidVersion = false;

	public Sprite[] Skins;
	public SystemTime systemTime;
	public CargoTimer cargoTime;
	public FuelCellTimer fuelCellTime;
	public static GameDataManager instance;
	public GameObject debugPanel;
	public Text debugTxt;
	//public PlayerGameData loadedPlayerData;

	//public LevelGameData[] loadedLevelData;


	public PlayerData LoadedTempPlayerData;
	public LevelData[] LoadedTempLevelData;

	private Product AllSkins_Android;
	private Product AllSkins_IOS;



	//public string DebugString;


	void Awake()
	{
		
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
			Debug.Log("Destroyed another Game Data manager");
		}

	}

	public int HighScore{
		get { return GameDataManager.instance.LoadedTempPlayerData.HighScore;}
		set { 
			if (value >= GameDataManager.instance.LoadedTempPlayerData.HighScore) 
				GameDataManager.instance.LoadedTempPlayerData.HighScore = value; 
		}
	}

	void Start(){
		systemTime = GetComponent<SystemTime> ();

		//GameDataManager.instance.LoadedTempPlayerData = new PlayerData();


		//GameDataManager.instance.LoadedTempLevelData = new LevelData[14];
		//for(int i = 0; i <14;i++){
		//	GameDataManager.instance.LoadedTempLevelData[i] = new LevelData();
		//}

		GameDataManager.instance.LoadTempPlayer();

		GameDataManager.instance.LoadAllLevels();



	}

	void Update(){
		
	
	}



	public float GetTimeBetweenNowAndPastSave(){
		//PlayerGameData playerData = LoadScriptableObject<PlayerGameData> ("PlayerData");
		Debug.Log(Mathf.Abs((float)(DateTime.Now -  GameDataManager.instance.LoadedTempPlayerData.SavedTime).TotalSeconds));
		return Mathf.Clamp(Mathf.Abs((float)(DateTime.Now -  GameDataManager.instance.LoadedTempPlayerData.SavedTime).TotalSeconds), 0, 3600);

	}


	string GetInternalPath()
	{
		AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
		return activity.Call<AndroidJavaObject>("getFilesDir").Call<string>("getPath");
	}




	public void SaveTempPlayer(){
		Debug.Log("Saving player");
		string SaveFilePath = Application.persistentDataPath;

		#if UNITY_ANDROID

		if(persistentDataPath != null){
			SaveFilePath = persistentDataPath;
		}
		#endif

		#if UNITY_IOS
		SaveFilePath = Application.persistentDataPath;
		#endif

		try{

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create(SaveFilePath + "/playerInfo.dat");



		GameDataManager.instance.LoadedTempPlayerData.CurrentLevelSelected = LevelSelectorButton.currentLevel;
		GameDataManager.instance.LoadedTempPlayerData.TimeUntilCargoReplenish = cargoTime.GetTimeLeft ();
		GameDataManager.instance.LoadedTempPlayerData.TimeUntilFuelCellRepleinish = fuelCellTime.GetTimeLeft ();
		GameDataManager.instance.LoadedTempPlayerData.SavedTime = DateTime.Now;


		bf.Serialize (file, GameDataManager.instance.LoadedTempPlayerData);
		file.Close ();

		}
		catch(System.Exception e){

			Debug.Log(e);

			//debugPanel.SetActive(true);
			//debugTxt.text += "Failed To Save Player File\n";
		}




	}


	public void LoadTempPlayer(){
		Debug.Log("Loading player...");


		string SaveFilePath = Application.persistentDataPath;
		#if UNITY_ANDROID

		if(persistentDataPath != null){
			//debugPanel.SetActive(true);
			//debugTxt.text = "found";
			SaveFilePath = persistentDataPath;
		}
		#endif

		#if UNITY_IOS
		SaveFilePath = Application.persistentDataPath;
		#endif

		try{
			if (File.Exists (SaveFilePath + "/playerInfo.dat")) {

                FileStream file = File.Open (SaveFilePath + "/playerInfo.dat", FileMode.Open,FileAccess.Read,FileShare.Read);
                try
                {

                    BinaryFormatter bf = new BinaryFormatter();
                    GameDataManager.instance.LoadedTempPlayerData = (PlayerData)bf.Deserialize(file) as PlayerData;
                }
                catch(SerializationException e )
                {
                    Debug.Log(e);
                    debugPanel.SetActive(true);
                    debugTxt.text +=  "cannot serialized player file  \n";

                }
				file.Close ();

				fuelCellTime.TimeLimit =  GameDataManager.instance.LoadedTempPlayerData.TimeUntilFuelCellRepleinish;
				fuelCellTime.ReduceTime (GetTimeBetweenNowAndPastSave ());
				fuelCellTime.RestartTimer ();

			

				cargoTime.TimeLimit =  GameDataManager.instance.LoadedTempPlayerData.TimeUntilCargoReplenish;
				cargoTime.ReduceTime (GetTimeBetweenNowAndPastSave ());
				cargoTime.RestartTimer ();

				LevelSelectorButton.currentLevel = GameDataManager.instance.LoadedTempPlayerData.CurrentLevelSelected;

		} else {


				GameDataManager.instance.LoadedTempPlayerData = new PlayerData ();
				//Debug.Log("Created New Save");


		 }

			//throw new System.ArgumentException();
		}
        catch (ArgumentException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "File name undefined for player\n";
        }
        catch (FileNotFoundException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "File for  Player data Not Found\n";
        }
        catch (UnauthorizedAccessException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "File load not authorized player file \n";
        }
        catch (DirectoryNotFoundException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "Directory Not found for player file\n";
        }
        catch (IOException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "Error in opening File for player data \n";
        }

        catch (NotSupportedException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "Error in opening File for player data\n";
        }
        catch (Exception e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += e.Message + "for player file\n";
        }

    }



	public void SaveTempLevel(int i)
	{
		Debug.Log("Saving...");
        try
        {

            string SaveFilePath = Application.persistentDataPath;

#if UNITY_ANDROID

            if (persistentDataPath != null)
            {


                SaveFilePath = persistentDataPath;
            }

#endif

#if UNITY_IOS
			SaveFilePath = Application.persistentDataPath;
#endif

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(SaveFilePath + "/Level" + (i + 1).ToString("D2") + "Info.dat");

            bf.Serialize(file, GameDataManager.instance.LoadedTempLevelData[i]);
            file.Close();

        }
        catch (FileNotFoundException e)
        {

        }

        catch (System.Exception e)
        {
            Debug.Log(e);
            //debugPanel.SetActive(true);
            //debugTxt.text += "Failed To Save Level File " + (i+1) + " \n";
        }
		//Debug.Log("Data Saved for Level " + i);

		//if(i == 1)
		//DebugString += Application.persistentDataPath.ToString ();

	}
		
	public void LoadLevelTemp(int i)
	{
		string SaveFilePath = Application.persistentDataPath;

		#if UNITY_ANDROID

		if(persistentDataPath != null){
			SaveFilePath = persistentDataPath;
		}
		#endif

		#if UNITY_IOS
		SaveFilePath = Application.persistentDataPath;
		#endif

		try{
			Debug.Log ("Loading Level Data....");
			if (File.Exists ( SaveFilePath + "/Level" + (i+1).ToString ("D2") + "Info.dat")) {

                FileStream file = File.Open(SaveFilePath + "/Level" + (i + 1).ToString("D2") + "Info.dat", FileMode.Open, FileAccess.Read, FileShare.Read);

                try
                {
                    BinaryFormatter bf = new BinaryFormatter();
           
                    GameDataManager.instance.LoadedTempLevelData[i] = (LevelData)bf.Deserialize(file) as LevelData;

                }
                catch(SerializationException e)
                {
                    Debug.Log(e);
                    debugPanel.SetActive(true);
                    debugTxt.text += "cannot serialized Level file" + (i+1) + "\n";
                }

			    file.Close ();




		     } 
			//Application.dataPath
			//throw new System.Exception();

		}
        catch (ArgumentException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "File name undefined" + (i + 1) + "\n";
        }
        catch (FileNotFoundException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "File for level " + (i + 1) + "Not Found\n"; 
        }
        catch (UnauthorizedAccessException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "File Open not Authorized for level" + (i + 1) + "\n";
        }
        catch (DirectoryNotFoundException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "Directory Not found for level " + (i + 1) + "\n";
        }
        catch (IOException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "Error in opening File for level " + (i + 1) + "\n";
        }
        
        catch (NotSupportedException e)
        {
            Debug.Log(e);
            debugPanel.SetActive(true);
            debugTxt.text += "File type not Supported for level " + (i + 1) + "\n";
        }
        catch (Exception e){
			Debug.Log(e);
			debugPanel.SetActive(true);
			debugTxt.text += e.Message + (i+1) + "\n";
		}
	}


	public void ErasePlayerGameFile(){
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {

			File.Delete (Application.persistentDataPath + "/playerInfo.dat");
		}

	}



	public void EraseLevelGameFile(int i){
		if (File.Exists (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat")) {
			File.Delete (Application.persistentDataPath +  "/Level" + i.ToString ("D2") + "Info.dat");
		}
	}


	public void EraseAllLevelGameFiles(){
		for (int i = 1; i < 15; i++) {

			if (File.Exists (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat")) {

				if (i != 1 && i != 13 && i != 14) {
					File.Delete (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat");
				}
			}

		}

	}


	void OnApplicationQuit() {
		GameDataManager.instance.SaveTempPlayer ();

		GameDataManager.instance.SaveAllLevels();
	}
		
	public void EraseAllLevels(){

		foreach (LevelData leveldata in  GameDataManager.instance.LoadedTempLevelData) {

			leveldata.Unlocked = false;
			leveldata.Completed = false;
			leveldata.GoldEmblemsColleted = 0;
		}
	}


	public void UnlockAllLevels(){

		foreach (LevelData leveldata in  GameDataManager.instance.LoadedTempLevelData) {

			leveldata.Unlocked = true;
			leveldata.Completed = true;
			leveldata.GoldEmblemsColleted = 3;
		}
	}
		


	public void SaveAllLevels(){

		for(int i = 0 ; i < 14; i++){
			GameDataManager.instance.SaveTempLevel (i);
		}
	}

	public void LoadAllLevels(){

		for(int i = 0 ; i < 14; i++){
			GameDataManager.instance.LoadLevelTemp (i);
		}
	}



	/// ======================================================================================================
	///Functions Dealing with Finding the Persisitent Data Path for a Unity Android Game
	/// ======================================================================================================


	private static string[] _persistentDataPaths;

	public static bool IsDirectoryWritable(string path) {
		try {
			if (!Directory.Exists(path)) return false;
			string file = Path.Combine(path, Path.GetRandomFileName());
			using (FileStream fs = File.Create(file, 1)) {}
			File.Delete(file);
			return true;
		} catch {
			return false;
		}
	}

	private static string GetPersistentDataPath(params string[] components) {
		try {
			string path = Path.DirectorySeparatorChar + string.Join("" + Path.DirectorySeparatorChar, components); 
			if (!Directory.GetParent(path).Exists) return null;
			if (!Directory.Exists(path)) {
				Debug.Log("creating directory: " + path);
				Directory.CreateDirectory(path);
			}
			if (!IsDirectoryWritable(path)) {
				Debug.LogWarning("persistent data path not writable: " + path);
				return null;
			}
			return path;
		} catch (Exception ex) {
			Debug.LogException(ex);
			return null;
		}
	}

	public static string persistentDataPathInternal {
		#if UNITY_ANDROID
		get { 
			if (Application.isEditor || !Application.isPlaying) return Application.persistentDataPath;
			string path = null; 
			if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("storage", "emulated", "0", "Android", "data", Application.identifier, "files"); 
			if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("data", "data", Application.identifier, "files");
			return path;
		}
		#else
		get { return Application.persistentDataPath; }
		#endif
	}

	public static string persistentDataPathExternal {
		#if UNITY_ANDROID
		get { 
			if (Application.isEditor || !Application.isPlaying) return null;
			string path = null; 
			if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("storage", "sdcard0", "Android", "data", Application.identifier, "files"); 
			if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("storage", "sdcard1", "Android", "data", Application.identifier, "files"); 
			if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("mnt", "sdcard", "Android", "data", Application.identifier, "files"); 
			return path;
		}
		#else
		get { return null; }
		#endif
	}

	public static string[] persistentDataPaths {
		get {
			if (_persistentDataPaths == null) {
				List<string> paths = new List<string>();
				if (!string.IsNullOrEmpty(persistentDataPathInternal)) paths.Add(persistentDataPathInternal);
				if (!string.IsNullOrEmpty(persistentDataPathExternal)) paths.Add(persistentDataPathExternal);
				if (!string.IsNullOrEmpty(Application.persistentDataPath) && !paths.Contains(Application.persistentDataPath)) paths.Add(Application.persistentDataPath);
				_persistentDataPaths = paths.ToArray();
			}
			return _persistentDataPaths;
		}
	}

	// returns best persistent data path
	public static string persistentDataPath {
		get { return persistentDataPaths.Length > 0 ? persistentDataPaths[0] : null; }
	}

	public static string GetPersistentFile(string relativePath) {
		if (string.IsNullOrEmpty(relativePath)) return null;
		foreach (string path in persistentDataPaths) {
			if (FileExists(path, relativePath)) return Path.Combine(path, relativePath);
		}
		return null;
	}

	public static bool SaveData(string relativePath, byte[] data) {
		string path = GetPersistentFile(relativePath);
		if (string.IsNullOrEmpty(path)) {
			return SaveData(relativePath, data, 0);
		} else {
			try {
				File.WriteAllBytes(path, data);	
				return true;
			} catch (Exception ex) {
				Debug.LogWarning("couldn't save data to: " + path);
				Debug.LogException(ex);
				// try to delete file again
				if (File.Exists(path)) File.Delete(path);
				return SaveData(relativePath, data, 0);
			}
		}
	}

	private static bool SaveData(string relativePath, byte[] data, int pathIndex) {
		if (pathIndex < persistentDataPaths.Length) {
			string path = Path.Combine(persistentDataPaths[pathIndex], relativePath);
			try {
				string dir = Path.GetDirectoryName(path);
				if (!Directory.Exists(dir)) {
					Debug.Log("creating directory: " + dir);
					Directory.CreateDirectory(dir);
				}
				File.WriteAllBytes(path, data);			
				return true;
			} catch (Exception ex) {
				Debug.LogWarning("couldn't save data to: " + path);
				Debug.LogException(ex);
				if (File.Exists(path)) File.Delete(path);		// try to delete file again
				return SaveData(relativePath, data, pathIndex + 1);	// try next persistent path
			}
		} else {
			Debug.LogWarning("couldn't save data to any persistent data path");
			return false;
		}
	}

	public static bool FileExists(string path, string relativePath) {
		return Directory.Exists(path) && File.Exists(Path.Combine(path, relativePath));
	}


    //=================================================================================================
    //Save and load data using playerPrefs

    public void SavePlayerData()
    {

    }

    public void LoadPlayerData()
    {

    }

    public void LoadLevelData(int i) { 
}

    //
    ///










}
[Serializable]
public class PlayerData
{
	private const int LEVEL_LIMIT = 5;
	private const int FUEL_LIMIT = 8;
	private const int POWERUP_LIMIT = 99;
	private const int CARGOBOX_LIMIT = 999;
	private const int UPGRADES_LIMIT = 9999;

	public DateTime SavedTime = DateTime.Now;
	public float TimeUntilFuelCellRepleinish;
	public int NumOfFuelCells = 3;
	public bool UserRated;
	public int HighScore = 0;
	public float EndlessRecordtime = 0f;
	public int CurrentSkin = 0;

	public int CurrentLevelSelected  = 0;

	[SerializeField]
	private bool boughtFighterBlue = false;
	[SerializeField]
	private bool boughtCamogreen = false;
	[SerializeField]
	private bool boughtArticLeopard = false;
	[SerializeField]
	private bool boughtBlackTiger = false;
	[SerializeField]
	private bool boughtDesertEagle = false;
	[SerializeField]
	private bool boughtFlyingBeagle = false;



	public int maxEnemiesKilled = 0;

	public int CommonCargoBoxes = 0;
	public int RareCargoBoxes = 0;
	public int EpicCargoBoxes = 0;


	public PlayerPowerState.AircraftPower StartingAirPower;
	public PlayerPowerState.WeaponPower StartingWeaponPower;


	public int FullAutoPowerUps= 0;
	public int MultishotPowerUps = 0;
	public int LaserbeamPowerUps = 0;
	public int ExplodingBulletsPowerUps = 0;

	public int InvulnerabilityPowerUps = 0;
	public int SpeedUpPowerUps = 0;
	public int InvisibilityPowerUps = 0;
	public int ShieldPowerUps = 0;


	//cargo box system
	public float TimeUntilCargoReplenish;
	public int CargoBoxUses;



	public int shipUpgrades = 0;
	public int curArmorLevel = 1;
	public int curHeatLevel = 1;
	public int curAmmoLevel = 1;
	public int curWingLevel = 1;

	public RewardTier AceRT = RewardTier.None;
	public RewardTier DeathFromAboveRT = RewardTier.None;
	public RewardTier ExtraMileRT = RewardTier.None;
	public RewardTier CampaignCommanderRT = RewardTier.None;
	public RewardTier AlwaysLoadedRT = RewardTier.None;
	public RewardTier OverPoweredRT = RewardTier.None;
	public RewardTier EarthMoverRT = RewardTier.None;
	public RewardTier GuardianAngelRT = RewardTier.None;
	public RewardTier MrTopGunRT = RewardTier.None;

	public int ArmorLevel {
		get { return Mathf.Clamp (curArmorLevel, 1, LEVEL_LIMIT); }
		set { curArmorLevel = value; }
	}

	public int HeatLevel {
		get { return Mathf.Clamp (curHeatLevel, 1, LEVEL_LIMIT); }
		set { curHeatLevel = value; }
	}

	public int AmmoLevel {
		get { return Mathf.Clamp (curAmmoLevel, 1, LEVEL_LIMIT); }
		set { curAmmoLevel = value; }
	}

	public int WingLevel {
		get { return Mathf.Clamp (curWingLevel, 1, LEVEL_LIMIT);}
		set { curWingLevel = value; }
	}

	public bool FighterBlueSkin {
		set {boughtFighterBlue = value;}
		get { return boughtFighterBlue;}
	}

	public bool CamoGreenSkin {
		set {boughtCamogreen = value;}
		get { return boughtCamogreen;}
	}
	public bool ArticLeopardSkin {
		set {boughtArticLeopard= value;}
		get { return boughtArticLeopard;}
	}

	public bool BlackTigerSkin {
		set {boughtBlackTiger = value;}
		get { return boughtBlackTiger;}
	}

	public bool DesertEagleSkin {
		set {boughtDesertEagle = value;}
		get { return boughtDesertEagle;}
	}

	public bool FlyingBeagleSkin{
		set {boughtFlyingBeagle = value;}
		get { return boughtFlyingBeagle;}
	}


	public void ErasePlayerData(){

		NumOfFuelCells = 3;
		//HighScore = 0;
		CurrentSkin = 0;
		//maxEnemiesKilled = 0;
		CommonCargoBoxes = 0;
		RareCargoBoxes = 0;
		EpicCargoBoxes = 0;


		shipUpgrades = 0;

		FullAutoPowerUps  = 0;
		MultishotPowerUps = 0;
		LaserbeamPowerUps = 0;
		ExplodingBulletsPowerUps = 0;

		InvulnerabilityPowerUps = 0;
		SpeedUpPowerUps = 0;
		InvisibilityPowerUps = 0;
		ShieldPowerUps = 0;


		ArmorLevel = 1;
		AmmoLevel = 1;
		HeatLevel = 1;
		WingLevel = 1;



		EndlessRecordtime = 0f;

		AceRT = RewardTier.None;
		DeathFromAboveRT = RewardTier.None;
		ExtraMileRT = RewardTier.None;
		CampaignCommanderRT = RewardTier.None;
		AlwaysLoadedRT = RewardTier.None;
		OverPoweredRT = RewardTier.None;
		EarthMoverRT = RewardTier.None;
		GuardianAngelRT = RewardTier.None;
		MrTopGunRT = RewardTier.None;

	}


	public void DecrementFuelCell(){

		NumOfFuelCells--;
        Debug.Log("Minus Fuel Cell");
		NumOfFuelCells = Mathf.Clamp (NumOfFuelCells, 0,FUEL_LIMIT);
	}

	public void IncrementFuelCell(){

		NumOfFuelCells++;

		NumOfFuelCells = Mathf.Clamp (NumOfFuelCells, 0, FUEL_LIMIT);
	}

	public void IncrementFuelCell( int cells){

		NumOfFuelCells += cells;

		NumOfFuelCells = Mathf.Clamp (NumOfFuelCells, 0, FUEL_LIMIT);
	}
		

	//Common Cargo Box 

	public void GainCommonCargoBox(){
		CommonCargoBoxes++;
		CommonCargoBoxes = Mathf.Clamp (CommonCargoBoxes, 0, CARGOBOX_LIMIT);
	}
	public void GainCommonCargoBox(int amt){
		CommonCargoBoxes = Mathf.Clamp (CommonCargoBoxes + amt, 0, CARGOBOX_LIMIT);
	}
	public void RemoveCommonCargoBox(){
		CommonCargoBoxes--;
		CommonCargoBoxes = Mathf.Clamp (CommonCargoBoxes, 0, CARGOBOX_LIMIT);
	}

	//Rare Cargo Box 
	public void GainRareCargoBox(){
		RareCargoBoxes++;
		RareCargoBoxes = Mathf.Clamp (RareCargoBoxes, 0, CARGOBOX_LIMIT);
	}
	public void GainRareCargoBox(int amt){
		RareCargoBoxes = Mathf.Clamp (RareCargoBoxes + amt, 0, CARGOBOX_LIMIT);
	}
	public void RemoveRareCargoBox(){
		RareCargoBoxes--;
		RareCargoBoxes = Mathf.Clamp (RareCargoBoxes, 0, CARGOBOX_LIMIT);
	}

	//Epic Cargo Box 
	public void GainEpicCargoBox(){
		EpicCargoBoxes++;
		EpicCargoBoxes = Mathf.Clamp (EpicCargoBoxes, 0, CARGOBOX_LIMIT);
	}
	public void GainEpicCargoBox(int amt){
		EpicCargoBoxes = Mathf.Clamp (EpicCargoBoxes + amt, 0, CARGOBOX_LIMIT);
	}
	public void RemoveEpicCargoBox(){
		EpicCargoBoxes--;
		EpicCargoBoxes = Mathf.Clamp (EpicCargoBoxes, 0, CARGOBOX_LIMIT);
	}



	public void GainWeaponPowerUp( PlayerPowerState.WeaponPower power){
		Debug.Log(power.ToString());
		FullAutoPowerUps++;
		switch (power) {

		case PlayerPowerState.WeaponPower.FullAuto:
			FullAutoPowerUps++;
			FullAutoPowerUps = Mathf.Clamp(FullAutoPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			MultishotPowerUps++;
			MultishotPowerUps = Mathf.Clamp (MultishotPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			LaserbeamPowerUps++;
			LaserbeamPowerUps = Mathf.Clamp (LaserbeamPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			ExplodingBulletsPowerUps++;
			ExplodingBulletsPowerUps = Mathf.Clamp (ExplodingBulletsPowerUps, 0, POWERUP_LIMIT);
			break;

		default:
			break;

		}


	}


	public void GainWeaponPowerUps(PlayerPowerState.WeaponPower power, int amt){
		switch (power) {
		case PlayerPowerState.WeaponPower.FullAuto:
			FullAutoPowerUps = Mathf.Clamp(FullAutoPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			MultishotPowerUps = Mathf.Clamp (MultishotPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			LaserbeamPowerUps = Mathf.Clamp (LaserbeamPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			ExplodingBulletsPowerUps = Mathf.Clamp (ExplodingBulletsPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		default:
			break;

		}
	}

	public void RemoveWeaponPowerUp(PlayerPowerState.WeaponPower power){

		switch (power) {
		case PlayerPowerState.WeaponPower.FullAuto:
			FullAutoPowerUps--;
			FullAutoPowerUps = Mathf.Clamp(FullAutoPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			MultishotPowerUps--;
			MultishotPowerUps = Mathf.Clamp (MultishotPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			LaserbeamPowerUps--;
			LaserbeamPowerUps = Mathf.Clamp (LaserbeamPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			ExplodingBulletsPowerUps--;
			ExplodingBulletsPowerUps = Mathf.Clamp (ExplodingBulletsPowerUps, 0, POWERUP_LIMIT);
			break;

		default:
			break;

		}


	}

	public void GainAircraftPowerUp(PlayerPowerState.AircraftPower power ){

		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			InvulnerabilityPowerUps++;
			InvulnerabilityPowerUps = Mathf.Clamp (InvulnerabilityPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			SpeedUpPowerUps++;
			SpeedUpPowerUps = Mathf.Clamp (SpeedUpPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.Shield:
			ShieldPowerUps++;
			ShieldPowerUps = Mathf.Clamp (ShieldPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			InvisibilityPowerUps++;
			InvisibilityPowerUps = Mathf.Clamp (InvisibilityPowerUps, 0, POWERUP_LIMIT);
			break;

		default:
			break;

		}
	}

	public void GainAircraftPowerUps(PlayerPowerState.AircraftPower power, int amt){
		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			InvulnerabilityPowerUps = Mathf.Clamp (InvulnerabilityPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			SpeedUpPowerUps = Mathf.Clamp (SpeedUpPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.Shield:
			ShieldPowerUps = Mathf.Clamp (ShieldPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			InvisibilityPowerUps = Mathf.Clamp (InvisibilityPowerUps + amt, 0, POWERUP_LIMIT);
			break;
		default:
			break;

		}
	}


	public void RemoveAircraftPowerUp(PlayerPowerState.AircraftPower power){

		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			InvulnerabilityPowerUps--;
			InvulnerabilityPowerUps = Mathf.Clamp (InvulnerabilityPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			SpeedUpPowerUps--;
			SpeedUpPowerUps = Mathf.Clamp (SpeedUpPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.Shield:
			ShieldPowerUps--;
			ShieldPowerUps = Mathf.Clamp (ShieldPowerUps, 0, POWERUP_LIMIT);
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			InvisibilityPowerUps--;
			InvisibilityPowerUps = Mathf.Clamp (InvisibilityPowerUps, 0, POWERUP_LIMIT);
			break;

		default:
			break;

		}
	}



	public void GainShipUpgrade(){
		shipUpgrades++;
	}

	public void GainShipUpgrades(int amt){
		shipUpgrades += amt;
	}


	public void RemoveShipUpgrade(){
		shipUpgrades--;
		Mathf.Clamp (shipUpgrades, 0, UPGRADES_LIMIT);

	}

	public void RemoveShipUpgrades(int amt){
		shipUpgrades -= amt;
		Mathf.Clamp (shipUpgrades, 0, UPGRADES_LIMIT);
	}




	public void SetRated(bool r){
		UserRated = r;
	}



}



[Serializable]
public class LevelData
{
	public string Name = "";
	public bool Completed = false;
	public bool Unlocked = false;
	public int GroundEnemiesKilled = 0;
	public int AirEnemiesKilled = 0;
	public int PowerUpsCollected = 0;
	public int AlliesSaved = 0;
	public int SecondaryAmmoCollected = 0;
	public int SecondaryKills = 0;
	public int GoldEmblemsColleted = 0;

}





