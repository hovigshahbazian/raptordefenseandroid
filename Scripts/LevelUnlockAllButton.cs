﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUnlockAllButton : MonoBehaviour {
	private Button levelDataUnlockBtn;

	// Use this for initialization
	void Start () {
		levelDataUnlockBtn = GetComponent<Button> ();

		levelDataUnlockBtn.onClick.AddListener (() => UnlockLevelClick());
	}




	public void UnlockLevelClick(){
		GameDataManager.instance.UnlockAllLevels ();
	}
}
